Jacob Sanz-Robinson (260706158)
Everything here is probably easier to see in the UML diagram pdf.
____________________________________________________
Design Patterns Used:

-Strategy: Seen in Cool303Theme. Using an abstract class for specific themes to inherit from allows us to choose the theme at runtime, as opposed to choosing a single theme directly. Another interesting use of this is in the Cool303Button, where overloading JButton’s paintComponent method allows us to choose between button shapes with ease when creating a new theme.

-Decorator: the setTheme method found within the Cool303Components is a good example of the decorator pattern as it is used to enhance a single components behavior at a time (modifying the way it looks). As specified in the textbook, the components are taught to add functionality to other components, while preserving the interface. The decorated components can still be used much in the same way as the Swing objects they decorated. Finally, an open-ended set of decorations is possible.

-Iterator: I used Java’s inbuilt iterator to traverse the ArrayList of Components (children) for each component.
____________________________________________________
Design Techniques Used:

-Abstract classes: See Strategy pattern above.

-Interfaces: Cool303Component is an interface. It means all components will have a method to set the theme, and a method to get the list of children.

-Inheritance: The Cool303Components all inherit from Java Swing classes.
-Others that can be seen in the code: protected (null theme throws error), contracts (see interfaces/abstract classes), design with reuse (see abstract classes).

-Every method is commented. Code is neat and minimal.
____________________________________________________
Other nifty stuff:

-Pastel theme based on my childhood cat’s vomit. Read the JavaDocs for more info.

-Probably the easiest-to-make themes any under-slept, bedraggled, stressed-out, badly-fed comp student could throw at you.