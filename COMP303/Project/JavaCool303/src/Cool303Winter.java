/**
 * 
 * File Name:  		Cool303Winter.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	A depressing Montreal winter theme. Grey buttons represent the nasty sludge on the roads. Still more pleasant that Pastel/cat vomit.
 * Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			8th April 2018
*/

//package JavaCool303;
import java.awt.*;

public class Cool303Winter extends Cool303Theme{	

	/**
	// Name:  Cool303Winter
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Montreal winter theme constructor. Less depressing than the real thing.
	// Inputs:   None.
	// Outputs:  None.
	// Side-effects: None.
	// Special notes: None.
	*/ 
	public Cool303Winter(){
		super("rectangle", new Color(100, 250, 250), new Color(200, 200, 200));
	}
}

