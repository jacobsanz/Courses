/**
 * 
 * File Name:  		Cool303Component.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	Interface for all components. Must be able to getChildren components and setTheme.
 * Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			8th April 2018
 * NOTE:			May remove the getChildren method. Or just leave it. Hrmph.
*/

//package JavaCool303;
import java.util.*;
import java.awt.*;

public interface Cool303Component {

	public void setTheme(Cool303Theme theme);
	
	public ArrayList<Cool303Component> getChildren();
}
