/**
 * 
 * File Name:  		Cool303Pastel.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	A pastel coloured, bubbly theme. Colour scheme specifically chosen to resemble a disturbing memory of my cat's vomit.
 * Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			8th April 2018
*/

//package JavaCool303;
import java.awt.*;

public class Cool303Pastel extends Cool303Theme{

	/**
	// Name:  Cool303Pastel
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Cat vomit theme constructor.
	// Inputs:   None.
	// Outputs:  None.
	// Side-effects: modifies the object in question.
	// Special notes: Nope.
	*/ 
	public Cool303Pastel(){
		super("oval", new Color(255, 255, 150), new Color(255, 180, 200)); //cat vomit and ovals.
	}
}
