/**
 * 
 * File Name:  		Cool303Button.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	A button that implements the Cool303Component and extends the JButton. Has a label which it prints to console. No children.
* Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			8th April 2018
*/

//package JavaCool303;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.awt.geom.*;

public class Cool303Button extends JButton implements Cool303Component{
	
	int label;
	private Cool303Theme themeForOverride;
	
	/**
	// Name:  Cool303Button
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Constructor for a button which prints an integer label to the screen.
	// Inputs:   integer label.
	// Outputs:  None.
	// Side-effects: Prints label to console when pressed. Sets the object parameters. Creates an Action Listener.
	// Special notes: None.
	*/ 
    Cool303Button(int label) {
        super(String.valueOf(label));
        this.label = label;      
        this.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(label);
            }
        });
    }
    
	/**
	// Name:  paintSetup
	// Developers:  Jacob Sanz-Robinson. And some bloke on StackExchange that posted about darker(). Bless 'im.
	// Purpose:  Prepares the fill and shape of the button to be painted. Darkens the button a bit when it's clicked. Aesthetic af.
	// Inputs:   A graphics 2D object and a shape.
	// Outputs:  None.
	// Side-effects: modifies inputs.
	// Special notes: None.
	*/ 
    protected void paintSetup(Graphics2D object2D, Shape shape){
    	object2D.setColor(getBackground());
        if (getModel().isPressed()) {
            object2D.setColor(getBackground().darker());
        }
        object2D.draw(shape);
        object2D.fill(shape);
    }
   
	/**
	// Name:  paintComponent
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Override the usual JButton paintComponent for one that we can change the button shape with.
	// Inputs:   gContextClass object
	// Outputs:  None.
	// Side-effects: modifies the input.
	// Special notes: None.
	*/  
    @Override
    protected void paintComponent(Graphics gContextClass){
        Graphics2D object2D = (Graphics2D) gContextClass;
        switch(themeForOverride.getButtonShape()){
            case "oval":
                paintSetup(object2D, new Ellipse2D.Double(0, 0, getSize().width - 5, getSize().height - 4));
                super.paintComponent(object2D);
                break;
            case "rectangle":
                paintSetup(object2D, new Rectangle2D.Double(0, 0, getSize().width - 8, getSize().height - 6));
                super.paintComponent(object2D);
                break;
            }
        } 
        
	/**
	// Name:  setTheme
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Applies the theme to the button. Makes sure it's visible.
	// Inputs:   a Cool303Theme.
	// Outputs:  None.
	// Side-effects: modifies the object in question.
	// Special notes: None.
	*/ 
	public void setTheme(Cool303Theme theme) {
		this.themeForOverride = theme;
		this.setBackground(theme.getButtonColor());
        this.setOpaque(false);
        this.setBorderPainted(false);
		this.repaint();
		//no need to paint children here.
	}
	
	/**
	// Name:  getChildren
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Returns an empty array. Fulfills the pesky interface, and buttons have no children.
	// Inputs:   None
	// Outputs:  Empty arraylist.
	// Side-effects: Nay.
	// Special notes: None.
	*/ 
	public ArrayList<Cool303Component> getChildren(){
		ArrayList<Cool303Component> children = new ArrayList<Cool303Component>(); //always empty for button
		return children;
	}
}
