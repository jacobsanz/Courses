/**
 * 
 * File Name:  		Cool303Container.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	A container to hold Cool303Components. Is a component itself. Extends Jpanel.
 * Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			8th April 2018
*/

//package JavaCool303;
import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.BorderFactory;
import javax.swing.border.*;
import java.awt.Font;

public class Cool303Container extends JPanel implements Cool303Component{

    private ArrayList<Cool303Component> children;
    private String title = "";
    Color optionalColor;
    
	/**
	// Name:  Cool303Container
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Constructor for the title-less container.
	// Inputs:   None
	// Outputs:  None.
	// Side-effects: None.
	// Special notes: None.
	*/ 
	public Cool303Container(){
		super();
		this.children = new ArrayList<Cool303Component>();
	}
	
	/**
	// Name:  Cool303Container
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Constructor for the titled container.
	// Inputs:   String as title.
	// Outputs:  None.
	// Side-effects: None.
	// Special notes: None.
	*/ 		
	public Cool303Container(String title){
		super();
		this.title = title;
		this.setBorder(BorderFactory.createTitledBorder(new LineBorder(Color.BLACK, 1), title, TitledBorder.LEFT, TitledBorder.TOP, new Font("Dialog", Font.BOLD, 12)));
		this.children = new ArrayList<Cool303Component>();
	}
	
	/**
	// Name:  put
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Adds the child to both the arraylist of children, and add the component to the superclass Jpanel.
	// Inputs:   A Comp303Component
	// Outputs:  None.
	// Side-effects: None.
	// Special notes: None.
	*/ 	
    public void put(Cool303Component component){
        this.children.add(component);
        this.add((Component)component);
    }

	/**
	// Name:  getChildren
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  gets Children. Like MJ.
	// Inputs:   None.
	// Outputs:  Children arraylist
	// Side-effects: None.
	// Special notes: None.
	*/ 	
	public ArrayList<Cool303Component> getChildren(){
		return this.children;
	}
	
	/**
	// Name:  setBGColour
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Sets the background colour of the container to any specified colour.
	// Inputs:   3 ints, for red/green/blue parameters.
	// Outputs:  None.
	// Side-effects: None.
	// Special notes:  Haven't really used it, but it was asked for.
	*/ 	
	public void setBGColor(int R, int G, int B){
		this.optionalColor = new Color(R, G, B);
		this.setBackground(optionalColor);
	}
	
	/**
	// Name:  setTheme
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Applies the theme to the container, and recursively to children.
	// Inputs:   a Cool303Theme.
	// Outputs:  None.
	// Side-effects: NEIN.
	// Special notes: None.
	*/ 
	public void setTheme(Cool303Theme theme){
		this.setBackground(theme.getBackgroundColor());
		Iterator<Cool303Component> itr = this.children.iterator();
		while (itr.hasNext()){
			Cool303Component current = itr.next();
			current.setTheme(theme);
		}
		this.setOpaque(true);
		this.repaint();
	}
		


}
