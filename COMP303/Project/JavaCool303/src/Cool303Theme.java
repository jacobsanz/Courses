/**
 * 
 * File Name:  		Cool303Theme.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	Abstract class housing the basic elements I deem a theme should contain.
 * Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			8th April 2018
*/

//package JavaCool303;
import java.awt.*;

public abstract class Cool303Theme{
	
	private String buttonShape;
	private Color backgroundColor;
	private Color buttonColor;

	/**
	// Name:  Cool303Theme
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Constructor to make a theme.
	// Inputs:   String buttonShape, Color backgroundColor, Color buttonColor
	// Outputs:  None.
	// Side-effects: None.
	// Special notes: This was the easiest way to make a theme I could come up with.
	//					String buttonShape: enter either "oval" or "rectangle". These cases are handled in Cool303Button. More could be added easily.
	//					Color backgroundColor: enter a new colour in the form... new Color(int R, int G, int B).
	//					Color buttonColor: as above.
	*/ 	
	public Cool303Theme(String buttonShape, Color backgroundColor, Color buttonColor){
		this.buttonShape = buttonShape;
		this.backgroundColor = backgroundColor;
		this.buttonColor = buttonColor;
	}

	/**
	// Name:  getButtonShape
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  gets Theme's button shape.
	// Inputs:   None.
	// Outputs:  String with "oval" or "rectangle"
	// Side-effects: None.
	// Special notes: None.
	*/ 		
	protected String getButtonShape(){
		return buttonShape;
	}

	/**
	// Name:  getBackgroundColor
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  gets a color for the background.
	// Inputs:   None.
	// Outputs:  Color object.
	// Side-effects: None.
	// Special notes: None.
	*/ 		
	protected Color getBackgroundColor(){
		return backgroundColor;
	}

	/**
	// Name:  getButtonColor
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  gets button's color.
	// Inputs:   None.
	// Outputs:  Color object.
	// Side-effects: None.
	// Special notes: None.
	*/ 	
	protected Color getButtonColor(){
		return buttonColor;
	}

}

	

