/**
 * 
 * File Name:  		Cool303Root.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	The daddy component. Is basically a Jframe that recusively sets themes to all it's children.
 * Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			8th April 2018
*/

//package JavaCool303;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

public class Cool303Root extends JFrame implements Cool303Component{

    private ArrayList<Cool303Component> children;
    
	/**
	// Name:  Cool303Root
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Constructor of the root object.
	// Inputs:   None.
	// Outputs:  None.
	// Side-effects: None.
	// Special notes: None.
	*/      
    public Cool303Root(){
		super();
		this.children = new ArrayList<Cool303Component>();
	}

	/**
	// Name:  setTheme
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Applies the theme to the root, and recursively to children.
	// Inputs:   a Cool303Theme.
	// Outputs:  None.
	// Side-effects: None.
	// Special notes: None.
	*/ 
	public void setTheme(Cool303Theme theme) {
		if (theme == null){
			throw new NullPointerException("Theme can't be null");
		}
		this.getContentPane().setBackground(theme.getBackgroundColor());
        Iterator<Cool303Component> itr = this.children.iterator();
		while (itr.hasNext()){
			Cool303Component current = itr.next();
			current.setTheme(theme);
		}
	}
	
	/**
	// Name:  put
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  Adds the child to both the arraylist of children, and add the component to the superclass Jpanel.
	// Inputs:   A Comp303Component. A container would make Swing happy.
	// Outputs:  None.
	// Side-effects: None.
	// Special notes: None.
	*/ 	
    public void put(Cool303Component container){
        this.children.add(container);
        this.add((Component)container);
    }

	/**
	// Name:  getChildren
	// Developers:  Jacob Sanz-Robinson
	// Purpose:  gets Children components.
	// Inputs:   None.
	// Outputs:  Children arraylist
	// Side-effects: None.
	// Special notes: None.
	*/ 	
	public ArrayList<Cool303Component> getChildren(){
		return this.children;
	}
	
	

}
