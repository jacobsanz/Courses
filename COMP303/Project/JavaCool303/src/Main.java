/**
 * 
 * File Name:  		Main.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	Test the Cool303 classes by building an application with 20 buttons.
 * Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			8th April 2018
*/

import javax.swing.*;
import java.awt.*;

 /**
 // Name:  Main
 // Developers:  Jacob Sanz-Robinson
 // Purpose:  Tests the Cool303 classes as stated in the file header comments.
 // Inputs:   None
 // Outputs:  Nah.
 // Side-effects: Prints numbers of the buttons to console, should you you to press said buttons. Displays various decorated swing elements.
 // Special notes: Nay.
 */
public class Main {

 	public static void main(String[] args) {

		Cool303Theme chosenTheme = new Cool303Winter(); //provide your theme here.

		Cool303Root root = new Cool303Root();
        root.setLocationRelativeTo(null);
		root.setVisible(true);
		root.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Cool303Container container = new Cool303Container("optional BOLD title");
		for (int i=1; i<=20; i++){
			container.put(new Cool303Button(i));
		}
		container.setLayout(new GridLayout(5,5));
		
		root.put(container);
		root.setTheme(chosenTheme);
		root.pack();
 	}





}
