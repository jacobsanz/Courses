/**
 * 
 * File Name:  		Main.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	Main class, builds the world and runs the simulation for 100n iterations.
* Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			14th April 2018
*/

import javax.swing.*;
import java.awt.*;
import java.util.Scanner; 

public class Main{
	
	private static World testWorld;

	public static void main(String[] args){
		buildWorld();
		Scanner scan = new Scanner(System.in);
		boolean exitCue = false;
		
		while (exitCue == false){
			for (int i = 0; i < 100; i++){
				testWorld.step();
				testWorld.display();
			}
			System.out.println("Would you like to run the simulation again? If so, press Y.");
			char answer = scan.next().charAt(0);
			if (answer == 'Y' || answer == 'y'){
				exitCue = false;
			}
		}
	}
	
	private static void buildWorld(){
		testWorld = new World(10, 10);
		//create objects.
		Immovable im1 = new Immovable();
		Immovable im2 = new Immovable();
		Immovable im3 = new Immovable();
		Immovable im4 = new Immovable();
		Immovable im5 = new Immovable();
		Moveable m1 = new Moveable();
		Moveable m2 = new Moveable();
		Moveable m3 = new Moveable();
		Autonomous a1 = new Autonomous();
		Autonomous a2 = new Autonomous();
		//add objects to the world.
		testWorld.add(im1, 5, 2);
		testWorld.add(im2, 3, 3);
		testWorld.add(im3, 4, 4);
		testWorld.add(im4, 5, 5);
		testWorld.add(im5, 3, 5);
		testWorld.add(m1, 4, 2);
		testWorld.add(m2, 6, 2);
		testWorld.add(m3, 4, 3);
		testWorld.add(a1, 7, 2);
		testWorld.add(a2, 6, 3);
	}
}
