Jacob Sanz-Robinson (260706158)
Everything seems to work except the World’s step. Hopefully, I will have time to fix this…But if you (T.A. or grader) are seeing this message, then I probably didn’t have time. Have mercy plz :(.
____________________________________________________
Design Patterns Used:

-MVC: This is best seen in the World Class. Our model is the 2D ground array. Our view is implemented through swing in the Display method. The controller is the step function, which controls data flow into the model. The main method in the Main class ensures the view is updated whenever the model is, seeing the two separate.

-Template: The template pattern is used when the items extend the SimItem abstract class.
____________________________________________________
Design Techniques Used:

-Abstract classes: SimItem is implemented as an abstract class so I could reuse its code.

-Inheritance: Immovable and Moveable both inherit from SimItem. Autonomous inherits from Moveable. This is so that when a block crashes into an autonomous, it too will be shifted by the collision.
-Others that can be seen in the code: protected (particularly in world, the range if the array is protected when adding elements to it), contracts (see abstract classes), design with reuse (see abstract classes).

-Code is commented.
____________________________________________________
