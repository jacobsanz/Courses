/**
 * 
 * File Name:  		World.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	The world for the block simulator. Provides an update for the world, a display through swing, and some methods.
* Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			14th April 2018
*/

import javax.swing.*;
import java.awt.*;

public class World{

	private SimItem[][] ground;
	JFrame worldFrame; 
	
	public World(int x, int y){
		this.ground = new SimItem[x][y];;
		this.worldFrame = new JFrame();
		worldFrame.pack();
	}
	
	public void step(){
		//Go through 2D array. Every time we encounter an autonomous, update its step, and any consequences of this step.
		for (int i = 0; i < this.ground.length-1; i++){
			for (int j = 0; j < this.ground[0].length-1; j++){
				if (this.ground[i][j] != null){
					int oldX = this.ground[i][j].getY();
					int oldY = this.ground[i][j].getX();
					if(this.ground[i][j].getToken() == 'A'){ //when we encounter an Autonomous, move it to its next position.
						Autonomous current = (Autonomous)this.ground[i][j];
						current.step();
						//if the position is valid, and there are no collisions, go there. Set the last position to null.
						if(validPosition(current.getX(), current.getY()) && (!collisionOccurs(current.getX(), current.getY()))){
							this.ground[current.getY()][current.getX()] = current;
							this.ground[i][j] = null;
						} 
						//if the position is valid, but we collide with another Moveable or its child Autonomous, move them.
						if(validPosition(current.getX(), current.getY()) && (this.ground[current.getX()][current.getY()] instanceof Moveable)){
							Moveable victim = (Moveable)this.ground[current.getY()][current.getX()]; //displaced by collision
							int oldVX = current.getX();
							int oldVY = current.getY();
							int direction = current.getLMD();
							victim.collisionStep(direction);
							//if no further collision occurs when the victim is moved (I am not allowing for chain reactions)...
							if(validPosition(victim.getX(), victim.getY()) && (!collisionOccurs(victim.getX(), victim.getY()))){ 
								this.ground[victim.getY()][victim.getX()] = victim;
								this.ground[oldVY][oldVX] = null;
							}
							victim.setLMD(99); //clear the last move direction.
							victim = null;
							current = null;
						}					
					}
				}
			}
		}
	}
	
	public void display(){
		//Uses an array of Jpanels on the world's Jframe to display the token of each block as a Jlabel.
		JPanel[][] panels = new JPanel[this.ground.length][this.ground[0].length];
		this.worldFrame.setSize(500, 500);
		this.worldFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.worldFrame.setVisible(true);
		JPanel pnl = new JPanel(new GridLayout(this.ground.length,this.ground[0].length));
		pnl.setSize(500, 500);
		this.worldFrame.add(pnl);
		JLabel lbl;
		for (int i = 0; i < this.ground.length; i++){
			for (int j = 0; j < this.ground[0].length; j++){
				if (this.ground[i][j] != null){
					lbl = new JLabel(Character.toString(this.ground[i][j].getToken()), JLabel.CENTER);
				} else {
					lbl = new JLabel(" ");
				}
				lbl.setSize(30, 30);
				panels[i][j] = new JPanel();
				panels[i][j].add(lbl);
				panels[i][j].setBorder(BorderFactory.createLineBorder(Color.red));
				pnl.add(panels[i][j]);
			}
		}
	}
	
	public void add(SimItem item, int x, int y){
		//adds a SimItem to the ground array
		if (!collisionOccurs(x, y)){
			this.ground[item.getY()][item.getX()] = null;
			this.ground[y][x] = item;
			item.setX(y);
			item.setY(x);
		} else{
			throw new IllegalArgumentException("A block is already occupying this position");
		}
	}
	
	public boolean collisionOccurs(int x, int y){
		//checks if something is already in a cell.
		if(this.ground[y][x] != null){
			return true;
		}
		return false;
	}
	
	public boolean validPosition(int x, int y){
		//checks if a position is within the world bounds, and is NOT occupied by an immoveable.
		boolean answer = false;
		if ((x>0)&&(x<this.ground[0].length-1)){
			if ((y>0)&&(x<this.ground.length-1)){
				answer = true;
				if (this.ground[y][x] != null){
					if(this.ground[y][x].getToken() == 'I'){
						answer = false;
					}
				}
			}
		}
		return answer;
	}

}
