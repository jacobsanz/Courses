/**
 * 
 * File Name:  		Moveable.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	A block which shows no initiative of its own, but can be shoved around by other moving blocks.
* Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			14th April 2018
*/
public class Moveable extends SimItem{

	protected int lastMoveDirection; //the direction of the last move it took, so that if collision occurs, we can apply this to other item.
	
	public Moveable(){
		super();
		this.token = 'M';
		this.name = "Moveable";
	}
	
	public int getLMD(){
		return this.lastMoveDirection;
	}
	
	public void setLMD(int lmd){
			this.lastMoveDirection = lmd;
	}
	
	public void collisionStep(int direction){
		//Moves the Moveable after a collision. Gets the Last Moved Direction from the block that bumps into it.
		int newX, newY;
		newX = this.x;
		newY = this.y;
		switch (direction){
            case 0:  newY = newY - 1; //up
                     break;
            case 1:  newX = newX + 1; //right
                     break;
            case 2:  newY = newY + 1; //down
                     break;
            case 3:  newX = newX - 1; //left 
                     break;
            case 99: break;	//do nothing
            default: throw new IllegalArgumentException("Invalid direction...");
		}
			this.setX(newX);
			this.setY(newY);
			this.setLMD(direction);
	}
}
