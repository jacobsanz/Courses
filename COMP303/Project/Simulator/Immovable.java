/**
 * 
 * File Name:  		Immovable.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	I AM AN IMMOVABLE AND I DO NOTHING EXCEPT SIT THERE. I AM SNORLAX.
* Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			14th April 2018
*/

public class Immovable extends SimItem{

	public Immovable(){
		super();
		this.token = 'I';
		this.name = "Immovable";
	}

}
