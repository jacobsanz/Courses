/**
 * 
 * File Name:  		Autonomous.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	A block which moves one block at a time through it's own volition (the random package).
* Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			14th April 2018
*/

import java.util.Random;

public class Autonomous extends Moveable{
	
	private Random rand;

	public Autonomous(){
		super();
		this.token = 'A';
		this.name = "Autonomous";
		this.rand = new Random();
	}	

	public void step(){
		//0 is move up, 1 is move right...etc...
		//If new position is valid, then set new x and new y. with setters.
		int newX, newY;
		int direction = rand.nextInt(4);
		newX = this.x;
		newY = this.y;
		switch (direction){
            case 0:  newY = newY - 1;
                     break;
            case 1:  newX = newX + 1;
                     break;
            case 2:  newY = newY - 1;
                     break;
            case 3:  newX = newX - 1;
                     break;
            default: throw new IllegalArgumentException("Invalid direction...");
		}
			this.setX(newX);
			this.setY(newY);
			this.setLMD(direction);
		
	}
	
}
