/**
 * 
 * File Name:  		SimItem.java
 * Developers:  	Jacob Sanz-Robinson (260706158)
 * Purpose:     	Abstract class to provide code reuse for the block items.
* Inputs:			None
 * Outputs:  		None
 * Modifications:	None
 * Date:   			14th April 2018
*/

public abstract class SimItem{
	
	protected String name = "";
	protected char token;
	protected int x, y;
	
	public SimItem(){
		
	}
	
	public char getToken(){
		return this.token;
	}
	
	public int getX(){
		return this.x;
	}
	
	public int getY(){
		return this.y;
	}
	
	public void setX(int x){
		if (x >= 0){	
			this.x = x;
		}
	}
	
	public void setY(int y){
		if (y >= 0){	
			this.y = y;
		}
	}
}
