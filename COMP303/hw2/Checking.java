// File Name:		Checking.java
// Developers:		Jacob Sanz-Robinson (260706158)
// Purpose:			Creates a Checking as a child of Account.
// Inputs: 			None
// Outputs: 		None
// Modifications:	None
// Date:			4th Feb. 2018

public class Checking extends Account{
	
	private static String accountType = "Checking";
	
	// Name:			Checking
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Constructor for a checking account. Calls the parent constructor, sets attributes.
	// Inputs: 			A double for initial deposit.
	// Outputs:			None
	// Side-effects:	sets attributes in Account.
	// Special notes:	None	
	Checking (double initialDeposit){
		super();
		this.setBalance(initialDeposit);
		this.accountType = accountType;
	}

}
