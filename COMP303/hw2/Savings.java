// File Name:		Savings.java
// Developers:		Jacob Sanz-Robinson (260706158)
// Purpose:			Creates a Savings as a child of Account.
// Inputs: 			None
// Outputs: 		None
// Modifications:	None
// Date:			4th Feb. 2018

public class Savings extends Account{
	
	private static String accountType = "Savings";
	
	// Name:			Savings
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Constructor for a savings account. Calls the parent constructor, sets attributes.
	// Inputs: 			a double for initial deposit.
	// Outputs:			None
	// Side-effects:	sets attributes in Account.
	// Special notes:	None		
	Savings (double initialDeposit){
		super();
		this.setBalance(initialDeposit);
		this.accountType = accountType;
	}
}
