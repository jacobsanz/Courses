// File Name:		Menu.java
// Developers:		Jacob Sanz-Robinson (260706158)
// Purpose:			Provide a User Interface, and and organized, broken up, easy to debug way to control methods in other classes.
// Inputs: 			None
// Outputs: 		None
// Modifications:	None
// Date:			4th Feb. 2018

import java.util.*;

public class Menu{

		Scanner scan = new Scanner(System.in);
		Bank myBank = new Bank();
		boolean exitSignal;
		
		
		// Name:			displayMenu
		// Developers:		Jacob Sanz-Robinson
		// Purpose:			Calls the main functions that allow the programs to interface with the user. Loops until user exits.
		// Inputs: 			None
		// Outputs:			None
		// Side-effects:	None
		// Special notes:	Aw yiss...	
		public void displayMenu(){
			System.out.println("COMP 303 - assignment 2 - banking application. Yay.");
			while (!exitSignal){
				optionShow();
				int option = optionGet();	//get input
				optionDo(option);			//perform action based on input.
			}
		}
		
		// Name:			optionShow
		// Developers:		Jacob Sanz-Robinson
		// Purpose:			Prints all user option to screen.
		// Inputs: 			None
		// Outputs:			None
		// Side-effects:	Prints 7 options.
		// Special notes:	Will be looped infinitely in displayMenu until user quits.				
		private void optionShow(){
			System.out.println("Choose an action by typing in the corresponding number and the enter key:");
			System.out.println("1 - Create customer");
			System.out.println("2 - Create bank account");
			System.out.println("3 - Get balance");
			System.out.println("4 - Deposit");
			System.out.println("5 - Withdrawal");
			System.out.println("6 - Make a transfer");
			System.out.println("7 - Quit");
		}
		
		// Name:			optionGet
		// Developers:		Jacob Sanz-Robinson
		// Purpose:			Recieves numerical input from user, encoding their decision.
		// Inputs: 			None
		// Outputs:			None
		// Side-effects:	Prints prompts.
		// Special notes:	None			
		private int optionGet(){
			int option = -9; //unobtrusive random initilization value.
			boolean validOption = false;
			while (validOption != true){
				System.out.println("Your option: ");
				try {
					option = Integer.parseInt(scan.nextLine());
				}
				catch( NumberFormatException e){
					System.out.println("Invalid option. You need to enter a number as your option.");
				}
				if (option < 0 || option > 7){
					System.out.println("Invalid option. Make sure your option in the valid numerical range.");
				} else {
					validOption = true;
				}
			}
			return option;
		}
		
		// Name:			optionDo
		// Developers:		Jacob Sanz-Robinson
		// Purpose:			Takes action based on users input in optionGet
		// Inputs: 			An integer representing the users choice of action
		// Outputs:			None
		// Side-effects:	Prints prompts.
		// Special notes:	None
		private void optionDo(int option){
			switch(option){
					case 1:
						createCustomer();
						break;
					case 2:
						createAccount();
						break;
					case 3:
						checkBalance();
						break;
					case 4:
						depositMenu();
						break;
					case 5:
						withdrawMenu();
						break;
					case 6:
						transferMenu();
						break;
					case 7:
						System.out.println("Exiting program.");
						System.exit(0);
						break;
					default:
						System.out.println("Well, this is awkward. Something went wrong");
			}
		}
		
		// Name:			createCustomer
		// Developers:		Jacob Sanz-Robinson
		// Purpose:			Guides a user through adding a new customer to the database.
		// Inputs: 			None.
		// Outputs:			None
		// Side-effects:	Prints prompt. Creates a customer object, adds it to the banks arraylist of customers.
		// Special notes:	None
		private void createCustomer(){
			String customerName = "";
			System.out.println("Type in your name, and press enter:");
			customerName = scan.nextLine();
			Customer customer = new Customer(customerName);
			myBank.newCustomer(customer);
		}
			
		// Name:			createAccount
		// Developers:		Jacob Sanz-Robinson
		// Purpose:			Guides a user through adding a new account to a customer of choice.
		// Inputs: 			None
		// Outputs:			None
		// Side-effects:	Prints prompts, adds a new account to the arraylist of accounts held by the customer.
		// Special notes:	None
		private void createAccount(){
			int chosenAccount = whichCustomer();
			if (chosenAccount < 0){
				return;
			}
			Customer currentCustomer = myBank.getCustomer(chosenAccount);
			String accountType = "";
			double initialDeposit = 0.0;
			boolean validAccount = false;
			while(!validAccount){
				System.out.println("Type in 'checking' or 'savings', and press enter:");
				accountType = scan.nextLine();
				if (accountType.equalsIgnoreCase("checking") || accountType.equalsIgnoreCase("savings")){
					validAccount = true;
				} else{
					System.out.println("Invalid account type");
				}
			}
			validAccount = false;
			while(!validAccount){
				System.out.println("Type in your initial deposit, and press enter");
				try {
					initialDeposit = Double.parseDouble(scan.nextLine());
				}
				catch( NumberFormatException e){
					System.out.println("Invalid intiial deposit. You need to enter a number as your deposit.");
				}
				if (initialDeposit < 0){
					System.out.println("Invalid initial deposit. Needs to be more than 0.");
				} else {
					validAccount = true;
				}
			}
			Account myAccount;
			if (accountType.equalsIgnoreCase("checking")){
				myAccount = new Checking(initialDeposit);
			} else{
				myAccount = new Savings(initialDeposit);
			}
			myBank.getCustomer(chosenAccount).newAccount(myAccount);
		}	
		
		
		// Name:			whichCustomer
		// Developers:		Jacob Sanz-Robinson
		// Purpose:			Gets integer input from the user, which is the unique Customer ID.
		// Inputs: 			None.
		// Outputs:			An integer (customer ID). -1 if there are no customers.
		// Side-effects:	Prints prompts.
		// Special notes:	Will be used to access the Customer fields.
		private int whichCustomer(){
			ArrayList<Customer> customers = myBank.getAllCustomers();
			if (customers.size() <= 0){
				System.out.println("No customers exist.");
				return -1;
			}
			int account = -1;
			System.out.println("Enter the customer number:");
			try {
				account = Integer.parseInt(scan.nextLine());
			}
			catch( NumberFormatException e){
				System.out.println("Invalid. You need to enter a number.");
				account = -1;
			}
			if (account >= customers.size()){
				System.out.println("This customer doesn't exist.");
				return -1;
			}
			return account;		
		}	

		// Name:			whichAccount
		// Developers:		Jacob Sanz-Robinson
		// Purpose:			Gets integer input from the user, which is the account ID, within customer.
		// Inputs: 			Takes a customer as input.
		// Outputs:			Outputs the int ID for the account. -1 if there are no accounts.
		// Side-effects:	Prints prompts.
		// Special notes:	None		
		private int whichAccount(Customer customer){
			ArrayList<Account> accounts = customer.getAllAccounts();
			if (accounts.size() <= 0){
				System.out.println("No accounts exist.");
				return -1;
			}
			int account = -1;
			System.out.println("Enter the account number:");
			try {
				account = Integer.parseInt(scan.nextLine());
			}
			catch( NumberFormatException e){
				System.out.println("Invalid. You need to enter a number.");
				account = -1;
			}
			if (account >= accounts.size()){
				System.out.println("This account doesn't exist.");
				return -1;
			}
			return account;		
		}		
			
		// Name:			depositMenu
		// Developers:		Jacob Sanz-Robinson
		// Purpose:			Guides the user through making a deposit into an account.
		// Inputs: 			None
		// Outputs:			None
		// Side-effects:	Prints prompts. Changes the contents of an account.
		// Special notes:	None			
		private void depositMenu(){
			int chosenCustomer = whichCustomer();
			if (chosenCustomer < 0){ //prevents crashes when there are no customers
				return;
			}
			int chosenAccount = whichAccount(myBank.getCustomer(chosenCustomer));
			double quantity = 0.0;
			double discountPercentage = myBank.getCustomer(chosenCustomer).getDiscountPercentage(); 
			if (chosenAccount >= 0){ //prevents crahses when there are no accounts
				System.out.println("Enter deposit amount:");
				try {
					quantity = Double.parseDouble(scan.nextLine());
				}
				catch( NumberFormatException e){
					System.out.println("Invalid deposit. You need to enter a number as your withdrawal.");
					quantity = 0.0;
				}			
				myBank.getCustomer(chosenCustomer).getAccount(chosenAccount).deposit(quantity, discountPercentage);
			}	
		}
		
		// Name:			withdrawtMenu
		// Developers:		Jacob Sanz-Robinson
		// Purpose:			Guides the user through making a withdrawal from an account.
		// Inputs: 			None
		// Outputs:			None
		// Side-effects:	Prints prompts. Changes the contents of an account.
		// Special notes:	None	
		private void withdrawMenu(){
			int chosenCustomer = whichCustomer();
			if (chosenCustomer < 0){
				return;
			}
			int chosenAccount = whichAccount(myBank.getCustomer(chosenCustomer));
			double quantity = 0.0;
			double discountPercentage = myBank.getCustomer(chosenCustomer).getDiscountPercentage(); 
			if (chosenAccount >= 0){
				System.out.println("Enter withdrawal amount:");
				try {
					quantity = Double.parseDouble(scan.nextLine());
				}
				catch( NumberFormatException e){
					System.out.println("Invalid withdrawal. You need to enter a number as your withdrawal.");
					quantity = 0.0;
				}
				if(myBank.getCustomer(chosenCustomer).getAccount(chosenAccount) instanceof Savings){ ///Checks if the account in question is a Savings account
					if (quantity <= 1000.0){
						System.out.println("Invalid withdrawal. Savings withdrawals need to be >= $1000");
						return;
					}
				}		
				myBank.getCustomer(chosenCustomer).getAccount(chosenAccount).withdraw(quantity, discountPercentage);
			}	
		}
		
		// Name:			checkBalance
		// Developers:		Jacob Sanz-Robinson
		// Purpose:			Guides the user through checking the balance of an account.
		// Inputs: 			None
		// Outputs:			None
		// Side-effects:	Prints stuff.
		// Special notes:	None	
		private void checkBalance(){
			int chosenCustomer = whichCustomer();
			if (chosenCustomer < 0){
				return;
			}
			int chosenAccount = whichAccount(myBank.getCustomer(chosenCustomer));
			if (chosenAccount >= 0){
				System.out.println(myBank.getCustomer(chosenCustomer).getAccount(chosenAccount).getBalance());
			} else{
				System.out.println("invalid account");
			}
		}
		
		// Name:			transferMenu
		// Developers:		Jacob Sanz-Robinson
		// Purpose:			Guides the user through transferring money between a customers accounts.
		// Inputs: 			None
		// Outputs:			None
		// Side-effects:	Prints prompts, changes the content of 2 accounts.
		// Special notes:	Uses withdraw and deposit methods, to reuse their code.
		private void transferMenu(){
			int chosenCustomer = whichCustomer();
			if (chosenCustomer < 0){
				return;
			}
			double discountPercentage = myBank.getCustomer(chosenCustomer).getDiscountPercentage();
			double quantity = 0.0;
			System.out.println("ENTER THIS FOR THE RECEIVING ACCOUNT");
			int receivingAccount = whichAccount(myBank.getCustomer(chosenCustomer));
			System.out.println("ENTER THIS FOR THE SENDING ACCOUNT");
			int sendingAccount = whichAccount(myBank.getCustomer(chosenCustomer));
			if (sendingAccount >= 0 && receivingAccount >= 0){
				System.out.println("Enter transfer amount:");
				try {
					quantity = Double.parseDouble(scan.nextLine());
				}
				catch( NumberFormatException e){
					System.out.println("Invalid amount. You need to enter a number as your amount.");
					quantity = 0.0;
				}
			myBank.getCustomer(chosenCustomer).getAccount(sendingAccount).withdraw(quantity, discountPercentage);
			myBank.getCustomer(chosenCustomer).getAccount(receivingAccount).deposit(quantity, discountPercentage);
			System.out.println("Transfer Success!");			
			}
		}
}
