// File Name:		Account.java
// Developers:		Jacob Sanz-Robinson (260706158)
// Purpose:			Provides attribute stuff, and importantly the withdraw and deposit methods which the whole program will rely on.
// Inputs: 			None
// Outputs: 		None
// Modifications:	None
// Date:			4th Feb. 2018

public class Account{
	
	private double balance = 0.0;
	private static String accountType;
	
	//A simple constructor, setter, and getter. Shouldn't need more documentation.
	Account(){
		this.balance = balance;
	}
	
	public double getBalance(){
		return balance;
	}
	
	public void setBalance(double balance){
		this.balance = balance;
	}
	
	// Name:			withdraw
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Decrements balance of money in an account
	// Inputs: 			Two doubles, a quantity and a discount percentage to modify the set $1 fee.
	// Outputs:			None
	// Side-effects:	Prints prompts, modifies balance attribute.
	// Special notes:	None	
	public void withdraw(double quantity, double discountPercentage){
		double charge = 1.0 - discountPercentage/100.0;
		if (quantity < 0.0){
			System.out.println("If I were a real bank, I'd definitely be taking all your money right now.");
			return;
		}
		if ((quantity + charge) > balance){
			System.out.println("Not enough money for withdrawal.");
			return;
		} else {
			balance -= (quantity + charge);
			System.out.println("Withdrawal success. Bling bling.");
			return;
		}
	}
	
	// Name:			deposit
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Increments balance of money in an account
	// Inputs: 			Two doubles, a quantity and a discount percentage to modify the set $1 fee.
	// Outputs:			None
	// Side-effects:	Prints prompts, modifies balance attribute.
	// Special notes:	None
	public void deposit(double quantity, double discountPercentage){
		double award = 1.0 + discountPercentage/100.0;
		if (quantity < 0.0){
			System.out.println("Invalid. Enter a positive quantity.");
			return;
		} else {
			quantity += award;
			balance += quantity;
			System.out.println("Deposit success!");
		}
		
	}



}
