// File Name:		Bank.java
// Developers:		Jacob Sanz-Robinson (260706158)
// Purpose:			Launches the application from main. Provides an Arraylist stucture to store all customers
// Inputs: 			None
// Outputs: 		None
// Modifications:	None
// Date:			4th Feb. 2018

import java.util.*;

public class Bank{
	
	ArrayList<Customer> customerList = new ArrayList<Customer>();
	
	// Name:			main
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Creates and displays a menu object.
	// Inputs: 			None
	// Outputs:			None
	// Side-effects:	See the Menu class.
	// Special notes:	None
	public static void main(String[] args){
		Menu myMenu = new Menu();
		myMenu.displayMenu();
		}
	
	// Name:			newCustomer
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Adds a customer to the banks arraylist of customers
	// Inputs: 			None
	// Outputs:			None
	// Side-effects:	Changes the customerList.
	// Special notes:	None
	void newCustomer(Customer customer){
		customerList.add(customer); 
	}
	
	//Below are two getters, these should not require further documentation.
	Customer getCustomer(int identifier){
		return customerList.get(identifier);
	}
	
	ArrayList<Customer> getAllCustomers(){
		return customerList;
	}
	
		
}
