// File Name:		Customer.java
// Developers:		Jacob Sanz-Robinson (260706158)
// Purpose:			Contains all attributes and setters/getter for these. Most notably, stores accounts as an Arraylist.
// Inputs: 			None
// Outputs: 		None
// Modifications:	None
// Date:			4th Feb. 2018

import java.util.*;

public class Customer{
	
	private final String customerName;
	private int customerNumber;
	private double discountPercentage = 2.0;
	private static int startingNumber = 0; //A starting number to generate unique accounts IDs. Should coincide with position in arraylist.
	ArrayList<Account> accountList = new ArrayList<Account>();
	
	// Name:			Customer
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Constructor for a Customer
	// Inputs: 			String name
	// Outputs:			None
	// Side-effects:	Creates an object with initialized attributes. Prints a prompt.
	// Special notes:	None
	Customer(String customerName){
		this.customerName = customerName;
		this.discountPercentage = discountPercentage;
		customerNumber = startingNumber++;
		System.out.println("Note this down, your unique customer number is " + customerNumber);
		this.accountList = accountList;
	}

	// Name:			newAccount
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Adds an account to the customers arraylist of accounts
	// Inputs: 			None
	// Outputs:			None
	// Side-effects:	Changes the accountList. Prints a prompt.
	// Special notes:	None	
	void newAccount(Account account){
		accountList.add(account);
		System.out.println("Account added to customer. Note this down, this accounts number is " + (accountList.size()-1));
	}
	
	//Below are some setters and getters that shouldn't require further documentation.
	Account getAccount(int identifier){
		return accountList.get(identifier);
	}
	
	ArrayList<Account> getAllAccounts(){
		return accountList;
	}
	
	public double getDiscountPercentage(){
		return discountPercentage;
	}
	
	public void setDiscountPercentage(double discountPercentage){
		this.discountPercentage = discountPercentage;
	}

	public int getCustomerNumber(){
		return customerNumber;
	}
	
}
