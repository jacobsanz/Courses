// File Name:		PinTest.java
// Developers:		Jacob Sanz-Robinson (260706158)
// Purpose:			Test the public methods in Pin.java. As specified in class, this indirectly tests the private methods, too.
// Inputs: 			None
// Outputs: 		None
// Modifications:	None
// Date:			18th Jan 2018

public class PinTest{

	// Name:			main
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Tests the public functions in the Pin class. From the assignment instructions: "each method from Pin.java will have a corresponding testing loop within PinTest.java".
	// Inputs: 			String[] args - not used.
	// Outputs:			None.
	// Side-effects:	Prints the result of each case tested to the screen.
	// Special notes:	The hidden funtions are tested within their public wrappers. The void function with no ins/outs was strange to test, but succesfully stores the last valid pin generated, so seems to be working fine by these tests.
	public static void main(String[] args){
		int pin = -128; //we start testing with a few negative cases, because why not, and work our way up to three digit positives.
		String strPin = "";
		boolean result;
		int resultArray[] = new int[2];
		Pin thePin = new Pin();

		System.out.println("Test case for method checkOld");
		while (pin < 999){
			strPin = Integer.toString(pin);
			result = thePin.checkOld(strPin);
			System.out.println("checkOld Argument: " + strPin + " Result: " + result);
			System.out.println("");

			strPin = "0" + Integer.toString(pin); //Add 0 in front of each test case. We can see 00 --> true, and test the '-' character.
			result = thePin.checkOld(strPin);
			System.out.println("checkOld Argument: " + strPin + " Result: " + result);
			System.out.println("");

			pin++;
		}

		System.out.println("Test case for method checkNew");
		pin = -128;
		while (pin < 128){
			strPin = Integer.toString(pin);
			result = thePin.checkNew(strPin);
			System.out.println("checkNew Argument: " + strPin + " Result: " + result);
			System.out.println("");

			strPin = "0" + Integer.toString(pin); //explained in previous method.
			result = thePin.checkNew(strPin);
			System.out.println("checkNew Argument: " + strPin + " Result: " + result);
			System.out.println("");

			pin++;
		}

		System.out.println("Test case for private method StringToArray"); //Recall he method stores -1 to indicate a non-integer input.
		pin = -128;
		while (pin < 128){
			strPin = Integer.toString(pin);
			thePin.StringToArrayWrapper(strPin, resultArray);
			System.out.println("StringToArray Argument: " + strPin + " Result[0]: " + resultArray[0]+ " Result[1]: " + resultArray[1]);
			System.out.println("");

			strPin = "0" + Integer.toString(pin); //explained in previous method.
			thePin.StringToArrayWrapper(strPin, resultArray);
			System.out.println("StringToArray Argument: " + strPin + " Result[0]: " + resultArray[0]+ " Result[1]: " + resultArray[1]);
			System.out.println("");

			pin++;
		}

		System.out.println("Test case for private method IsValidPin");
		pin = -128;
		while (pin < 128){
			strPin = Integer.toString(pin);
			result = thePin.IsValidPinWrapper(strPin); //We are testing the private method in a wrapper.
			System.out.println("IsValidPin Argument: " + strPin + " Result: " + result);
			System.out.println("");

			strPin = "0" + Integer.toString(pin); //Purpose explained in previous method.
			result = thePin.IsValidPinWrapper(strPin);
			System.out.println("IsValidPin Argument: " + strPin + " Result: " + result);
			System.out.println("");

			pin++;
		}



		System.out.println("Test case for method updatePin"); //Strange, because it's just a void function with no arguments. Uses private fields.
		thePin.updatePin(); //will print the last valid PIN (99) it came across in the checkNew function.

	}
}
