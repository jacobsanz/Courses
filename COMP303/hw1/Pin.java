// File Name:		Pin.java
// Developers:		Jacob Sanz-Robinson (260706158)
// Purpose:			Provide hidden and API methods for PinMain. Ensures program would also work if looped, as opposed to terminating.
// Inputs: 			None
// Outputs: 		None
// Modifications:	None
// Date:			17th Jan 2018

public class Pin{

	private int currentPin[] = new int[2];
	private int oldPin[] = new int[2];
	private int newPin[] = new int[2];

	// Name:			Pin
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Constructor method. Initializes the oldPin to 00 when an object is created.
	// Inputs: 			None
	// Outputs:			None
	// Side-effects:	Modifies the private oldPin variable in the Pin class.
	// Special notes:	None.	
	public Pin(){ //constructor
		this.oldPin[0] = 0;
		this.oldPin[1] = 0;
    }

	// Name:			IsValidPin
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Checks if inputted string pin is valid (two digits, each between 0-9).
	// Inputs: 			String inputPin - the string to be checked.
	// Outputs:			A boolean. True if valid, false otherwise.
	// Side-effects:	None
	// Special notes:	Hidden, used by other methods in class.
	private static boolean IsValidPin(String inputPin){
		if (inputPin.length() == 2){
			int firstDigit = Character.getNumericValue(inputPin.charAt(0)); //utily returns numeric value, or -1.
			int secondDigit = Character.getNumericValue(inputPin.charAt(1));
			if (firstDigit >= 0 && firstDigit <= 9 && secondDigit >= 0 && secondDigit <= 9){ //checks each digit is within valid range.
				return true;
			}
		}
		return false;
	}

	// Name:			IsValidPinWrapper
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Wraps IsValidPin for testing.
	// Inputs: 			String inputPin - the string to be checked.
	// Outputs:			A boolean. True if valid, false otherwise.
	// Side-effects:	None
	// Special notes:	For testing only.	
	public boolean IsValidPinWrapper(String inputPin){
		boolean result = IsValidPin(inputPin);
		return result;
	}

	// Name:			StringToArray
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Stores the numeric value of the first two characters of the inputted string as the first two elements of an integer array. Stores -1 if not a numeric value.
	// Inputs: 			String inputPin - a user inputted string in PinMain. int[] arr - array the pin is being written to.
	// Outputs:			None.
	// Side-effects:	Modifies the inputted array. In this case meant to be the pin class's private vars.
	// Special notes:	Hidden, used by other methods in class.
	private static void StringToArray(String inputPin, int[] arr){
		if (IsValidPin(inputPin)){		
			int firstDigit = Character.getNumericValue(inputPin.charAt(0));
			int secondDigit = Character.getNumericValue(inputPin.charAt(1));
			arr[0] = firstDigit;
			arr[1] = secondDigit;
		} else {
			arr[0] = -1; //deals with the case that the pin is a single digit in PinTest.
			arr[1] = -1;
		}
		return;
	}

	// Name:			StringToArrayWrapper
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Wraps IsValidPin for testing.
	// Inputs: 			String inputPin - the string to be checked.
	// Outputs:			A boolean. True if valid, false otherwise.
	// Side-effects:	None
	// Special notes:	For testing only.	
	public void StringToArrayWrapper(String inputPin, int[] arr){
		StringToArray(inputPin, arr);
		return;
	}

	// Name:			checkOld
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Checks if inputted string pin is valid, and if it is the same as the old pin.
	// Inputs: 			String inputPin - the string to be checked.
	// Outputs:			A boolean. True if input is valid and the same as the old pin. Else, false.
	// Side-effects:	The Pin Class's currentPin private var is modified.
	// Special notes:	None
	public boolean checkOld(String inputPin){
		if (IsValidPin(inputPin)){
			StringToArray(inputPin, this.currentPin);
			if (this.oldPin[0] == this.currentPin[0] && this.oldPin[1] == this.currentPin[1]){ //digit-wise comparison of old and current pin.				
				return true;
			}
		}	
		return false;
	}

	// Name:			checkNew
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Checks if inputted string pin is valid, and if it is the same as the current pin.
	// Inputs: 			String inputPin - the string to be checked.
	// Outputs:			A boolean. True if input is valid and different to the old pin. Else, false.
	// Side-effects:	The Pin Class's newPin private var is modified. 
	// Special notes:	None
	public boolean checkNew(String inputPin){
		if (IsValidPin(inputPin)){
			StringToArray(inputPin, this.newPin);
			if (this.newPin[0] == this.currentPin[0] && this.newPin[1] == this.currentPin[1]){ //digit-wise comparison of new and current pins.
				return false;
			}
			return true;
		}
		
		return false;
	}

	// Name:			updatePin
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Updates the old pin to store the current pin, and the current pin o store the newly inputted pin.
	// Inputs: 			None
	// Outputs:			None.
	// Side-effects:	The Pin Class's currentPin and oldPin private vars are modified. Prints prompt demonstrating the update.
	// Special notes:	None
	public void updatePin(){
		this.oldPin[0] = this.currentPin[0];		//this would make the program work if it were to be looped after a pin change.
		this.oldPin[1] = this.currentPin[1];
		this.currentPin[0] = this.newPin[0];
		this.currentPin[1] = this.newPin[1];
		System.out.println("Your pin has been updated to " + this.currentPin[0] + this.currentPin[1]); //Having this print statement inside the method as a side effect simplifies the PinMain and PinTest classes, as the array is private.
		return;
	}
}
