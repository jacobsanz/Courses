// File Name:		PinMain.java
// Developers:		Jacob Sanz-Robinson (260706158)
// Purpose:			Creates/modifies a 2 digit pin object with user input. As specified in class, would also work if looped (instead of terminated).
// Inputs: 			User args
// Outputs: 		Prints prompts, creates and modifies an object.
// Modifications:	None
// Date:			17th Jan 2018

import java.util.*;

public class PinMain{

	// Name:			main
	// Developers:		Jacob Sanz-Robinson
	// Purpose:			Guides user to update their pin by calling functions from the Pin class, and displaying prompts.
	// Inputs: 			String[] args - not used. Uses a scanner object to obtain the data.
	// Outputs:			None.
	// Side-effects:	Prints multiple prompts, calls methods that modify the object's private vars.
	// Special notes:	None.
	public static void main(String[] args){	
		System.out.println("Welcome to pin update!");
		Pin thePin = new Pin();
		Scanner s = new Scanner(System.in);
		String newpin = "";
		String oldpin = "";
		boolean oldStatus = false; //these will indicate when to stop our loops.
		boolean newStatus = false;

		while (oldStatus != true){
			System.out.println("Please input your old pin:");
			oldpin = s.nextLine();
			if (thePin.checkOld(oldpin)){			
					oldStatus = true;
					System.out.println("Old pin confirmed.");	
			} else {
				System.out.println("That is not your old pin.");
			}
		}

		while (newStatus != true){
			System.out.println("Please input your new pin:");
			newpin = s.nextLine();
			if (thePin.checkNew(newpin)){
				newStatus = true;
			} else {
				System.out.println("Invalid pin.");
			}
		}
		System.out.println("New pin confirmed.");
		thePin.updatePin();	//Updates the variables already within the object, so no inputs needed.
		
	}
}
