/**
 * 
 * File Name:  PinTest.java
 * Developers:  Jacob Sanz-Robinson (260706158)
 * Purpose:              jUnit tests for the methods in Pin.java
 * Inputs:   None
 * Outputs:   None
 * Modifications: None
 * Date:   22nd February 2018
*/

import org.junit.Test;
import static org.junit.Assert.*;
import java.lang.reflect.*;

/**
 * Please note that the constructor method has no inputs, and other than initializing an array to [0][0] performs no further mutations. It doesn't really require testing.
 */
public class PinTest {
    
     /**
     * Test of constructor
     */
    public PinTest() {
    }
 

    /**
     * Test of IsValidPinWrapper method, of class Pin.
     */
    @Test
    public void testIsValidPinWrapper() {
        System.out.println("IsValidPinWrapper");
        String inputPin = "";
        boolean expResult = false;
        int pin = -128;
        Pin instance = new Pin(); //in this case, only need to create one instance
        while (pin < 128){
                expResult = false;
                inputPin = Integer.toString(pin);
                if( pin < 100 && pin >= 0 ){
                    expResult = true;
                }
                if ( pin < 10 && pin >= 0 ){
                    inputPin = "0" + Integer.toString(pin);
                }
                boolean result = instance.IsValidPinWrapper(inputPin); //We are testing the private method in a wrapper.
                assertEquals(expResult, result);
                pin++;
        }
    }

    /**
     * Test of StringToArrayWrapper method, of class Pin.
     */
    @Test
    public void testStringToArrayWrapper() {
        System.out.println("StringToArrayWrapper");
        String inputPin = "";
        int resultArray[] = new int[2];
        Pin instance = new Pin();
        instance.StringToArrayWrapper(inputPin, resultArray);
        int pin = -128;
        boolean expResult = false;
        while (pin < 128){
                expResult = true;
                inputPin = Integer.toString(pin);
                if ( pin < 10 && pin >= 0 ){
                    inputPin = "0" + Integer.toString(pin);
                }
                instance.StringToArrayWrapper(inputPin, resultArray);
                if(Character.getNumericValue(inputPin.charAt(0)) == resultArray[0] && Character.getNumericValue(inputPin.charAt(1)) == resultArray[1] ){
                    expResult = true;
                }
                assertEquals(expResult, true);
                pin++;
        }
    }

    /**
     * Test of checkOld method, of class Pin.
     */
    @Test
    public void testCheckOld() {
        System.out.println("checkOld");
        String inputPin = "";
        boolean expResult = false;
        boolean result;
        int pin = -128;
        while (pin < 128){
                expResult = false;
                inputPin = Integer.toString(pin);
                Pin instance = new Pin();
                if ( pin < 10 && pin >= 0 ){
                    inputPin = "0" + Integer.toString(pin);
                }
                if (inputPin.equals("00")){ //always equal to the last value, in this case, initialized to 00.
                    expResult = true;
                }
                result = instance.checkOld(inputPin);
                assertEquals(expResult, result);
                pin++;
        }
    }

    /**
     * Test of checkNew method, of class Pin.
     */
    @Test
    public void testCheckNew() {
        System.out.println("checkNew");
        String inputPin = "";
        boolean result;
        boolean expResult;
        int pin = -128;
        while (pin < 128){
                expResult = true;
                inputPin = Integer.toString(pin);
                Pin instance = new Pin();
                if (pin < 0 || pin > 99){
                    expResult = false;
                }
                if ( pin < 10 && pin >= 0 ){
                    inputPin = "0" + Integer.toString(pin);
                }
                if (inputPin.equals("00")){ //not equal to the previous value, in this case, for a new Pin object, 00.
                    expResult = false;
                }
                result = instance.checkNew(inputPin);
                assertEquals(expResult, result);
                pin++;
        }
    }

    /**
     * Test of updatePin method, of class Pin.
     */
    @Test
    public void testUpdatePin() { 
    //updatePin calls a method to test pin validity (which was tested above), then updates arrays.
    //I found the best way to check it was to pass in pin values using checkNew, update the pin twice to push these values down to the "old" array, where checkOld could test them.
    //From this we can see that out of all the pins tested, changes only happened when the 100 valid ones were tested. The method's print statement corroborates this.
        System.out.println("updatePin");
        String inputPin = ""; 
        Pin instance = new Pin();
        int result = 0;
        boolean holder;
        int expResult = 0;
        int pin = -128;
        while (pin < 128){
                inputPin = Integer.toString(pin);
                if (pin >= 0 && pin < 100){
                    expResult += 1;
                }                
                if ( pin < 10 && pin >= 0 ){
                    inputPin = "0" + Integer.toString(pin);
                }
                holder = instance.checkNew(inputPin);
                instance.updatePin();
                instance.updatePin();
                if (instance.checkOld(inputPin)){
                  result += 1;
                }
                pin++;
        }
        assertEquals(expResult, result);
    }
    
}
