/**
 * 
 * File Name:  PinTestRunner.java
 * Developers:  Jacob Sanz-Robinson (260706158)
 * Purpose:              runs PinTest.java jUnit tests.
 * Inputs:   None
 * Outputs:   None
 * Modifications: None
 * Date:   22nd February 2018
*/


import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class PinTestRunner {
   public static void main(String[] args) {
      Result result = JUnitCore.runClasses(PinTest.class);
      for (Failure failure : result.getFailures()) {
         System.out.println(failure.toString());
      }
      System.out.println(result.wasSuccessful());
   }
}
