#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\usepackage{fullpage}
\usepackage{amsfonts}
\usepackage{nicefrac}


\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\end_preamble
\use_default_options false
\maintain_unincluded_children false
\language english
\language_package none
\inputencoding auto
\fontencoding default
\font_roman "palatino" "default"
\font_sans "default" "default"
\font_typewriter "mathpazo" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 12
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 0
\use_package cancel 0
\use_package esint 1
\use_package mathdots 0
\use_package mathtools 0
\use_package mhchem 0
\use_package stackrel 0
\use_package stmaryrd 0
\use_package undertilde 0
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
Student: Jacob Sanz-Robinson
\end_layout

\begin_layout Standard
McGill ID: 260706158
\end_layout

\begin_layout Standard
Course Title: Discrete Structures I
\end_layout

\begin_layout Standard
Course number and section: 240-001
\end_layout

\begin_layout Standard
Assignment number: 2
\end_layout

\begin_layout Standard
Professor: Philip Evan Decorte
\end_layout

\begin_layout Standard
Date: Fall 2016 - 21/10/16.
\end_layout

\begin_layout Section*
Math 240: Homework Assignment 3 - Mathematical induction and number theory.
\end_layout

\begin_layout Standard
Third assignment...By this point \SpecialChar LaTeX
 seems like something a human might occasionally
 almost find useful.
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Enumerate
Suppose not, so 
\begin_inset Formula $\sqrt{91}$
\end_inset

 is rational.
\begin_inset Newline newline
\end_inset

There exist 
\begin_inset Formula $a,b\in\mathbb{Z}$
\end_inset

 with 
\begin_inset Formula $b\neq0$
\end_inset

, such that 
\begin_inset Formula $\sqrt{91}=\frac{a}{b}$
\end_inset

.
\begin_inset Newline newline
\end_inset

Assume a and b are written in their lowest terms (
\shape italic
gcd(a,b) = 1
\shape default
).
\begin_inset Newline newline
\end_inset


\begin_inset Formula $91=\frac{a^{2}}{b^{2}}$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $91b^{2}=a^{2}$
\end_inset

, which implies that 
\begin_inset Formula $91|a^{2}$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $91=7\cdotp13$
\end_inset

, so 
\begin_inset Formula $91|a$
\end_inset

, as 7 and 13 are prime.
\begin_inset Newline newline
\end_inset

Let a = 91k, 
\begin_inset Formula $k\in\mathbb{Z}$
\end_inset

.
\begin_inset Newline newline
\end_inset


\begin_inset Formula $91b^{2}=a^{2}=91^{2}k^{2}$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $91b^{2}=91^{2}k^{2}$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $b^{2}=91k^{2}$
\end_inset

, which implies 
\begin_inset Formula $91|b$
\end_inset

.
\begin_inset Newline newline
\end_inset

Therefore 
\begin_inset Formula $gcd(a,b)\neq1$
\end_inset

, so as a and b are not written in their assumed lowest terms there is a
 contradiction, and 
\begin_inset Formula $\sqrt{91}$
\end_inset

 is irrational.
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Enumerate
\begin_inset Formula $ $
\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
Assume x and y are rational numbers.
 By definition, x and y can be written as a quotient of two integers.
\begin_inset Newline newline
\end_inset

Let 
\begin_inset Formula $x=\frac{a}{b}$
\end_inset

 and 
\begin_inset Formula $y=\frac{i}{j}$
\end_inset

, where 
\begin_inset Formula $a,b,i,j\in\mathbb{Z}$
\end_inset

.
\begin_inset Newline newline
\end_inset

The product xy can be written 
\begin_inset Formula $xy=\frac{a}{b}\cdotp\frac{i}{j}=\frac{ai}{bj}$
\end_inset

.
\begin_inset Newline newline
\end_inset

As this product is a ratio of integers, it is also rational.
\end_layout

\begin_layout Enumerate
Assume 
\begin_inset Formula $x\neq0$
\end_inset

 is irrational, or y is rational.
\begin_inset Newline newline
\end_inset

If xy is rational, it can be written as a quotient of integers as above,
 say 
\begin_inset Formula $xy=\frac{a}{b}$
\end_inset


\begin_inset Newline newline
\end_inset


\shape italic
Case 1:
\shape default

\begin_inset Newline newline
\end_inset

If x is irrational, x cannot be represented as a quotient of 2 integers.
 As x is non-zero, the product xy cannot be zero, and is therefore also
 irrational.
 This is the case that acts as a proof for statement.
\begin_inset Newline newline
\end_inset


\shape italic
Case 2:
\shape default

\begin_inset Newline newline
\end_inset

If x is rational, say it can be represented as 
\begin_inset Formula $x=\frac{i}{j}.$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $xy=\frac{a}{b}$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $y=\frac{a}{bx}=\frac{a}{b(\frac{i}{j})}=\frac{aj}{bi}$
\end_inset

, so y is rational.
\begin_inset Newline newline
\end_inset


\end_layout

\end_deeper
\begin_layout Enumerate
For the first case, we assume 
\begin_inset Formula $\sqrt{2}^{\sqrt{2}}$
\end_inset

is rational.
\begin_inset Newline newline
\end_inset

If this were true, then we would be done, and 
\begin_inset Formula $x=y=\sqrt{2}$
\end_inset

.
\begin_inset Newline newline
\end_inset

The second case is where 
\begin_inset Formula $\sqrt{2}^{\sqrt{2}}$
\end_inset

is irrational.
\begin_inset Newline newline
\end_inset

We take 
\begin_inset Formula $x=\sqrt{2}^{\sqrt{2}}$
\end_inset

and 
\begin_inset Formula $y=\sqrt{2}$
\end_inset

.
\begin_inset Newline newline
\end_inset

So 
\begin_inset Formula $x^{y}=(\sqrt{2}^{\sqrt{2}})^{\sqrt{2}}=\sqrt{2}^{2}=2$
\end_inset

, which is rational.
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Enumerate
\begin_inset Formula $ $
\end_inset


\begin_inset Newline newline
\end_inset


\end_layout

\begin_deeper
\begin_layout Enumerate
\begin_inset Tabular
<lyxtabular version="3" rows="6" columns="2">
<features tabularvalignment="middle">
<column alignment="left" valignment="top">
<column alignment="left" valignment="top">
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
gcd(2016, 208)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
= gcd(208, 144)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
(208 x 9) + 144 = 2016
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
= gcd(144, 64)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
(144 x 1) + 64 = 208
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
= gcd(64, 16)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
(64 x 2) + 16 = 144
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
= gcd(16, 0)
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
(16 x 4) + 0 = 64
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
gcd = 16
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\end_layout

\begin_layout Enumerate
These integers do not exist, as 
\begin_inset Formula $\frac{1000}{16}$
\end_inset

will not yield an integer answer.
\end_layout

\begin_layout Enumerate
From part (a) we know: 
\begin_inset Newline newline
\end_inset


\begin_inset Tabular
<lyxtabular version="3" rows="5" columns="2">
<features tabularvalignment="middle">
<column alignment="center" valignment="top">
<column alignment="left" valignment="top">
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
16 = 144 - (64 x 2)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
16 = 144 - (208 - 144) x 2
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
16 = (144 x 3) - (208 x 2)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
16 = 3x(2016 - (208 x 9)) - (208 x 2)
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
16 = (3 x 2016) - (208 x 29)
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset

1024 = 64 x 16
\begin_inset Newline newline
\end_inset

1024 = 64 x [(3 x 2016) - (29 x 208)]
\begin_inset Newline newline
\end_inset

1024 = (192 x 2016) - (1856 x 208)
\begin_inset Newline newline
\end_inset

So a = 192, and b = 1856.
\end_layout

\end_deeper
\begin_layout Enumerate

\series bold
Base case, n = 0:
\series default

\begin_inset Newline newline
\end_inset


\begin_inset Formula $\frac{3^{(0+1)}-1}{2}=1$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Newline newline
\end_inset


\series bold
Induction step, n = k+1:
\series default

\begin_inset Newline newline
\end_inset


\begin_inset Formula $1+3+3^{2}+3^{3}+\ldots+3^{k}=\frac{3^{k+1}-1}{2}$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $1+3+3^{2}+3^{3}+\ldots+3^{k}+3^{k+1}=\frac{3^{k+2}-1}{2}$
\end_inset


\begin_inset Newline newline
\end_inset

We want to prove the step above, for k+1:
\begin_inset Newline newline
\end_inset


\begin_inset Formula $1+3+3^{2}+3^{3}+\ldots+3^{k}+3^{k+1}=\frac{3^{k+1}-1}{2}+3^{k+1}$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $1+3+3^{2}+3^{3}+\ldots+3^{k}+3^{k+1}=\frac{3^{k+1}-1}{2}+\frac{2\cdotp3^{k+1}}{2}$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $1+3+3^{2}+3^{3}+\ldots+3^{k}+3^{k+1}=\frac{3\cdotp3^{k+1}-1}{2}=\frac{3^{k+2}-1}{2}$
\end_inset


\begin_inset Newline newline
\end_inset

So it holds true for n = k+1.
\begin_inset Newline newline
\end_inset


\end_layout

\begin_layout Enumerate

\end_layout

\begin_deeper
\begin_layout Enumerate

\series bold
Base Case, a = 1:
\series default

\begin_inset Newline newline
\end_inset


\begin_inset Formula $a_{0}\cdotp10^{0}\equiv a_{0}$
\end_inset

(mod 9)
\begin_inset Newline newline
\end_inset


\begin_inset Formula $1\equiv1$
\end_inset

(mod 9)
\begin_inset Newline newline
\end_inset


\series bold
Inductive Step, a = k+1:
\series default

\begin_inset Newline newline
\end_inset


\begin_inset Formula $(a_{0}\cdotp10^{0})+(a_{1}\cdotp10^{1})+\ldots+(a_{n}\cdotp10^{n})\equiv a_{0}+a_{1}+\ldots+a_{n}$
\end_inset

(mod 9)
\begin_inset Newline newline
\end_inset


\begin_inset Formula $(a_{0}\cdotp10^{0})+\ldots+(a_{k}\cdotp10^{k})+(a_{k+1}\cdotp10^{k+1})\equiv a_{0}+\ldots+a_{k}+a_{k+1}$
\end_inset

(mod 9)
\begin_inset Newline newline
\end_inset

We know that 
\begin_inset Formula $10^{n}\equiv1$
\end_inset

(mod 9) as a property of congruences:
\begin_inset Newline newline
\end_inset


\begin_inset Formula $(a_{0}\cdotp1)+\ldots+(a_{k}\cdotp1)+(a_{k+1}\cdotp1)\equiv a_{0}+\ldots+a_{k}+a_{k+1}$
\end_inset

(mod 9)
\begin_inset Newline newline
\end_inset


\begin_inset Formula $a_{0}+\ldots+a_{k}+a_{k+1}\equiv a_{0}+\ldots+a_{k}+a_{k+1}$
\end_inset

(mod 9)
\begin_inset Newline newline
\end_inset

So it holds true for n = k+1.
\end_layout

\end_deeper
\begin_layout Enumerate

\end_layout

\begin_deeper
\begin_layout Enumerate
Let P(n) be the sentence 
\begin_inset Quotes eld
\end_inset

The sum of the digits of 9n after k iterations,
\begin_inset Formula $n_{k}$
\end_inset

, is 9 for any large enough k
\begin_inset Quotes erd
\end_inset

.
\begin_inset Newline newline
\end_inset


\series bold
Base Case, P(1), and continued iterations:
\series default
\shape italic

\begin_inset Newline newline
\end_inset


\shape default
Here we notice that 
\begin_inset Formula $n_{k}$
\end_inset

 decreases, and becomes constant when its value is 9.
\shape italic

\begin_inset Newline newline
\end_inset

Example 1:
\shape default

\begin_inset Newline newline
\end_inset


\begin_inset Formula $P(1)=n_{1}=9$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $n_{k}=9$
\end_inset


\begin_inset Newline newline
\end_inset


\shape italic
Example 2:
\shape default

\begin_inset Newline newline
\end_inset


\begin_inset Formula $P(2)=n_{1}=18$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $n_{2}=9$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $n_{k}=9$
\end_inset


\begin_inset Newline newline
\end_inset


\shape italic
Example 3:
\shape default

\begin_inset Newline newline
\end_inset


\begin_inset Formula $P(27699)=n_{1}=249291$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $n_{2}=27$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $n_{3}=9$
\end_inset


\begin_inset Newline newline
\end_inset


\begin_inset Formula $n_{k}=9$
\end_inset


\begin_inset Newline newline
\end_inset


\series bold
Induction Step, P(k+1):
\series default

\begin_inset Newline newline
\end_inset

By the definition of the induction hypothesis (the sentence statement of
 P(n)), and by (a), it is clear that 
\begin_inset Formula $9|n_{k}$
\end_inset

.
\begin_inset Newline newline
\end_inset

As such 
\begin_inset Formula $n_{k}\equiv0$
\end_inset

(mod 9).
\begin_inset Newline newline
\end_inset

If n > 0, producing 
\begin_inset Formula $n_{k}$
\end_inset

, there will always be a numerical return, which will be 
\begin_inset Formula $n_{k+1}$
\end_inset

.
\begin_inset Newline newline
\end_inset

To prove P(k+1), as 
\begin_inset Formula $9|n_{k}$
\end_inset

, then 
\begin_inset Formula $9|n_{k+1}-n_{k}$
\end_inset

.
\begin_inset Newline newline
\end_inset

Therefore 
\begin_inset Formula $n_{k+1}\equiv n_{k}$
\end_inset

(mod 9).
\begin_inset Newline newline
\end_inset

So as 
\begin_inset Formula $n_{k}\equiv0$
\end_inset

 (mod 9), then 
\begin_inset Formula $n_{k+1}\equiv0$
\end_inset

 (mod 9).
 In addition, we know that the value of 
\begin_inset Formula $n_{k}$
\end_inset

 tends to 9.
\begin_inset Newline newline
\end_inset

So P(k+1) holds true.
\end_layout

\end_deeper
\begin_layout Enumerate
Let P(n) be the sentence 
\begin_inset Quotes eld
\end_inset

There is a survivor when (2n+1) people throw a cake at the nearest person.
\begin_inset Quotes erd
\end_inset

 
\begin_inset Newline newline
\end_inset


\series bold
Base Case, P(1):
\series default

\begin_inset Newline newline
\end_inset

When n = 1, 2n+1 = 3, so 3 people are involved.
 Let's call them A, B, and C.
 If we assume A and B are closest to each other:
\begin_inset Newline newline
\end_inset

(distance A-C) > (distance B-C) > (distance A-B).
\begin_inset Newline newline
\end_inset

Thus, A and B throw their cake at each other, and C could throw a cake at
 either A or B.
\begin_inset Newline newline
\end_inset

C has not had cake thrown at him, showing P(1) is true.
\begin_inset Newline newline
\end_inset


\series bold
Induction Step, P(k+1):
\series default

\begin_inset Newline newline
\end_inset

We want to show that 
\begin_inset Formula $\forall k(P(k)\rightarrow P(k+1))$
\end_inset

.
\begin_inset Newline newline
\end_inset

We will begin by assuming that P(k) is true, so there is at least one survivor.
 If we assume n = (k+1), there are 2(k+1)+1 = 2k+3 people.
\begin_inset Newline newline
\end_inset

Let two people, A and B, be the closest pair in the 2k+3 people, then there
 are 2 posssible cases:
\begin_inset Newline newline
\end_inset


\shape italic
Case 1:
\shape default

\begin_inset Newline newline
\end_inset

Here someone throws a cake at A or B.
 This means that 3 cakes are thrown at A and B (as they throw cakes at each
 other).
\begin_inset Newline newline
\end_inset

Therefore, at most (2k+3-3) = 2k cakes are thrown at the remaining people.
\begin_inset Newline newline
\end_inset

As there was an odd number of people involved, there must be at least one
 survivor, and possibly more.
\begin_inset Newline newline
\end_inset


\shape italic
Case 2:
\shape default

\begin_inset Newline newline
\end_inset

here nobody throws a cake at A or B.
 Besides A and B, there are 2k+1 people involved.
 By the induction hypothesis (The original statement of P(n)), when there
 are 2k+1 people involved, there is at least one survivor.
\begin_inset Newline newline
\end_inset

So in both cases P(n) holds true.
\end_layout

\begin_layout Enumerate
Let P(n) be the sentence 
\begin_inset Quotes eld
\end_inset


\begin_inset Formula $n\geq24$
\end_inset

 cent postage can be obtained using 5 and 7 cent stamps.
\begin_inset Quotes erd
\end_inset


\begin_inset Newline newline
\end_inset


\series bold
Base Case(s):
\series default

\begin_inset Newline newline
\end_inset

P(24) = 7 + 7 + 5 + 5
\begin_inset Newline newline
\end_inset

P(25) = 5 + 5 + 5 + 5 + 5
\begin_inset Newline newline
\end_inset

P(26) = 7 + 7 + 7 + 5
\begin_inset Newline newline
\end_inset

P(27) = 5 + 5 + 5 + 5 + 7
\begin_inset Newline newline
\end_inset

P(28) = 7 + 7 + 7 + 7
\begin_inset Newline newline
\end_inset


\series bold
Induction Step, P(k+1):
\series default

\begin_inset Newline newline
\end_inset

We want to show that 
\begin_inset Formula $\forall k(P(k)\rightarrow P(k+1))$
\end_inset

.
\begin_inset Newline newline
\end_inset

As 
\begin_inset Formula $n\geq24$
\end_inset

, P(24) shown in the Base Cases will use at least 2x7 cent stamps.
 It is clear there are 2 possible cases to obtained k+1 cents of postage.
\begin_inset Newline newline
\end_inset


\shape italic
Case 1:
\shape default

\begin_inset Newline newline
\end_inset

If P(k) can be obtained with at least 2x7 cent stamps, these can be replaced
 by 3x5 cent stamps to obtain P(k+1).
\begin_inset Newline newline
\end_inset


\shape italic
Case 2:
\shape default

\begin_inset Newline newline
\end_inset

If P(k) can be obtained with at least 4x5 cent stamps, P(k+1) can be obtained
 can be obtained by replacing these with 3x7 cent stamps.
\begin_inset Newline newline
\end_inset

So in both cases P(n) holds true.
\end_layout

\end_body
\end_document
