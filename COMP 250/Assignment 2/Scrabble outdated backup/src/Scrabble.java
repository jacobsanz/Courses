// STUDENT_NAME:
// STUDENT_ID:

import java.util.*;
import java.io.*;



public class Scrabble{

    static HashSet<String> myDictionary; // this is where the words of the dictionary are stored

    // DO NOT CHANGE THIS METHOD
    // Reads dictionary from file
    public static void readDictionaryFromFile(String fileName) throws Exception {
        myDictionary=new HashSet<String>();

        BufferedReader myFileReader = new BufferedReader(new FileReader(fileName) );

        String word;
        while ((word=myFileReader.readLine())!=null) myDictionary.add(word);

	myFileReader.close();
    }



    /* Arguments: 
        char availableLetters[] : array of characters containing the letters that remain available
        String prefix : Word assembled to date
        Returns: String corresponding to the longest English word starting with prefix, completed with zero or more letters from availableLetters. 
	         If no such word exists, it returns the String ""
     */
     public static String longestWord(char availableLetters[], String prefix) {
    	 String longest = "";
    	 if (availableLetters.length <= 1){
    		 if (myDictionary.contains(prefix)){ //THIS BASE CASE WORKS. Tested.
    			 longest = prefix;
    		 } else {
    			 longest = "";
    		 } //end of base case
    	 }
    	 else {
    		 for (int i = 0; i < availableLetters.length; i++){ //length-2?
    			 char[] before = Arrays.copyOfRange(availableLetters, 0, i); //ALL ELEMENTS BEFORE i
    			 char[] after = Arrays.copyOfRange(availableLetters, i+1, availableLetters.length); //DODGY -2 //after i
    			 char[] new_letters = Arrays.copyOf(before, before.length + after.length);
    			 System.arraycopy(after, 0, new_letters, before.length, after.length);
    			 //System.out.println("before:");
    			// for (int j = 0; j < before.length; j++){
    			//	 System.out.println(before[j]);
    			// }
    			// System.out.println("prefix:");
    			 //System.out.println(prefix + availableLetters[i]);
    			// System.out.println("after:");
    			// for (int j = 0; j < after.length; j++){
    			//	 System.out.println(after[j]);
    			// }
    			// System.out.println("");
   				 longest = longestWord(new_letters, prefix + availableLetters[i]);
   				 if (myDictionary.contains(longest)){ //NOTICE THAT THE PREFIX IS WHAT GOES WRONG BY PRINTING IT.
   					return longest;
  				 }
    		 }
    	 }
    	 return longest;


    }

    
    
    /* main method
        You should not need to change anything here.
     */
    public static void main (String args[]) throws Exception {
       
	// First, read the dictionary
	try {
	    readDictionaryFromFile("englishDictionary.txt");
        }
        catch(Exception e) {
            System.out.println("Error reading the dictionary: "+e);
        }
        
        
        // Ask user to type in letters
        BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in) );
        char letters[]; 
        do {
            System.out.println("Enter your letters (no spaces or commas):");
            
            letters = keyboard.readLine().toCharArray();

	    // now, enumerate the words that can be formed
            String longest = longestWord(letters, "");
	    System.out.println("The longest word is "+longest);
        } while (letters.length!=0);

        keyboard.close();
        
    }
}