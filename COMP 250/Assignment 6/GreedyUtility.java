import java.util.Arrays; 
public class GreedyUtility {
  
  public static int[] GreedyChoice(int[] C, int[] U, int N){
    int k = C.length;
    int[] Q = new int[k];
    int total_cost = 0;
    int total_utility = 0;

    while ((total_cost < N)){ //executes the summation.
      double current_ratio = 0;
      double best_ratio = 0;
      int i_value = -1; //no_obtrusive starting value for the object number
      for (int i = 0; i<=(k-1) ; i++){
        current_ratio = ((double)U[i]) / ((double)C[i]); //calculate the ratio for all objects
        if ((current_ratio > best_ratio)){
          if ((total_cost + C[i]) <= N){
            best_ratio = current_ratio; //update the best object
            i_value = i; //update object number
          }
        }
      }
      if ((i_value == -1)){
        break;
      }
      else {
        total_cost += C[i_value]; //update arrays
        total_utility += U[i_value];
        Q[i_value]++; //update how many of object i
      }
    }
    
    System.out.println("total utility " + total_utility);
    System.out.println("total cost " + total_cost);
    //System.out.println(Arrays.toString(Q));
    return Q;
  }
  
  public static int[] DynamicValue(int[] C, int[] U, int N){
    int[] DP_array = new int[N+1]; //stores values for calculating total utility
    int[] object = new int[N+1]; //stores first object for each cost
    for (int i = 0; i<=N-1 ; i++){ //initialize both arrays to 0.
      DP_array[i] = 0;
      object[i] = 0;
    }
    
    for (int i = 1; i<=N ; i++){ //for all optimal sub-budgets
      for (int j = 0; j<=(C.length-1); j++){ //for each object
        if ((i >= C[j]) && (U[j] + DP_array[i-C[j]] > DP_array[i])){ //if there is money left and we are increasing our utility
          DP_array[i] = U[j] + DP_array[i-C[j]]; //update the DP utilities.
          object[i] = j; //updates the stored object
        }
      }
    }
    
    int[] quantities = new int[C.length]; //will store the quantity of each object
    int i = N;
    while (i - C[object[i]] >= 0){ //while we do not exceed budget
      for (int j = 0; j <= C.length-1; j++){ //for each of the possible objects
        if (object[i] == j){ //if we have added this object to our purchase
          quantities[object[i]] += 1; //update the amount of this object
        }
      }
      i = i - C[object[i]]; //update how much money we have left
    }
    return quantities;
  }
  
  
  public static void main(String[] args) { 
    int N = 38;
    int[] U = new int[]{1,5,8,9};
    int[] C = new int[]{2,6,8,10};
    int[] answer = GreedyChoice(C, U, N);
    System.out.println(Arrays.toString(answer));
    System.out.println(Arrays.toString(DynamicValue(C, U, N)));
  }
  
}
