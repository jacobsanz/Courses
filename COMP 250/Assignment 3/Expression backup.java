import java.io.*;
import java.util.*;

public class Expression {
	static String delimiters="+-*%()";
	
	// Returns the value of the arithmetic Expression described by expr
	// Throws an exception if the Expression is malformed
	static Integer evaluate(String expr) throws Exception {
		
	    StringTokenizer st = new StringTokenizer(expr, delimiters, true);  //unchanged line from the original downloaded file.  
        Stack<String> operators = new Stack<String>();
        Stack<Integer> numbers = new Stack<Integer>(); //Have to use Integer instead of int
        
	    while (st.hasMoreTokens()) {
	    	String current = st.nextToken(); //Each element is a number of an operator.
	    	//System.out.println(operators.toString() + numbers.toString() + current);
	    	
	    	if (current.equals("(")){
	    		operators.push(current);
	    	}
	    	
	    	if (current.equals("+") || current.equals("-") || current.equals("*") || current.equals("%")){
	    		while ((!operators.isEmpty()) && (!operators.peek().equals("(")) ){ //Loop calculation through same precedence level/bracket.
	    			String top_operator = operators.pop();
	    			Integer number1 = numbers.pop();
	    			Integer number2 = numbers.pop();
	    			Integer result = 0;
	    			if (top_operator.equals("+")){ //I didn't know if I was allowed to write separate methods, hence this.
	    				result = number1 + number2;
	    			}
	    			if (top_operator.equals("-")){
	    				result = number2 - number1; //switch nums
	    			}
	    			if (top_operator.equals("*")){
	    				result = number1 * number2;
	    			}
	    			if (top_operator.equals("%")){
	    				result = number2 / number1; //remember to switch nums here due to pop order.
	    			}	
	    			numbers.push(result);
	    		}
	    		operators.push(current);
	    	}
	    	
	    	if (Character.isDigit(current.charAt(0))){ //Checking first character of string to see if it's a number.
	    		numbers.push(Integer.valueOf(current));
	    	}
	    	
	    	if (current.equals(")")){
	    		String top_operator = operators.pop();
	    		Integer result = 0;
	    		while (!top_operator.equals("(")){ //Solves everything inside the parenthesis.
	    			//System.out.println("start of loop:");
	    			//System.out.println(operators.toString() + numbers.toString() + top_operator);
	    			Integer number1 = numbers.pop();
	    			Integer number2 = numbers.pop();
	    			if (top_operator.equals("+")){ //Copy and pasted from above.
	    				result = number1 + number2;
	    			}
	    			if (top_operator.equals("-")){
	    				result = number2 - number1;
	    			}
	    			if (top_operator.equals("*")){
	    				result = number1 * number2;
	    			}
	    			if (top_operator.equals("%")){
	    				result = number2 / number1;
	    				//System.out.println(number1 + "/" + number2); //switch numbers around due to pop order
	    			}
	    			numbers.push(result);
	    			top_operator = operators.pop();
	    			//System.out.println("end of loop:");
	    			//System.out.println(operators.toString() + numbers.toString() + top_operator);
	    		}
	    	}
	    	
	    }
	    //System.out.println(operators.toString() + numbers.toString());	
	    //System.out.println("BAAACK IN BLAAAAACK");
	    while (!operators.isEmpty()){ //calculate all remaining same-precedence operations.
	    	//System.out.println("got to last loop");
	    	//System.out.println(operators.toString() + numbers.toString());
	    	String top_operator = operators.pop();
	    	Integer number1 = numbers.pop();
    		Integer number2 = numbers.pop();
    		Integer result = 0;
    		if (top_operator.equals("+")){ //Copy and pasted from above.
    			result = number1 + number2;
    		}
    		if (top_operator.equals("-")){
    			result = number2 - number1;
    		}
    		if (top_operator.equals("*")){
    			result = number1 * number2;
    		}
    		if (top_operator.equals("%")){
    			result = number2 / number1;
    		}	
    		numbers.push(result);
	    } 
	    
	    
	    //System.out.println("got to the end!");
	    return numbers.peek();    
	}	

	
	
	
	
	
	
	
	
	
	
	
	public static void main(String args[]) throws Exception {
		String line;
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
                                      	                        
		do {
			line=stdin.readLine();
			if (line.length()>0) {
				try {
					Integer x=evaluate(line);
					System.out.println(" = " + x);
				}
				catch (Exception e) {
					System.out.println("Malformed Expression: "+e);
				}
			}
		} while (line.length()>0);
	}
}