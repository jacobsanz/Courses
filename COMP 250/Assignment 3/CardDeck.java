import java.util.*;

class CardDeck {
    LinkedList<Integer> deck;

    // constructor, creates a deck with n cards, placed in increasing order
    CardDeck(int n) {
	deck = new LinkedList<Integer> ();
	for (int i=1;i<=n;i++) deck.addLast(new Integer(i));
    }

    // executes the card trick
    public void runTrick() {

	while (!deck.isEmpty()) {
	    // remove the first card and remove it
	    Integer topCard = deck.removeFirst();
	    System.out.println("Showing card "+topCard);

	    // if there's nothing left, we are done
	    if (deck.isEmpty()) break;
	    
	    // otherwise, remove the top card and place it at the back.
	    Integer secondCard = deck.removeFirst();
	    deck.addLast(secondCard);

	    System.out.println("Remaining deck: "+deck);

	}
    }


    public void setupDeck(int n) {
    	LinkedList<Integer> new_deck = new LinkedList<Integer> ();
    	List<Integer> rotating = new ArrayList<Integer>();
    	for (int i = n; i > 0; i--){ //for the # of cards
    		Collections.rotate(rotating, 1); //rotates the arraylist one place to the right
    		rotating.add(0, i); //adds the next element to the beginning of the arraylist
    	}
    	for (int i = 0; i < n; i++){
    		new_deck.addLast(rotating.get(i)); //add the contents of the arraylist to the linked list.
    	}
    	this.deck = new_deck; //instructions were unclear, but as the method is void, I assumed it should update the original deck.
    }

    public void rotate_LL() {
    	  
    }

    public static void main(String args[]) {
	// this is just creating a deck with cards in increasing order, and running the trick. 
	CardDeck d = new CardDeck(10);
	d.runTrick();

	// this is calling the method you are supposed to write, and executing the trick.
	CardDeck e = new CardDeck(10);
	e.setupDeck(10);
	e.runTrick();
    }
}

    