import java.lang.Math.*;

class ExpressionTree {
    private String value;
    private ExpressionTree leftChild, rightChild, parent;
    
    ExpressionTree() {
        value = null; 
        leftChild = rightChild = parent = null;
    }
    
    // Constructor
    /* Arguments: String s: Value to be stored in the node
                  ExpressionTree l, r, p: the left child, right child, and parent of the node to created      
       Returns: the newly created ExpressionTree               
    */
    ExpressionTree(String s, ExpressionTree l, ExpressionTree r, ExpressionTree p) {
        value = s; 
        leftChild = l; 
        rightChild = r;
        parent = p;
    }
    
    /* Basic access methods */
    String getValue() { return value; }

    ExpressionTree getLeftChild() { return leftChild; }

    ExpressionTree getRightChild() { return rightChild; }

    ExpressionTree getParent() { return parent; }


    /* Basic setting methods */ 
    void setValue(String o) { value = o; }
    
    // sets the left child of this node to n
    void setLeftChild(ExpressionTree n) { 
        leftChild = n; 
        n.parent = this; 
    }
    
    // sets the right child of this node to n
    void setRightChild(ExpressionTree n) { 
        rightChild = n; 
        n.parent=this; 
    }
    

    // Returns the root of the tree describing the expression s
    // Watch out: it makes no validity checks whatsoever!
    ExpressionTree(String s) {
        // check if s contains parentheses. If it doesn't, then it's a leaf
        if (s.indexOf("(")==-1) setValue(s);
        else {  // it's not a leaf

            /* break the string into three parts: the operator, the left operand,
               and the right operand. ***/
            setValue( s.substring( 0 , s.indexOf( "(" ) ) );
            // delimit the left operand 2008
            int left = s.indexOf("(")+1;
            int i = left;
            int parCount = 0;
            // find the comma separating the two operands
            while (parCount>=0 && !(s.charAt(i)==',' && parCount==0)) {
                if ( s.charAt(i) == '(' ) parCount++;
                if ( s.charAt(i) == ')' ) parCount--;
                i++;
            }
            int mid=i;
            if (parCount<0) mid--;

        // recursively build the left subtree
            setLeftChild(new ExpressionTree(s.substring(left,mid)));
    
            if (parCount==0) {
                // it is a binary operator
                // find the end of the second operand.F13
                while ( ! (s.charAt(i) == ')' && parCount == 0 ) )  {
                    if ( s.charAt(i) == '(' ) parCount++;
                    if ( s.charAt(i) == ')' ) parCount--;
                    i++;
                }
                int right=i;
                setRightChild( new ExpressionTree( s.substring( mid + 1, right)));
        }
    }
    }


    // Returns a copy of the subtree rooted at this node... 2014
    ExpressionTree deepCopy() {
        ExpressionTree n = new ExpressionTree();
        n.setValue( getValue() );
        if ( getLeftChild()!=null ) n.setLeftChild( getLeftChild().deepCopy() );
        if ( getRightChild()!=null ) n.setRightChild( getRightChild().deepCopy() );
        return n;
    }
    
    // Returns a String describing the subtree rooted at a certain node.
    public String toString() {
        String ret = value;
        if ( getLeftChild() == null ) return ret;
        else ret = ret + "(" + getLeftChild().toString();
        if ( getRightChild() == null ) return ret + ")";
        else ret = ret + "," + getRightChild().toString();
        ret = ret + ")";
        return ret;
    } 


    // Returns the value of the expression rooted at a given node
    // when x has a certain value
    double evaluate(double x) {
    	
    	double answer = 0.0; //initialize.
		
		if (this.getValue() == null){ //if it's empty, return 0.
		answer = 0.0;
		}
		
		if ((this.getLeftChild() == null) && (this.getRightChild() == null)){ //If it is a leaf. Base Case..We know what to do with leaves.
			String temp =  this.getValue();
			if (temp.equals("x")){
				answer = x; //replace letter x with the value of parameter.
			}
			else{
			answer = Double.parseDouble(this.getValue()); //if it's not the algebraic variable, it has to be a number.
			}
		}
	
		else{ //for all other cases, recursion
			double number1 = this.getLeftChild().evaluate(x);
			double number2 = 0;
			if (this.getRightChild() != null){ //feels a bit like sticking duct tape on a crack
				number2 = this.getRightChild().evaluate(x);
			}
			String operator = this.getValue();
			//here goes the nasty bit
			if (operator.equals("add")){ //eeeeeew it's hideous.
				answer = number1 + number2;
			}
			if (operator.equals("minus")){
				answer = number1 - number2;
			}
			if (operator.equals("mult")){
				answer = number1 * number2;
			}
			if (operator.equals("exp")){
				answer = Math.exp(number1);
			}
			if (operator.equals("sin")){
				answer = Math.sin(number1);
			}
			if (operator.equals("cos")){
				answer = Math.cos(number1);
			}
			//else{
			//	throw new IllegalArgumentException("I can't handle this operation, you dollop.");
			//}
		}
		return answer;
    }                                                 

    /* returns the root of a new expression tree representing the derivative of the
       original expression */
    ExpressionTree differentiate() {
    	
    	if ((this.getLeftChild() == null) && (this.getRightChild() == null)){ //base case, seems to be working.
    		if (this.getValue().equals("x")){
    			return new ExpressionTree("1"); //x --> 1
    		} else{
    			return new ExpressionTree("0"); //any other constant --> 0.
    		}	
    	}
    	
    	if ((this.getValue().equals("add")) || (this.getValue().equals("minus"))){ //same thing for + and -. //SEEMS TO WORK.
    		ExpressionTree number1 = this.getLeftChild().differentiate(); //recursively differentiate the separate terms.
    		ExpressionTree number2 = this.getRightChild().differentiate();
    		ExpressionTree answer = new ExpressionTree(this.getValue(), number1, number2, this.getParent());
    		return answer;
    	}
    	
    	if (this.getValue().equals("mult")){ //seems to work
    		ExpressionTree copyleft = this.getLeftChild().deepCopy(); //stores the original values to use the chain rule
    		ExpressionTree copyright = this.getRightChild().deepCopy();
    		ExpressionTree left_dif = this.getLeftChild().differentiate(); //get differentiated recursively.
    		ExpressionTree right_dif =  this.getRightChild().differentiate();
    		ExpressionTree number1 = new ExpressionTree("mult", copyleft, right_dif, this.getParent()); //the two terms to be added
    		ExpressionTree number2 = new ExpressionTree("mult", copyright, left_dif, this.getParent());
    		ExpressionTree answer = new ExpressionTree("add", number1, number2, this.getParent()); //adding them up
    		return answer;
    	}
    	
    	if (this.getValue().equals("exp")){
    		ExpressionTree copy = this.deepCopy(); //copies the original
    		ExpressionTree left_dif = this.getLeftChild().differentiate(); //get differentiated recursively.
    		ExpressionTree answer = new ExpressionTree("mult", copy, left_dif, this.getParent()); //chain rule multiply.
    		return answer;
    	}
    	
    	if (this.getValue().equals("sin")){
    		ExpressionTree copyleft = this.getLeftChild().deepCopy(); //copy the only child.
    		ExpressionTree number1 = new ExpressionTree("cos", copyleft, null, this.getParent()); //sin[f(x)] --> cos[f(x)]
    		ExpressionTree number2 = this.getLeftChild().differentiate(); //for the chain rule
    		ExpressionTree answer = new ExpressionTree("mult", number1, number2, this.getParent()); //multiplying them together
    		return answer;
    	}
    	
    	if (this.getValue().equals("cos")){
    		ExpressionTree copyleft = this.getLeftChild().deepCopy(); //copy the only child.
    		ExpressionTree number1 = new ExpressionTree("sin", copyleft, null, this.getParent()); //cos[f(x)] --> sin[f(x)] LACKS MINUS
    		ExpressionTree number2 = this.getLeftChild().differentiate(); //for the chain rule
    		ExpressionTree zero = new ExpressionTree("0");
    		ExpressionTree one = new ExpressionTree("1");
    		ExpressionTree minusone = new ExpressionTree("minus", zero, one, this.getParent()); //THIS.GETPARENT????
    		ExpressionTree minusnumber1 = new ExpressionTree("mult", minusone, number1, this.getParent()); //THIS.GETPARENT????
    		ExpressionTree answer = new ExpressionTree("mult", minusnumber1, number2, this.getParent()); //multiplying them together
    		return answer;
    	}
    	
    	return new ExpressionTree("0"); //just here to prevent crashes
    }
        
    
    public static void main(String args[]) {
        ExpressionTree e = new ExpressionTree("mult(add(2,x),cos(x))");
    	//ExpressionTree e = new ExpressionTree("mult(x,add(add(2,x),cos(minus(x,4))))");
        System.out.println(e);
        System.out.println(e.evaluate(1));
        System.out.println(e.differentiate());
   
 }
}
