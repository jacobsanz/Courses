3.
This algorithm for the tripartite mergesort sorts an array of numbers in ascending order. We use the notation array[-1] to denote the last element of an array, and use the oracle as a function call with three parameters. For example, oracle(a,b,c) will return the largest of elements a,b, and c in 1 time unit.
This oracle allows us to perform less overall comparisons, as we can rapidly add the largest of three elements to the end of our destination array during the merge procedure. We use the oracle at most n times at each step, and have (log_3(n)), giving us a runtime of O(nlog_3(n)), which we prove by induction.

trisort (array a)
	if (n == 1)
		return a
    l1 = a[0]...a[floor((n/3)-1]
    l2 = a[floor(n/3)]...a[floor(2n/3)-1]
	l3 = a[floor(2n/3)]...a[n-1]

     l1 = trisort(l1)
     l2 = trisort(l2)
     l3 = trisort(l3)
     return merge(l1, l2, l3)

merge (array a, array b, array c)
	d = destination array

	while (a, b, and c have elements)
		x = Oracle(a[-1], b[-1], c[-1])
		if x = a[-1]
			add a[-1] to the end of d
			remove a[-1] from a
		if x = b[-1]
			add b[-1] to the end of d
			remove b[-1] from b
		if x = c[-1]
			add c[-1] to the end of d
			remove c[-1] from c

	while (a and b have elements)
		x = Oracle(a[-1], b[-1], copy of b[-1])
		if x = a[-1]
			add a[-1] to the next free slot nearest to the end of d
			remove a[-1] from a
		if x = b[-1]
			add b[-1] to the next free slot nearest to the end of d
			remove b[-1] from b
	while (a and c have elements)
		x = Oracle(a[-1], c[-1], copy of c[-1])
		if x = a[-1]
			add a[-1] to the next free slot nearest to the end of d
			remove a[-1] from a
		if x = c[-1]
			add c[-1] to the next free slot nearest to the end of d
			remove c[-1] from c
	while (b and c have elements)
		x = Oracle(b[-1], c[-1], copy of c[-1])
		if x = b[-1]
			add b[-1] to the next free slot nearest to the end of d
			remove b[-1] from b
		if x = c[-1]
			add c[-1] to the next free slot nearest to the end of d
			remove c[-1] from c

	while (a has elements)
		add a[-1] to the beginning of d
		remove a[-1] from a
	while (b has elements)
		add b[-1] to the beginning of d
		remove b[-1] from b
	while (c has elements)
		add c[-1] to the beginning of d
		remove c[-1] from c

	return d

------------------------------------------------------------------------------------------

Our worst recurrence, in terms of the number of uses of the oracle (comparisons) is:
T(n) = 0 for n <= 1
T(n) = T(floor(n/3)) + 2T(ceil(n/3) elsewhere

Proof by induction to show T(n) <= nlog_3(n) for all n:

Base case: n = 1. There are no comparisons, so T(1) = 0 holds.

Inductive step: We assume time up to (but not including) n holds, then we have 3 cases:

Case 1: n mod 3 = 0
Here the 3 partitions of the input are of equal size, so we solve for T(n) = 3T(n/3).
= nlog_3(n/3) + n - 1
= n(log_3(n) - 1) + n -1
= nlog_3(n) - n + n - 1
= nlog_3(n) -1

Case 2: n mod 3 = 1
Here we solve for  T(n) = T(ceil(n/3)) + 2T(floor(n/3))
= ((n+2)/3)log_3((n+2)/3) + ((2n-2)/3)log_3((n-1)/3) + n - 1
= (n/3)log_3(((n+2)/3) * ((n-1)/3) * (n-1)/3)) + (2/3)*log_3((n+2)/(n-1)) + n - 1
= (n/3)log_3(n^3/27) + (2/3)*log_3(1) + n - 1
<=(n/3)log3_(n^3/27) + 2/3 + n - 1
= nlog_3(n) - n + 2/3 + n - 1
= nlog3_(n) - 1/3

Case 3: n mod 3 = 2
Here we solve for T(n) = T(floor(n/3)) + 2T(ceil(n/3))
= ((n-2)/3)log_3((n-2)/3) + ((2n+2)/3)log_3((n+1)/3) + n - 1
= (n/3)log_3(((n-2)/3) * ((n+1)/3) * (n+1)/3)) + (2/3)*log_3((n+1)/(n-2)) + n - 1
= (n/3)log_3(n^3/27) + (2/3)*log_3(1) + n - 1
<=(n/3)log3_(n^3/27) + 2/3 + n - 1
= nlog_3(n) - n + 2/3 + n - 1
= nlog3_(n) - 1/3


 
