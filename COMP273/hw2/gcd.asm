# Program to implement the Dijkstra's GCD algorithm

	.data		# variable declarations follow this line
str1: 	.asciiz "Enter the first integer: "
str2: 	.asciiz "Enter the second integer: "   
str3:	.asciiz "GCD: "               
														
	.text		# instructions follow this line	
	
main:     		# indicates start of code to test lcm the procedure
	li $v0, 4		#prints first prompt
	la $a0, str1
	syscall
	li $v0, 5		#stores first int
	syscall
	add $s0, $v0, $zero	#store first int m into $s0.
	
	li $v0, 4		#prints second prompt
	la $a0, str2
	syscall
	li $v0, 5		#stores second int
	syscall
	add $s1, $v0, $zero	#move second int n into $s1.
	
	jal gcd			#Calls gcd procedure
	add $s2, $v0, $zero	#stores procedure's output $v0 in $s2 for syscall
	
	li $v0, 4		#prints gcd prompt
	la $a0, str3
	syscall
	li $v0, 1		# system call code for print_int
	add  $a0, $s2, $zero	
	syscall
	
	j Exit																                    																	                    																	                    
																	                    																	                    																	                    																	                    
gcd:	     		
	addi $sp,$sp,-12	# create space for three items on the stack
	sw $ra,8($sp)		# save $ra on the stack			
	sw $s0,4($sp)		# save m the stack
	sw $s1,0($sp)		# save n on the stack
	
	beq $s0, $s1, base	#if n==m goto base
	blt $s1, $s0, rec1	#if n<m goto recursive case 1
	j rec2			#else goto rec2 (m>n)
base:
	add $v0, $s1, $zero	#return m in $v0
	addi $sp,$sp,12		# pop three items off the stack
	jr $ra	

rec1:
	sub $s0, $s0, $s1	#m = m-n
	jal gcd			#recursive gcd with new m value

	lw $ra,8($sp)		#restore our ra, m, n to what they were
	lw $s0,4($sp)
	lw $s1,0($sp)

	addi $sp,$sp,12		# pop three items off the stack
	jr $ra			#return to wherever we were called

rec2:
	sub $s1, $s1, $s0	#n = n-m
	jal gcd			#recursive gcd with new n value

	lw $ra,8($sp)		#restore our ra, m, n to what they were
	lw $s0,4($sp)
	lw $s1,0($sp)

	addi $sp,$sp,12		# pop three items off the stack
	jr $ra			#return to wherever we were called

stop:	
	addi $sp,$sp,12		#pop 3 items (n,m, $ra)off the stack
	jr $ra			#return to main


Exit:	nop			# end of program							
# End of program
