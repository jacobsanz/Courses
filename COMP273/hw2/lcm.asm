# Program to calculate the least common multiple of two numbers

	.data		# variable declarations follow this line
first: 	.word 10	# the first integer
second: .word 15    	# the second integer                  
														
	.text		# instructions follow this line	
																	                    
main:   # indicates start of code to test lcm the procedure
	la $t0, first		#load first number into a1
	lw $a1, 0($t0)
	la $t0, second		#load second number into a2
	lw $a2, 0($t0)
	li $a0, 1		#iterator i set to 1 in a0

	jal lcm			#Calls lcm procedure
	
	add $s0, $v0, $zero	#we returned the procedure's final value in $v0, but we put it in $s0 for the syscall.
	li $v0, 1		#system call code for print_int
	add  $a0, $s0, $zero
	syscall
	
	j Exit			#end of program


lcm:	     			# the “lcm” procedure
start_loop:
	slt $t0, $a0, $a2	#check condition: if iterator less than second number
	
	mul  $s0, $a0, $a1	#iterator * first number in s0, assumes no overflow.
	div $s0, $a2		#(i*first)/second to get mod
	mfhi $s1		#move remainder (contents of hi) to s1
	beq $s1, $zero, end_loop#if mod=0 then end loop
	
	addi $a0, $a0, 1	#increment the iterator i	
	j start_loop		#loop if answer has not been reached this iteration
	
end_loop:
	add $v0, $zero, $s0	#get s0 (i*first) into conventional return register
	jr $ra			#return to main

Exit:	nop			# end of program






									
# End of program
