# Program to capitalize the first letter of a string

	.data		# variable declarations follow this line	
	
str: 	.asciiz "Enter the string to capitalize: "
usr_input:.space 127              #Reserve 127 bytes for string,
                 
														
	.text		# instructions follow this line	
	
	
																	                    
main:     			 #indicates start of code to test "upper" the procedure
    	li  $v0, 4		 #print prompt
    	la  $a0, str     	
    	syscall
    	
    	li  $v0, 8		#get users input
    	la $a0, usr_input	#Read string into a0, pointer to first element.
    	li $a1, 127		#indicate length
    	syscall
	
	jal upper

        li  $v0, 4		#print the modified string
        la  $a0, usr_input
        syscall
        
        li $v0, 10		#signal its the end of the main
        syscall   
        
    	
upper:	     				# the “upper” procedure
	addi $s0, $zero, 0		#$s0 is a counter intialized to zero
	addi $s1, $zero, 32		#s1 is 32 (ASCII space)
	addi $s2, $zero, 97		#s2 is 97 (ASCII a)
	addi $s3, $zero, 122		#s3 is 122 (ASCII z)
start_loop:
	slti $t0, $s0, 127		#check condition: while counter $s0 is less than 127, $t0 is true
	beq $t0, $zero, end_loop	#if 127 bytes done, go to end of loop.

	lb $t0, ($a0) 			#load next byte into $t0
	
	beq $s0, $zero, check		#case: if first character, check it
	
	beq $t0, $zero, end_loop	#if current byte is NULL (ASCII = 0), end loop.
	bne $t0, $s1, not_space		#if current byte != SPACE, skip to incrementers..
	
					#else, we are dealing with a space, so go to the next char			
	addi $a0, $a0, 1 		#increment the pointer of the string (move one byte forward).
	addi $s0, $s0, 1 		#increment counter
	lb $t0, ($a0) 			#load a next byte into $t0
check:					#check if in desired range: 97-122, if true, then subtract 32
cond1:	 ble $t0, $s3, cond2            #If smaller/equal to 122, check next condition
cond2:   bge $t0, $s2, in_range         #if also bigger/equal to 97, then it is in range.
	
	beq $s0, $zero, not_space	#case: if first character, increment it
			
	j start_loop			#loop if answer has not been reached this iteration
		
not_space:	
	addi $a0, $a0, 1 		#increment the pointer of the string (move one byte forward).
	addi $s0, $s0, 1 		#increment counter
	j start_loop			#loop if answer has not been reached this iteration
	
in_range:
	addi $t0, $t0, -32 		#if within range, subract 32 to make it a capital letter	
	sb $t0, ($a0)
	j start_loop

end_loop: 	
	jr $ra			
								
# End of program
