#StudentID: 	260706158
#Name: 		Jacob Sanz-Robinson

.data

str1: 	.asciiz "\nGimme a char: \n"
orgtxt: .asciiz "\nOriginal linked list\n"
revtxt:	.asciiz "\nReversed linked list\n"

.text
#There are no real limit as to what you can use
#to implement the 3 procedures EXCEPT that they
#must take the specified inputs and return at the
#specified outputs as seen on the assignment PDF.
#If convention is not followed, you will be
#deducted marks.

main:
#build a linked list
#print "Original linked list\n"
#print the original linked list

#reverse the linked list
#On a new line, print "reversed linked list\n"
#print the reversed linked list
#terminate program
jal build
add $a0, $v1, $zero 		#pass address of head to $a0 as required.
add $s4, $v1, $zero		#			!!!

li $v0, 4			#prints first prompt
la $a0, orgtxt
syscall

add $a0, $s4, $zero		#DELETE AFTER TESTING REV
jal print

add $a1, $s4, $zero		#DELETE AFTER TESTING REV			!!!
jal reverse

li $v0, 4			#prints first prompt
la $a0, revtxt
syscall

add $a0, $v1, $zero 		#pass address of head to $a0 as required.
jal print

j Exit
####################################################################################################################

build: # For all the subroutines in build: head = s0, temp = s1.
		addi $sp, $sp,-4		# create space for $ra on the stack
		sw $ra, 0($sp)			# save on the stack
		
		addi $s7, $zero, 42		#the ascii value of '*'
		j make_head

input_proc:	#prompt and store inputs into $a0		
		li $v0, 4			#prints first prompt
		la $a0, str1
		syscall
		
		li   $v0, 12       
  		syscall           		 #Read Character
    		
    		add $a1, $zero, $v0 		 #put char into $a1
    		
    		jr $ra
	
make_head:	
		jal input_proc
		beq $a1, $s7, end_build		#if input is *, then end the agony at once.
		
		li $a0, 5			# Need to malloc 5 bytes
		jal malloc			# Call malloc
		
		add $s0, $v0, $zero		# head gets the malloc result 
		add $t0, $zero, $a1		# load our char into $t0
		sw $t0, 4($s0)  		# head.value = char
		sw $zero, 0($s0)		# head.next = NULL

loop_build:	beq $a1, $s7, end_build		#loop guard. While input = *, then goto end_build
add_node: 
		jal input_proc
		
    		li $a0, 5			#As above
		jal malloc
		
		add $s1, $v0, $zero		# temp gets the malloc result
		sw $s0, 0($s1)			# temp.next = head
		add $t0, $zero, $a1		#load char into $t0
		sw $t0, 4($s1) 			# temp.value = char
		move $s0, $s1			# head = temp

		j loop_build			#back to beginning of loop_build		

end_build:
		lw $v1, 0($s0) #store head address in v1	
		lw $ra, 0($sp)	#pop and restore stack
		addi $sp, $sp, 4
		jr $ra

#####################################################################################################################

print:
#$a0 takes the address of the first node
#prints the contents of each node in order

		#add $a0, $v1, $zero 		#pass address of head to $a0 as required.
		add $t0, $zero, $a0		#and pass it to t0 to actually do stuff
		
loop_print:
		beq $t0, $zero, end_print	#Loop Guard. While the adress at $t0 is not null.
		#lw $a0, 4($t0)			#load char into a0 for printing
		
		li $v0, 4 
		la $a0, 4($t0) 			#load char into $a0 for printing.
		syscall
		
		lw $t0, 0($t0)			#load the next adress into $t0 and loop.
		j loop_print
				
end_print:	
		jr $ra
######################################################################################################################

reverse:
#$a1 takes the address of the first node of a linked list
#reverses all the pointers in the linked list
#$v1 returns the address

		#a1 = headref, t0 = prev, t1 = current, t2 = next
		
		add $t0, $zero, $zero		#initialize prev to NULL. Not really needed.
		add $t1, $a1, $zero		#initialize current to the inputted head reference
		
loop_rev:	beq $t1, $zero, end_rev		# loop guard. While current != NULL...
		lw  $t2, 0($t1)			# next  = current.next;
		sw $t0, 0($t1)			# current.next = prev
		add $t0, $t1, $zero		# prev = current
		add $t1, $t2, $zero		# current = next
		j loop_rev

end_rev: 	
		add $v1, $zero, $t0		#return head in $v1. headref = prev.
		jr $ra
		#ALMOST WORKING. JUST SKIPPING FIRST ELEM IN RETURN FOR SOME REASON.
######################################################################################################################
malloc: #$a0 will be the number of bytes we need. Adress of allocated bits will be in $v0.
	li $v0, 9
	syscall
	jr $ra

Exit:	nop
