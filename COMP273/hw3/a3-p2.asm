# Name:		Jacob Sanz-Robinson
# Student ID: 	260706158

# Problem 2 - Dr. Ackermann or: How I Stopped Worrying and Learned to Love Recursion
###########################################################
.data
error: .asciiz "error: m, n must be non-negative integers\n"

str1: 	.asciiz "Enter the first integer (m): "
str2: 	.asciiz "Enter the second integer (n): "   
str3:	.asciiz "Ackermann Number: "
.text 
###########################################################
main:
# get input from console using syscalls
	li $v0, 4		#prints first prompt
	la $a0, str1
	syscall
	li $v0, 5		#stores first int
	syscall
	add $s0, $v0, $zero	#store first int m into $s0.
	
	li $v0, 4		#prints second prompt
	la $a0, str2
	syscall
	li $v0, 5		#stores second int
	syscall
	add $s1, $v0, $zero	#move second int n into $s1.
	
	add $a0, $s0, $zero
	add $a1, $s1, $zero
	jal check		#call check, makes sure args are non-negative.

# compute A on inputs 
	jal A
	add $s2, $v0, $zero	#stores procedure's output $v0 in $s2 for syscall	
	
# print value to console and exit with status 0

	li $v0, 4		#prints prompt
	la $a0, str3
	syscall
	li $v0, 1		# system call code for print_int
	add  $a0, $s2, $zero	
	syscall
	
	j Exit	
###########################################################
# int A(int m, int n)
# computes Ackermann function
A: 
	addi $sp,$sp,-8		#create space for 2 items on the stack
	sw $ra,8($sp)		#save $ra on the stack			
	sw $s0,4($sp)		#save s0 on the stack
	
	beq $a0, $zero, Base	#if m==0 goto base case
	beq $a1, $zero, Rec1	#if n==0 goto recursive case 1 (it is implied m>0 at this point)
	j Rec2			#else goto Rec2 (m and n > 0)
	
Base:	#Base case, when m=0.
	addi $v0, $a1, 1	#answer is n+1
	j Stop
	
Rec1: #n=0, m>0
	addi $a0, $a0, -1	#m -> m-1
	addi $a1, $zero, 1	#n -> 1
	jal A			#call A(m-1, 1)
	j Stop
	
Rec2:
	addi $a1, $a1, -1	#n -> n-1
	add $s0, $zero, $a0 	#store m in $s0
	jal A			#call A(m, n-1) and return to this line
	addi $a0, $s0, -1	#m -> m-1
	add $a1, $zero, $v0	#n -> A(m, n-1)
	jal A			#call A(m-1, A(m,n-1))

Stop: 
	lw $ra,8($sp)		#restore our ra, s0 to what they were
	lw $s0,4($sp)
	
	addi $sp,$sp,8		# pop 2 items off the stack
	jr $ra	
	
###########################################################
# void check(int m, int n)
# checks that n, m are natural numbers
# prints error message and exits with status 1 on fail
check:
	bltz $a0, minus		#if any of the two args is less than 0...goto minus
	bltz $a1, minus
	jr $ra			#Else continue with the program.
	
minus: 
	li $v0, 4		#prints first prompt
	la $a0, error
	syscall
	addi $a0, $zero, 1
	li $v0, 17				#error code 1
	syscall

Exit:	#nop			# end of program
	add $a0, $zero, $zero
	li $v0, 17				#error code 0
	syscall
