# Name: 	Jacob Sanz-Robinson
# Student ID: 	260706158

# Problem 3
# Numerical Integration with the Floating Point Coprocessor
###########################################################
.data
N: .word 20
a: .float -1.0
b: .float 20.0
error: .asciiz "error: must have low < hi\n"

onef: .float 1.0
halff: .float 0.5
zerof: .float 0.0

str1: 	.asciiz "check"

#function: .word 0x004000dc #address f function, in this case, ident.



.text 
###########################################################
main:
	# set argument registers appropriately
	
	# call integrate on test function 
	
	# print result and exit with status 0
	
	lw $a1, N				#$a1 gets N
	lwc1 $f12, a 				#$f12 gets low 
	lwc1 $f13, b 				#$f13 gets hi hi hi, highly reccomended: https://www.youtube.com/watch?v=BAM55FPpHpU
	la $a0, ident				#$a0 gets the label of the function we are integrating.
	
	jal integrate
	
	li $v0, 2				#print float
	mov.s $f12, $f0				#put return value f0 into f12 for printing
	syscall
	
	j Exit
###########################################################
# float integrate(float (*func)(float x), float low, float hi)
# integrates func over [low, hi]
# $f12 gets low, $f13 gets hi, $a0 gets address (label) of func
# $f0 gets return value
integrate: 
	addi $sp, $sp,-4			# create space for $ra on the stack
	sw $ra, 0($sp)				# save on the stack
	
	jal check 				#calls check.
	
	# initialize $f4 to hold N
	# since N is declared as a word, will need to convert 
	mtc1 $a1, $f4 				#move N from $a1 to $f4
	cvt.s.w $f4, $f4 			#cast N (int to float), as requested.
	
	sub.s $f10, $f13, $f12 			# $f10 = b-a
	div.s $f10, $f10, $f4  			# $f10 = (b-a)/N = delta x.
	
	lwc1 $f20, zerof			#make sure counter f20 is initialized to 0
	
	lwc1 $f2, zerof				#make sure our sum is initialized at 0
	
	#lw $a0, square		HOW DOES A0 GET LABEL/ADRESS OF FUNCTION
	
rei_loop: #we will use f20 to hold our count up to f4 (N). 
	  #$f2 will hold the partial sum, and be transfered to $f0 when done.
	 c.eq.s  $f20, $f4		#Loop guard. While the counter is less than number of rectangles N
	 bc1t end_loop         		#Branch if $f20 = $f4
	
	lwc1 $f18, onef				#load 1.0 into $f18 temp.
	lwc1 $f16, halff			#load 0.5 into f16 temp.
	
	add.s $f6, $f20, $f18			# count+1. $f6 holds our value of x
	mul.s $f6, $f6, $f10			# (count+1)*width
	add.s $f6, $f6, $f12			# x = ((count+1)*width) + low
	
	mul.s $f8, $f16, $f10			#f8 holds width/2
	sub.s $f12, $f6, $f8			#f12 holds x of midpoint of current rectangle
	
	mov.s $f6, $f12 			#backup our f6 (current x)
	
	jalr $ra, $a0				#call function on f12
	
	lwc1 $f12, a 				#f12 is restored to low to prevent chaos in the multiverse
	
	mul.s $f24, $f10, $f0			# multiply result of function @ f6 * width f10. f24 saves this area of rectangle.
	add.s $f2, $f2, $f24			#add f2 += this multiplication

	lwc1 $f18, onef				#load 1.0 into $f18 temp.
	add.s $f20, $f20, $f18			#increment counter by 1
	
	j rei_loop


end_loop:		
	mov.s $f0, $f2				#f0 return value gets end result of f2 partial sum
	
	lw $ra, 0($sp)				#pop and restore stack
	addi $sp, $sp, 4
	jr $ra
###########################################################
# void check(float low, float hi)
# checks that low < hi
# $f12 gets low, $f13 gets hi
# # prints error message and exits with status 1 on fail
check:
	c.lt.s  $f13, $f12   			#set flag true if $f13 < $f12
        bc1t  minus          			#Branch if $f12 < $f13
	jr $ra					#Else continue with the program.
	
minus: 
	li $v0, 4				#prints error prompt provided and exits
	la $a0, error
	syscall
	addi $a0, $zero, 1
	li $v0, 17				#error code 1
	syscall
	#j Exit
###########################################################
# float ident(float x) { return x; }
# function to test your integrator
# $f12 gets x, $f0 gets return value
ident:
	mov.s $f0, $f12	
	jr $ra
############################################################
square: #test squaring function

	mul.s $f12, $f12, $f12
	mov.s $f0, $f12		
	jr $ra

###########################################################
fugly:	#test some nasty poly (2x^4 + 1)
	mul.s $f12, $f12, $f12
	mul.s $f12, $f12, $f12
	add.s $f12, $f12, $f12
	lwc1 $f30, onef
	add.s $f12, $f12, $f30
	mov.s $f0, $f12		
	jr $ra
	
Exit:	#nop
	add $a0, $zero, $zero
	li $v0, 17				#error code 0
	syscall