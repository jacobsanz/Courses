#Jacob Sanz-Robinson	(260706158)
#Calculator
#You will recieve marks based 
#on functionality of your calculator.
#REMEMBER: invalid inputs are ignored.


#Have fun!


#TODO:
#main procedure, that will call your calculator

#calculator procedure, that will deal with the input
	#2 cases you must consider:

	#  Number Operation Number <enter is pressed>
	#  Must display the result on the screen

	#  Operation Number <enter is pressed>
	#  uses prior result as the first number
	#  Returns the new result to the display


#driver for getting input from MIPS keyboard


#driver for putting output to MIPS display



.data
	
intro:	.asciiz "Calculator for MIPS\npress ’c’ for clear and ’q’ to quit: \n"
rez:	.asciiz "Result:   "
arr1:  .space 1000		#1000 byte numbers supported
arr2:  .space 200		#miscellaneous stuff?

hundred: .float 100.00
ofive:   .float 0.50
zerof:   .float 0.00

.text 

main:	
	#You must press to delimit each number and operation (even before the enter) '50*space*+*space*-2*space**enter*'!!!!
	la $a1, intro
	jal Start #display the intro string in mmio
	j Calc
	j Exit
	######################################

Read:  	lui $t0, 0xffff #ffff0000	#straight off MyCourses/Langers notes
Loop1:	lw $t1, 0($t0) #control		#load from the input control register
	andi $t1,$t1,0x0001		#reset (clear) all bits except LSB
	beq $t1,$zero,Loop1		#if not yet ready, then loop back
	lw $v0, 4($t0) #data		#input device is ready, so read
	jr $ra				#####The data read is in v0

Write:  lui $t0, 0xffff #ffff0000	#straight off Mycourses/Langers Notes
Loop2: 	lw $t1, 8($t0) #control		#load the output control register
	andi $t1,$t1,0x0001		#reset all bits except LSB
	beq $t1,$zero,Loop2		#if not ready, then loop back
	sw $a0, 12($t0) #data		#output device is ready, so write
	jr $ra				####Writes data in $a0 to output of mmio
	
Start:	#Displays intro string in MMIO
	#a1 has address of text to print
	addi $sp,$sp,-4		#create space for item on the stack
	sw $ra,0($sp)		#save $ra on the stack		
	add $s0, $a1, $zero	#adress increments by 1 in s0
Startl:	
	lb $a0,($s0)			#load ascii char into $a0
	jal Write			#write it to mmio
	addi $s0, $s0, 1		#increment counter
	addi $s1, $s1, 1		
	beq $s1, 58, xStart		#stop when counter is 61
	j Startl
xStart: lw $ra,0($sp)
	addi $sp,$sp,4			#pop ra off stack
	jr $ra
	
	
Result_txt:	la $a2, rez#Displays intro string in MMIO
	#a1 has address of text to print
	addi $sp,$sp,-4		#create space for item on the stack
	sw $ra,0($sp)		#save $ra on the stack		
	add $s0, $a2, $zero	#adress increments by 1 in s0
Resultr:	
	lb $a0,($s0)			#load ascii char into $a0
	jal Write			#write it to mmio
	addi $s0, $s0, 1		#increment counter
	addi $s1, $s1, 1		
	beq $s1, 12, xResultr		#stop when counter is 9
	j Resultr
xResultr: lw $ra,0($sp)
	addi $sp,$sp,4			#pop ra off stack
	jr $ra

###################################################################################
Calc:	addi $sp,$sp,-4		#create space for item on the stack
	sw $ra,0($sp)		#save $ra on the stack	
	
	###############
	jal Read		#get a character into v0
	
	beq $v0, 113, Exit	#q, so exit
	beq $v0, 99, Clear	#c, so goto clear
	

Check_int:	blt $v0, 48, Check_sign
		bgt $v0, 57, Check_sign
		j Treat_int	#if between 48-57 (0-9 in dec) then process int
		
Check_sign:	beq $v0, 43, splu 
		beq $v0, 45, smin 
		beq $v0, 42, smul 
		beq $v0, 47, sdiv #if +-*/ then process sign

Check_space:	beq $v0, 32, Treat_space #if space

Check_enter:	beq $v0, 10, Treat_enter #if newline

lol:		j Calc	#Not a character we are interested in, so don't write.

#s0 = sign
#s1 = op sign
#s2 = n1 int count
#s3 = result
#s4 = n1
#s5 = n2
#s6 = prevresult
#s7 = have i pressed space?
#a1 = n1 array (label: arr1)
#a2 = n2 array (label: arr2)

Treat_int: #input int in $v0, storing in an array

		store_n1:	la $a1, arr1
				add $t1, $a1, $s2	#arr1 + counter
				addi $t0, $v0, -48	#convert to dec
				sb $t0, ($t1) 		#0-9 so only need bytes
				addi $s2, $s2, 1	#inc counter
				addi $a0, $v0, 0 	#for writing to screen
				j wCalc

Treat_sign:	
		
		splu:	addi $a0, $v0, 0
			addi $s0, $0, 1
			j wCalc
		smin:	addi $a0, $v0, 0
			addi $s0, $0, 2
			j wCalc
		smul:	addi $a0, $v0, 0
			addi $s0, $0, 3
			j wCalc
		sdiv:	addi $a0, $v0, 0
			addi $s0, $0, 4
			j wCalc

Treat_space:	
		addi $s7, $s7, 1
		#if count != 0, store num, if 0, then its a sign, not a number.		
		beq $s2, 0, opsign
		
		make_n1:	la $a1, arr1
				add $t0, $0, $0  #counter to 0
				
		loop_n1:	beq $s2, 0, endloopn1				#0 or -1???/
				add $t1, $t0, $a1
				lb $t2, ($t1)	#t2 holds arr1[n incrementing]
				addi $t0, $t0, 1
				add $t3, $0, $s2	#place s2 in t3 for power input
				addi $t3, $t3, -1	#TEST
				jal power		#get 10^intcount in t4
				mul $t2, $t2, $t4
				addi $s2, $s2, -1	#decrement intcount by 1
				add $s3, $s3, $t2	#build up n1	
				j loop_n1
		endloopn1:	
				
				bne $s0, 2, writespace 	#not a negative number, so write
				mul  $s3, $s3, -1	#negative number, encode it as such
				j writespace
				
		writespace:
				bgt $s4, 0, store_n2	# if not 0...store as second number
				blt $s4, 0, store_n2
				add $s4, $0, $s3	# else store as num1

		store_n2:	add $s5, $0, $s3
				j finsp
				
		finsp:
				addi $a0, $v0, 0
				add $s3, $0, $0		#reset
				j wCalc
		
		opsign: 
			add $s1, $s0, $0
			addi $a0, $v0, 0
			#add $s3, $s0, 0
			j wCalc


Treat_enter:
    	
		bne $s7, 2, decide_op #INSERTS PREVIOUS IF THERE IS ONLY 1 NUMBER (2 SPACES).
		add $s4, $s6, $0
		
		decide_op:	
				beq $s1, 1, addem 
				beq $s1, 2, addem
				beq $s1, 3, mulem
				beq $s1, 4, divem
				
				
				
		addem:	add $t6, $s4, $s5
			add $s6, $t6, $0	#store result as prevresult
			
			addi $a0, $t6, 0	#load into a0 for writing
			j to_ascii
		
		subem:  sub $t6, $s4, $s5
			add $s6, $t6, $0
			
			addi $a0, $t6, 0
			j to_ascii
		
		mulem:  mul $t6, $s4, $s5
			add $s6, $t6, $0
			
			addi $a0, $t6, 0
			j to_ascii
		
		divem:	##ROUNDING IN FLOATING POINT
			lwc1 $f20, zerof		#load constants
			lwc1 $f22, ofive
			lwc1 $f24, hundred
			mtc1 $s4, $f4 			#move 
			cvt.s.w $f4, $f4 		#cast (int to float)
			mtc1 $s5, $f6 			#move
			cvt.s.w $f6, $f6 		#cast (int to float)
			div.s $f8, $f4, $f6		#perform division
			#rounding to 2 decimal places
			mul.s $f8, $f8, $f10 		#Multiply by 100.0
			c.lt.s $f8, $f20		#If sign is negative
			bc1t  minusf          	 	#Branch if
			add.s $f8, $f8, $f20		#else, positive, add 0.5
		endf:	cvt.w.s $f8, $f8 		#Cast to integer, truncate
			cvt.s.w $f8, $f8		#Cast back to float
			div.s $f8, $f8, $f10 		#Divide by 100.0
			j back
		minusf:	sub.s $f8, $f8, $f20	  	#-0.5
			j endf
			
		
		back:	mfc1 $s6, $f8		#convert to integer 
			
			
			div $t6, $s4, $s5	#floating point attempt abandoned at 2:19a.m. :(
			add $s6, $t6, $0
			addi $a0, $t6, 0
			j to_ascii
			
to_ascii: 	
    		
    		addi $a0, $0, 10	#new line
		jal Write
    		jal Result_txt
    		
    		add $t6, $s6, $0
    		
    		li  $v0, 1           # service 1 is print integer
    		add $a0, $t6, $zero  # load desired value into argument register $a0, using pseudo-op
    		syscall
    		li  $v0, 11           # service 1 is print integer
    		add $a0, $0, 37  # load desired value into argument register $a0, using pseudo-op
    		syscall
    		
negative_res:   
		bge $s6, 0, startascii_pos	#if positive number, start ascii looping, else print minus sign, and then positive
		addi $a0, $0, 45	#minus sign
		jal Write
		mul $s6, $s6, -1
		j startascii_neg
    		
startascii_pos: 
startascii_neg:	############ write a new line for result
		addi $t0, $t0, 1	#counter
		addi $t9, $s6, 0	#t9 holds result of s6	
		jal digits		#number of digits in s6 is now stored in t7
		addi $t3, $t7, -1	#start at 1 less than digits	
ascii_loop:	blt $t3, 0, end_ascii
		jal power		#we now have 10^t3 stored in t4
		div $t9, $t4		#get MSB, then second MSB, then third, etc
		mflo $a0		#number we want to write goes to a0
		mfhi $t9		#remainder gets looped on again
		addi $t3, $t3, -1
		addi $a0, $a0, 48	#convert digit to ascii
		jal Write
		j ascii_loop	

end_ascii:	
		addi $a0, $0, 10
		j wCalc2

wCalc2:		
	add $a1, $0, $0
	add $s0, $0, $0
	add $s1, $0, $0
	add $s2, $0, $0
	add $s3, $0, $0
	add $s4, $0, $0
	add $s5, $0, $0	
	add $s7, $0, $0
	add $t0, $0, $0
	add $t1, $0, $0
	add $t2, $0, $0
	add $t3, $0, $0
	add $t4, $0, $0
	add $t5, $0, $0	
	#add $t6, $0, $0
	add $t7, $0, $0	
	add $t8, $0, $0
	add $t9, $0, $0

	jal Write
	j Calc

Clear:
	add $a1, $0, $0
	add $s0, $0, $0
	add $s1, $0, $0
	add $s2, $0, $0
	add $s3, $0, $0
	add $s4, $0, $0
	add $s5, $0, $0
	add $s6, $0, $0 #clears memory WATCH OUT	
	add $s7, $0, $0
	add $t0, $0, $0
	add $t1, $0, $0
	add $t2, $0, $0
	add $t3, $0, $0
	add $t4, $0, $0
	add $t5, $0, $0	
	add $t6, $0, $0#
	add $t7, $0, $0	
	add $t8, $0, $0
	add $t9, $0, $0
	
	addi $a0, $0, 12
	j wCalc2	
		
wCalc:	
	jal Write
	j Calc
	#################
xCalc:	lw $ra,0($sp)
	addi $sp,$sp,4			#pop ra off stack
	jr $ra
	
	###############
###################################################
#input in t3, output 10^t3 in t4.
power: addi $t4, $0, 1
	add $t5, $0, $0 #clear t5
powloop:blt $t3, 0, endpow
	bge $t5, $t3, endpow
	addi $t5, $t5, 1
	mul $t4, $t4, 10
	j powloop
endpow:	jr $ra
#############################################################################################	
# calculates num digits in a word.
digits:	add $t8, $t9, 0 #input is in t9
	addi $t7, $0, 0 #counter
dig_loop: beq $t8, 0, end_dig
	  addi $t7, $t7, 1
	  div $t8, $t8, 10
	  j dig_loop
end_dig: jr $ra #return result in t7

##########################################################################################
Exit: 
	li $v0, 10				#error code 0
	syscall
