#Jacob Sanz-Robinson (260706158)
.data

#Must use accurate file path.
#These file paths are EXAMPLES, 
#should not work for you
#str1:	.asciiz "/Users/McGill/Courses/COMP-273/assignments/2017/Assign4/test1.txt"
#str3:	.asciiz "test-blur.pgm"	#used as output

str1:	.asciiz "/home/jacob/Desktop/hw4/test1.txt"
str3:	.asciiz "/home/jacob/Desktop/hw4/test-blur.pgm"
nope:	.asciiz "Oh noez. Error in file stuff."
chez:	.asciiz "hey \n"
l1:	.asciiz "P2\n24 7\n15\n"		#header for writefile

buffer:  .space 2048		# buffer for upto 2048 bytes

newbuff:.align 2 
	.space 672

tbuff:	 .align 2
	 .space 672		#temporary buffer. Too hipster to use newbuff. Needed align, because i was getting errors.

wbuff:	.align 2
	.space 672	
	
	.text
	.globl main

main:	la $a0, str1		#readfile takes $a0 as input
	jal readfile	
	
	jal blur	
		
	la $a1, newbuff 	#########prints blurred array
	add $s0, $zero, $zero
loopity:beq $s0, 700, nuff	
	addi $s0, $s0, 4
	add $a1, $s0, $a1			
	lw $t0, ($a1)
	li  $v0, 1           #print integer
   	add $a0, $t0, $zero  
    	syscall
	li  $v0, 11           #print integer
   	addi $a0, $zero, 32  
    	syscall		
	j loopity
nuff:


	jal writefile

	li $v0,10		# exit
	syscall

readfile:
#done in Q1

	li $v0, 13		# Open file
	li $a1, 0        	# Flags
	li $a2, 0		# Reading mode
	syscall
	move $s0, $v0     	# Save file descriptor for reading/closing 
	bltz $v0, error		# If there is an error, goto error
	
	li $v0, 14       	# Read file
	move $a0, $s0      	# File descriptor into a0 for reading 
	la $a1, buffer   	# address input
	li $a2, 2048     	# Max chars (buffer length)
	syscall
	bltz $v0, error		# If there is an error, goto error
	
	li $v0, 16       	# Close file
	move $a0, $s0      	# File descriptor to close
	syscall
	
	#li $v0, 4		# Prints buffer######################
	#la $a0, buffer			###############
	#syscall			 #########				
											
	la $a2, tbuff
	la $a0, buffer			#load pointer to buffer, to turn into cosecutive ints.
consecutive:	
	lb $t0, 0($a0)			#load buffer, byte by byte.
	addi $a0, $a0, 1		#increment buffer
	
	addi $s5, $s5, 1		#increment ascii counter
	slti $t1, $s5, 497		#check condition: while counter $s0 is less than 497, $t0 is true
	beq $t1, $zero, done		#if 497 bytes done, go to end of loop.

	beq $t0, 32, consecutive	#space, loop.
	beq $t0, 10, consecutive	#new line, loop.

	#if there is a single number
	
	addi $t4, $t0, -48		#Convert numbers from ASCII into decimal, store in $t4
	
	lb $t0, 0($a0)			#load next buffer char
	addi $a0, $a0, 1		#increment buffer
	
	addi $s5, $s5, 1		#increment ascii counter
	addi $s6, $s6, 4		#increment decimal counter
	slti $t1, $s5, 497		#check condition: while counter $s0 is less than 497 $t0 is true
	beq $t1, $zero, done		#if 2048 bytes done, end
	
	beq $t0, 32, onenum		#space, skip.
	beq $t0, 10, onenum		#new line, skip
	
	#if there are 2 numbers in a row
	addi $t5, $t0, -48		#convert from ascii to dec, store in $t5

	j twonum
	
onenum: #There is 1 number. Store as word in temp buffer.
	#i know at this point t4 has correct num
	add $a2, $a2, $s6
	sw $t4, ($a2)		#store in tbuff

	j consecutive
	

twonum: #There are 2 numbers in a row. Use positional notation to add stuff up, store as word in temp buffer.
	mul $t4, $t4, 10		#multiply t4 by 10 a la positional notation
	add $t6, $t4, $t5		#$t6 holds decimal representation
	#i know at this point t6 holds the correct num
	add $a2, $a2, $s6
	sw $t6, ($a2)		#store in tbuff

	j consecutive		
	
done:	#the decimal values are now stored in tbuff, as words.
	jr $ra
##############################################################################################################
blur: #tbuff with starting 2d array is a1. a2 is new buff, where blurred array will be. return in v1.

#use real values for averaging.
#HINT set of 8 "edge" cases.
#The rest of the averaged pixels will 
#default to the 3x3 averaging method
#we will return the address of our
#blurred 2D array in #v1

	la $a1,tbuff		#$a1 will specify the "2D array" we will be averaging
	la $a2,newbuff		#$a2 will specify the blurred 2D array.

	addi $s5, $zero, 0		#counter
	addi $s7 ,$zero, 24		#$s7 stores constant 24
loc:	#get current location, perform blurring
	addi $t0, $zero, 4
	mul $t0, $t0, $s5 
	add $a1, $a1, $t0
	lw $s0, ($a1)			#element from original array loaded into $s0
	
	slti $t1, $s5, 168		#check condition: while counter $s0 is less than 168 (7*24), $t0 is true
	beq $t1, $zero, xblur		#if 168 bytes done, go to end of loop.
	addi $s5, $s5, 1		#inc counter
	
	div $s5, $s7			#current/24
	mfhi $s1			#$s1 gets current mod 24. x coordinate (column).
	bne $s1, 0, casetf		#if 0, needs to be 24
	addi $s1, $zero, 24
casetf:	
	div $s5, $s7		#int division current/24
	mfhi $t0
	mflo $s2
	beq $t0, 0, casetft 
	addi $s2, $s2, 1		#s2 gets current y coordinate (row)	
	
casetft:
    	
	beq $s1, 1, edge		#if on edges, goto edge case
	beq $s1, 24, edge
	beq $s2, 1, edge
	beq $s2, 7, edge
	j avg				#else goto average
	
edge:	#copies itself to newarray
	addi $t0, $zero, 4
	mul $t0, $t0, $s5		#counter * 4
	add $a2, $a2, $t0
	addi $a2, $a2, -4		#store starting in cell 0
	sw $s0, ($a2)			#store untouched elem into newarray
	j fin

avg: #copies 9 immediate cells average (with int division) to newarray	
	addi $s4, $zero, 4 #constant 4
	
	add $t0, $zero, $s5 #THESE ARE WRONG
	addi $t0, $t0, -25 
	mul  $t0, $t0, $s4	
	addi $t0, $t0, -4
	add $t0, $t0, $a1
	lw $t1, ($t0)
	
	add $t0, $zero, $s5
	addi $t0, $t0, -24
	mul $t0, $t0, $s4
	addi $t0, $t0, -4
	add $t0, $t0, $a1
	lw $t2, ($t0)
	
	add $t0, $zero, $s5
	addi $t0, $t0, -23 
	mul $t0, $t0, $s4
	addi $t0, $t0, -4
	add $t0, $t0, $a1
	lw $t3, ($t0)
	
	add $t0, $zero, $s5
	addi $t0, $t0, -1
	mul $t0, $t0, $s4		
	addi $t0, $t0, -4
	add $t0, $t0, $a1
	lw $t4, ($t0)
	
	add $t0, $zero, $s5 #FOR SOME REASON, ONLY THIS ONE LOADS IN THE CORRECT VALUE
	addi $t0, $t0, 1
	mul $t0, $t0, $s4		
	addi $t0, $t0, -4
	add $t0, $t0, $a1
	lw $t5, ($t0)
	
	add $t0, $zero, $s5
	addi $t0, $t0, 0
	mul $t0, $t0, $s4		
	addi $t0, $t0, -4
	add $t0, $t0, $a1
	lw $t9, ($t0)
	
	add $t0, $zero, $s5
	addi $t0, $t0, 23
	mul $t0, $t0, $s4		
	addi $t0, $t0, -4
	add $t0, $t0, $a1
	lw $t6, ($t0)
	
	add $t0, $zero, $s5
	addi, $t0, $t0, 24
	mul $t0, $t0, $s4		
	addi, $t0, $t0, -4
	add $t0, $t0, $a1
	lw $t7, ($t0)
	
	add $t0, $zero, $s5
	addi, $t0, $t0, 25
	mul $t0, $t0, $s4		
	addi, $t0, $t0, -4
	add $t0, $t0, $a1
	lw $t8, ($t0)
	##########################
	
	add $s0, $s0, $t1 #add up totals, store in s0
	add $s0, $s0, $t2
	add $s0, $s0, $t3
	add $s0, $s0, $t4
	add $s0, $s0, $t5
	add $s0, $s0, $t6
	add $s0, $s0, $t7
	add $s0, $s0, $t8
	
	div $s0, $s0, 9		#divide total by 9, average over 9 values

	addi $t0, $zero, 4
	mul $t0, $t0, $s5		#counter * 4
	add $a2, $a2, $t0
	addi $a2, $a2, -4		#store starting in cell 0
	sw $s0, ($a2)			#store untouched elem into newarray
	addi $s0, $0, 0			#reset s0
	j fin
fin:	
	j loc
xblur:
	la $v1, newbuff
	jr $ra
##########################################################################################################
writefile: #$a0 has str3, $a1 has newbuff
	la $a0, str3		#writefile will take $a0 as file location


	li $v0, 13		# Open file
	la $a0,str3		# Address of file to write to
	li $a1, 1        	# Flags
	li $a2, 0		# Mode
	syscall
	move $s0, $v0     	# Save file descriptor for reading/closing 
	bltz $v0, error		# If there is an error, goto error
	
	li $v0, 15       	# Write file
	move $a0, $s0      	# File descriptor into a0 for writing
	la $a1, l1   		# address input
	li $a2, 11  		# Max chars (header length)
	syscall
	bltz $v0, error		# If there is an error, goto error
	#########################
	
	
	la $a1, newbuff		#$a1 takes location of what we wish to write.
	la $a2, wbuff
	add $s0, $zero, $zero	#counter initialized at 0
	add $s1, $zero, $zero 	#counter at 0
	addi $s7, $zero, 32	#constant (space)
wloop:	

	#NEED TO LW, AND THEN UNDO POSITIONAL NOTATION
	add $s2, $s0, $a1
	lw $t0, ($s2)			#load newbuff, byte by byte.
	addi $t0, $t0, 48		#back to ascii

	
	add $s3,$s1, $a2		#store wbuff, byte by byte
	sb $t0, ($s3)
	
	#if mod 24 = 0 then no spaces, else 1 or 2 spaces
	
	slti $t1, $s0, 497		#check condition: while counter $s0 is less than 497, $t0 is true
	beq $t1, $zero, donew		#if 497 bytes done, go to end of loop.
	addi $s0, $s0, 4
	addi $s1, $s1, 1
	j wloop

donew:
	########################
	li $v0, 15       	# Write file
	move $a0, $s0      	# File descriptor into a0 for writing
	la $a1, tbuff   	# address input
	li $a2, 2048     	# Max chars (buffer length)
	syscall
	bltz $v0, error		# If there is an error, goto error	

	li $v0, 16       	# Close file
	move $a0, $s0      	# File descriptor to close
	syscall
	
	jr $ra			#Back to main

error: #prints message if error is found, bleeds into exit.
	li $v0, 4			#prints error prompt
	la $a0, nope
	syscall
	
exit: nop
