#Jacob Sanz-Robinson (260706158)
# fileio.asm
	
.data

#Must use accurate file path.
#These file paths are EXAMPLES, 
#should not work for you
#str1:	.asciiz "/Users/McGill/Courses/COMP-273/assignments/2017/Assign4/test1.txt"
#str2:	.asciiz "/Users/McGill/Courses/COMP-273/assignments/2017/Assign4/test2.txt"
str3:	.asciiz "/home/jacob/Desktop/hw4/test.pgm"	#used as output
str1:	.asciiz "/home/jacob/Desktop/hw4/test1.txt"
str2:	.asciiz "/home/jacob/Desktop/hw4/test2.txt"
buffer:  .space 2048		# buffer for upto 2048 bytes
nope:	.asciiz "Oh noez. Error in file stuff."

l1:	.asciiz "P2\n24 7\n15\n"		#header for writefile

	.text
	.globl main

main:	la $a0, str1		#readfile takes $a0 as input
	jal readfile
	
	#la $a0, str3		#writefile will take $a0 as file location
	#la $a1,buffer		#$a1 takes location of what we wish to write.
	jal writefile

	li $v0,10		# exit
	syscall

readfile:

#Open the file to be read,using $a0
#Conduct error check, to see if file exists

# You will want to keep track of the file descriptor*

# read from file
# use correct file descriptor, and point to buffer
# hardcode maximum number of chars to read
# read from file

# address of the ascii string you just read is returned in $v1.
# the text of the string is in buffer
# close the file (make sure to check for errors)

	li $v0, 13		# Open file
	li $a1, 0        	# Flags
	li $a2, 0		# Reading mode
	syscall
	move $s0, $v0     	# Save file descriptor for reading/closing 
	bltz $v0, error		# If there is an error, goto error
	
	li $v0, 14       	# Read file
	move $a0, $s0      	# File descriptor into a0 for reading 
	la $a1, buffer   	# address input
	li $a2, 2048     	# Max chars (buffer length)
	syscall
	bltz $v0, error		# If there is an error, goto error
	
	li $v0, 4		# Prints buffer
	la $a0, buffer
	syscall
	
	li $v0, 16       	# Close file
	move $a0, $s0      	# File descriptor to close
	syscall

	jr $ra			# Back to main
	
writefile:
#open file to be written to, using $a0.
#write the specified characters as seen on assignment PDF:
#P2
#24 7
#15
#write the content stored at the address in $a1.
#close the file (make sure to check for errors)

	li $v0, 13		# Open file
	la $a0,str3		# Address of file to write to
	li $a1, 1        	# Flags
	li $a2, 0		# Mode
	syscall
	move $s0, $v0     	# Save file descriptor for reading/closing 
	bltz $v0, error		# If there is an error, goto error
	
	li $v0, 15       	# Write file
	move $a0, $s0      	# File descriptor into a0 for writing
	la $a1, l1   		# address input
	li $a2, 11  		# Max chars (header length)
	syscall
	bltz $v0, error		# If there is an error, goto error
	
	li $v0, 15       	# Read file
	move $a0, $s0      	# File descriptor into a0 for writing
	la $a1, buffer   	# address input
	li $a2, 2048     	# Max chars (buffer length)
	syscall
	bltz $v0, error		# If there is an error, goto error	

	li $v0, 16       	# Close file
	move $a0, $s0      	# File descriptor to close
	syscall

	jr $ra			#Back to main

error: #prints message if error is found, bleeds into exit.
	li $v0, 4			#prints error prompt
	la $a0, nope
	syscall

exit:	nop
