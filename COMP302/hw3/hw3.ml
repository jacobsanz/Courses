(*Jacob Sanz-Robinson (260706158)
Chenthuran Sivanandan (260749843)*)


(* Question 1 - Unfolding *)

(* This is the function unfold, take some time to compare it it fold.
   If fold_left and fold_right take lists to generate a value, this
   function takes a value that will generate a list. The relation
   between folds and unfolds is the beginning of a wonderful tale in
   computer science. But let's not get ahead of ourselves.

   Unfold takes a function that from a seed value it generates a new
   element of the list, and a the seed for the next element, another
   function to stop the generation, and an initial seed.
*)

let rec unfold (f: 'seed -> ('a * 'seed)) (stop : 'b -> bool) (b : 'seed) : 'a list =
  if stop b then []
  else let x, b' = f b in
       x :: (unfold f stop b')

let nats max = unfold (fun b -> b, b + 1) (fun x -> x > max) 0

(* Q1.1: Return the even numbers up-to max *)
let evens max = unfold (fun b -> b, b + 2) (fun x -> x > max) 0

(* Q1.2: Return the Fibonacci sequence up-to max *)
let fib max = unfold (fun (x,y) -> (x,(y,x+y))) (fun (x,y) -> x > max) (1,1)

(* Q1.3: Return the list of rows of the Pascal triangle that are shorter than max *)
let pascal max =
  let get_next row = (*generates the next row from the previous one*)
    let first = [0]@row in
    let second = row@[0] in
    let rec add l1 l2 = (*this function does element-wise addition of two lists*)
      match l1,l2 with
      | [], [] -> []
      | [h1], [h2] -> [h1+h2]
      | h1::t1, h2::t2 -> (h1+h2)::(add t1 t2)
      in
    add first second (*these two lists added to one another generate the next row*)
  in
  unfold (fun row -> (row, get_next row)) (fun row -> List.length row >= max) [1]

let rec zip (l1 : 'a list) (l2 : 'b list) :  ('a * 'b) list =
match l1, l2 with
| [], _ -> []
| _, [] -> []
| x::xs, y::ys -> (x, y):: zip xs ys

(* (Extra credit) Optional: implement zip with a single call to unfold *)
let zip' l1 l2 = unfold (fun (h1::t1,h2::t2) -> (h1,h2),(t1,t2)) (fun (x,y)->x=[] || y=[]) (l1,l2)

(* Question 2 *)

let ugly x =
  let rec ackermann m n = match (m , n) with
    | 0 , n -> n+1
    | m , 0 -> ackermann (m-1) 1
    | m , n -> ackermann (m-1) (ackermann m (n-1))
  in
  ackermann 3 x

let memo_zero (f : 'a -> 'b) : 'a -> 'b = f

(*Q2.1: Write a function that memoizes the last value called. *)
let memo_one (f : 'a -> 'b) : ('a -> 'b) =
  let store = ref None in
 (fun elem ->   
    match !store with
    | Some (x,y) when (elem = x) -> y (*return stored y if x=elem*)
    | None -> store := Some (elem,f elem); f elem (*else store what's needed, and return the calc*)
    | Some (x,y) when (elem <> x)-> store := Some (elem,f elem); f elem
 )

(*
let ugly' = memo_one ugly

let u1 = ugly' 3                (* this one calls ugly with 3 *)
let u2 = ugly' 3                (* this one uses the stored value *)
let u3 = ugly' 1                (* the stored value is for 3 so it calls ugly *)
let u4 = ugly' 2                (* the stored value is for 1 so it calls ugly *)
let u5 = ugly' 10               (* the stored value is for 2 so it calls ugly and takes a couple of seconds *)
let u6 = ugly' 10               (* the one uses the stored value and returns immediately *)
*)

 (* Q2.2: Write a function that memoizes the last value called. *)
 let memo_many (n : int) (f : 'a -> 'b) : 'a -> 'b =
  let l2 = ref [] in (* this list is to store all the tuples *)
  let l3 = ref [] in (* this last has everything except for the last element *)
  
  fun a -> try 
           match (List.assoc a !l2) with 
           |x -> x
           with Not_found ->  (* if the value is not in the list then update the list *) 
           match (List.length !l3 < (n-1)) with
           |true -> l3 := !l3 @ [(a,f a)] ; l2 := !l3 ; f a (* since the list is not full store dont take out the last value *)
           |_ -> l2 := !l3 @ [a,f a] ; f a (* since the list is full take out the last value *)

(* Question 3: Doubly-linked circular lists  *)

(* Circular doubly linked lists *)

(* The type of a cell (a non-empty circular list) *)
type 'a cell = { mutable p : 'a cell; data : 'a ; mutable n : 'a cell}

(* The type of possibly empty circular lists *)
type 'a circlist = 'a cell option

(* An empty circular list *)
let empty :'a circlist = None

(* A singleton list that contains a single element *)
let singl (x : 'a) : 'a circlist =
  let rec pointer = {p = pointer ; data = x ; n = pointer} in
  Some pointer

(* Rotate a list to next element *)
let next : 'a circlist -> 'a circlist = function
  | None -> None
  | Some cl -> Some (cl.n)

(* Rotate a list to previous element *)
let prev : 'a circlist -> 'a circlist = function
  | None -> None
  | Some cl -> Some (cl.p)

  (* Q3.1: Write a function that add a new element at the beginning of a list *)
let cons (x : 'a)  (xs : 'a circlist) : 'a circlist =
  match xs with
  | None -> singl x
  | Some h -> (*treat first element found as head*)
              let node = {p = h.p ; data = x ; n = h} in
              h.p.n <- node; (*change the pointers around*)
              h.p <- node;
              Some h

(* Q3.2: Write a function that computes the length of a list (Careful with those infinite loops, Eugene)  *)
let rec length (l : 'a circlist) : int =
  let rec lengthy beginning temp count = (*traverse through the circlist until we find the initial address, incrementing counter*)
    match temp with 
    | node when (temp.n != beginning) -> lengthy (beginning) (temp.n) (count+1)
    | node when (temp.n == beginning) -> (count+1) (*If statements are a thing of the past*)
    | _ -> count
  in
  match l with
  | None -> 0
  | Some node -> lengthy node node 0 (*beginning and temp are first node*)

(* Q3.3: Write a function that produces an immutable list from a circular list *)
let to_list (l : 'a circlist)  : 'a list = 
  let rec makelist beginning temp l = (*same idea as in 3.2, but now appending to a list. list l is acc *)
    match temp with
    | node when (temp.n != beginning) -> makelist (beginning) (temp.n) ([temp.data] @ l)
    | node when (temp.n == beginning) -> ([temp.data] @ l)
    | _ -> l
  in
  match l with
  | None -> []
  | Some node -> makelist node node [] (*call with empty list as acc*)

(* Once you've written cons you can use this function to quickly populate your lists *)
let rec from_list : 'a list -> 'a circlist = function
| [] -> empty
| x::xs -> cons x (from_list xs)

(* Q3.4: Write a function that reverses all the directions of the list *)
let rev (l : 'a circlist) : 'a circlist =
(*when it's run in-place (destructively) the to_list is strange.*)
(*FOR A VERSION THAT IS NOT IN-PLACE PLEASE SEE THE REV FUNCTION BELOW THIS ONE!!!*)
  let rec revy beginning temp =
    let backup = temp.n in (*backup of the current node we are on.*)
    temp.n <- temp.p; (*change the fields of every node we encounter.*)
    temp.p <- backup;
    match temp with (*recursively iterate. Unless it's the head.*)
    | node when (backup != beginning) -> revy beginning backup
    | node when (backup == beginning) -> beginning
    | _ -> beginning
  in
  match l with
  | None -> l
  | Some node -> Some (revy node node)

(*
(*this version IS NOT IN PLACE, it creates a new list...just in case it is what was wanted*)
let rev (l : 'a circlist) : 'a circlist = 
  
  let constr (x : 'a)  (xs : 'a circlist) : 'a circlist =
    match xs with
    | None -> singl x
    | Some h -> 
                let node = {p = h.p ; data = x ; n = h} in
                h.p.n <- node;
                h.p <- node;
                Some h
  in
  
  let theempty = empty in
  
  let rec revtwo beginning temp l =
  let b = temp.data in
  match temp with
  | node when temp.p != beginning -> revtwo (beginning) (temp.p) (constr b l)
  | node when temp.p == beginning -> (constr temp.data l)
  |_ -> l
  in
  
  match l with 
  None -> None
  |Some node -> revtwo node.p node.p theempty
*)

(*(Extra credit) OPTIONAL: Write the map function as applied to lists *)
(*Just like in 3.4, we were unsure whether this should be in-place or return a completely different string. Here it is NOT in place.*)
(*example usage : to_list (map (fun x -> x+1) (from_list [1;2;3;4]));; *)
let map (f : 'a -> 'b) : 'a circlist -> ' b circlist = 
  let constr (xs : 'a circlist) (x : 'a): 'a circlist = (* same functions as in 3.1 *)
    match xs with
    | None -> singl x
    | Some h -> 
                let node = {p = h.p ; data = x ; n = h} in
                h.p.n <- node;
                h.p <- node;
                Some h
  in
  let theempty = empty in
  let rec mapping beginning temp l (f : 'a -> 'b) =
    let b = f (temp.data) in (* apply f on our current node *)
    match temp with
    | node when temp.n != beginning -> mapping (beginning) (temp.n) (constr l b) f (* call f on the node and add it to the new list *)
    | node when temp.n == beginning -> (constr l b) (* if we have reached the beginning give the list *) 
    |_ -> l
  in
  fun a -> match a with (* our function *)
           None -> None
           |Some node -> (mapping node node theempty f)

(* Some possibly useful functions (Wink, wink!) *)

(* A function that returns the Greatest Common Denominator of two numbers *)
let rec gcd (u : int) (v : int) : int =
  if v <> 0 then (gcd v (u mod v))
  else (abs u)

(* A function that returns the least common multiple of two numbers *)
let lcm (m : int) (n : int) : int  =
  match m, n with
  | 0, _ | _, 0 -> 0
  | m, n -> abs (m * n) / (gcd m n)


(* (Extra credit) OPTIONAL A function that compares two lists ignoring the rotation *)
let eq (l1 : 'a circlist) (l2 : 'a circlist) : bool = assert false

(* Some examples *)
(*
let ex = cons 12 (cons 43 (cons 34 (singl 3)))
let lex = to_list ex

let l1 = from_list [true; true ; false]
let l3 = from_list [true; true ; false ; true; true ; false]

let l4 = from_list ['a'; 'b'; 'a'; 'b']
let l5 = from_list ['a'; 'b'; 'a'; 'b'; 'a'; 'b']

let l6 = from_list ['a'; 'a']
let l7 = from_list ['a'; 'a'; 'a']

let l8 = from_list [1 ; 2 ; 3]
let l9 = from_list [3 ; 1 ; 2]  (* eq l8 l9 = true *)

let l10 = from_list [1 ; 2 ; 3]
let l11 = from_list [3 ; 2 ; 1]  (* eq l10 l11 = false *)
*)