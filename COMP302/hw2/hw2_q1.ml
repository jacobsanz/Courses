(* Jacob Sanz-Robinson: 260706158 and Chenthuran Sivanandan: 260749843*)
type prop = Atom of string
          | Not of prop
          | And of prop * prop
          | Or of prop * prop

let impl (p, q) = Or(Not p, q)
let iff (p, q) = And (impl (p,q), impl (q, p))

let mp = impl (And (Atom "P", impl (Atom "P", Atom "Q")), Atom "Q")

(* Proof by contradiction (reduction ad absurdum): ((¬ P) ⇒ Q) ∧ ((¬ P) ⇒ (¬ Q)) ⇒ P *)
let ra = impl (
             And (impl (Not (Atom "P"),
                        Atom "Q"),
                  impl (Not (Atom "P"),
                        Not (Atom "Q"))),
             Atom "P")

(* Atoms and their negations *)
type signed_atom
  = PosAtom of string
  | NegAtom of string

(* In NNF negations can only be applied to atoms, this datatype only
   allows for propositions in NNF *)
type nnf
  = AndN of nnf * nnf
  | OrN of nnf * nnf
  | AtomN of signed_atom

(* Q1.2: Write the function nnf that converts propositions into NNF,
   notice that the typechecker will guide you in your answer as the
   type forces the result to be in NNF. Your job is to not forget any
   sub-parts of the proposition in the conversion. *)
let rec to_nnf : prop -> nnf = function
  |Not(Not (p)) -> to_nnf p (*call the function on p because we do not know if it is an atom *)
  |Not(Or(p,q)) -> to_nnf (And(Not p, Not q)) (*convert it to what it is in prop and then apply to nnf it is easier that way *)
  |Not(And(p,q)) -> to_nnf (Or(Not p, Not q)) (* convert it to what it is in prop and then apply to nnf it is easier that way *)
  |And(p,q) -> AndN(to_nnf p, to_nnf q) (* put it in its nnf form and then apply the function to p and q *)
  |Or(p,q) -> OrN(to_nnf p, to_nnf q) (* put it in its nnf form and then apply the function to p and q *)
  |Atom p -> AtomN(PosAtom p) (* since it is an atom just return it in it's nnf form *)
  |Not(Atom p) -> AtomN(NegAtom(p))

(* Q1.3: Write a datatype cnf that represents only propositions in
   cnf. Hint: You might want to use more than one type to be able to
   represent sub-expressions.*)

type orr =
 Orc of orr*orr
 |Atomd of signed_atom

type cnf = 
 Andc of cnf*cnf (* cnf can have disjunctions and conjuctions in it *)
 |AtomC of orr (* this way our or statements only have other or statements or atoms in them *)


(* Q1.4: Write the distribute and nnf_to_cnf functions using the new
   datatype. Hint: you may need more than one helper function. *)
let tosigned:cnf -> orr = function
 AtomC(Atomd(PosAtom q)) -> Atomd(PosAtom q) (* this function allows us to return a cnf with an orr type in our distribute *)
 |AtomC(Atomd(NegAtom q)) -> Atomd(NegAtom q)
 |AtomC(Orc(p,q)) -> Orc(p,q) 

let rec distribute : cnf * cnf -> cnf = function
  |p, Andc(q,r)->Andc(distribute(p,q),distribute(p,r)) (* if we have any ands we need to put them togeather, call what is on the inside to figure out what to do with that *)
  |Andc(q,r),p->Andc(distribute(q,p),distribute(r,p)) 
  |p,q -> AtomC(Orc(tosigned p, tosigned q)) (* if there are no and's put both statements in an or *)

let rec nnf_to_cnf : nnf -> cnf = function
  |AndN(p,q)->Andc(nnf_to_cnf p,nnf_to_cnf q)
  |OrN(p,q)->distribute(nnf_to_cnf p,nnf_to_cnf q)
  |AtomN(PosAtom(p))->AtomC(Atomd(PosAtom(p)))
  |AtomN(NegAtom(p))->AtomC(Atomd(NegAtom(p)))

let to_cnf (p :prop) : cnf = nnf_to_cnf (to_nnf p)

(* Q1.5: Write the new positives and negative atoms function as in the
   previous version *)
let rec positives = function
  |AtomC(Atomd(PosAtom(p)))->[p]
  |AtomC(Atomd(NegAtom(p)))->[]
  |Andc(p,q)->positives p @ positives q
  |AtomC(Orc(p,q))->positives (AtomC(p)) @ positives (AtomC(q)) (* the part AtomC(p) and AtomC(q) ensures that we are calling the function on a cnf *)
  |_->raise (Invalid_argument "not in cnf")

let rec negatives = function
  |AtomC(Atomd(NegAtom(p))) -> [p]
  |AtomC(Atomd(PosAtom(p))) -> []
  |Andc(p,q) -> negatives p @ negatives q
  |AtomC(Orc(p,q)) -> negatives (AtomC(p)) @ negatives (AtomC(q)) 
  |_->raise (Invalid_argument "not in cnf")

(* Fill in the code for the intersection function from Q1.1 *)
let rec intersection l1 l2 = 
let rec has x l=
match l with
[] -> false 
|h::t -> 
        if(x=h) then (* this function checks if a given element is in a list *)
         true
        else
        has x t
in
match l1 with
[] -> []
|h::t -> 
       if(has h l2) then
        h::(intersection t l2) (* check if the element in the first list is in the second list *)
       else
        intersection t l2 (* if it is not check if the other elements are in the list *)

(* Q1.6: Write the new cnf_tautology function *)
let rec cnf_tautology : cnf -> bool = function
  |Andc(p,q)->cnf_tautology p && cnf_tautology q
  |p -> not([]=intersection (positives p) (negatives p)) 

let taut (p : prop) : bool = cnf_tautology (to_cnf p)
let unsat (p : prop) : bool = taut (Not p)
let sat (p : prop) : bool = not (unsat p)
