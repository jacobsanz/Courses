(* Student information:

   Enter your name, and if you chose to work in pairs, the name of the
   student you worked with (both students MUST submit the solution to
   myCourses):

   Name: Jacob Sanz-Robinson
   McGill ID: 260706158

   If you worked in pairs, the name of the other student.

   Name: Chenthuran Sivanandan
   McGill ID: 260749843


 *)





module Exp =
struct
  type name   = string
  type primop = Equals | LessThan | Plus | Minus | Times | Negate

  type exp =
    | Var of name
    | Int of int                      (* 0 | 1 | 2 | ... *)
    | Bool of bool                    (* true | false *)
    | If of exp * exp * exp           (* if e then e1 else e2 *)
    | Primop of primop * exp list     (* e1 <op> e2  or  <op> e *)
    | Fn of name * exp                (* fn x -> e *)
    | App of exp * exp                (* e1 e2 *)
    | Let of dec * exp                (* let dec in e end  *)
    | Pair of exp * exp               (* (e1, e2)  *)

  and dec =
    | Val of exp * name               (* x = e *)
    | Match of exp * name * name      (* x, y = e  *)


  (* ---------------------------------------------------------------- *)
  (* Generating new variable names *)

  let genCounter =
  let counter = ref 0 in
  ((fun x ->
    let _ = counter := !counter+1 in
    x ^ string_of_int (!counter)),
  fun () ->
    counter := 0)

  let (freshVar, resetCtr) = genCounter

 (* ---------------------------------------------------------------- *)
 (* Basic functions about lists *)

  let member x l = List.exists (fun y -> y = x) l

  let rec delete (vlist, l) = match l with
    |  [] -> []
    |  h :: t ->
       if member h vlist then delete (vlist, t)
       else h :: delete (vlist, t)

  let rec union p = match p with
  | ([], l) -> l
  | (x::t, l) ->
    if member x l then
      union (t, l)
    else
      x :: union (t, l)

  (* ---------------------------------------------------------------- *)
  (* Computing the set of free variables in an expression *)

  (* Q1.2: extend the function for Pair(_, _) and Let (Match(_, _, _), _) *)

  let rec freeVars e = match e with
  | Var y -> [y]
  | Int n -> []
  | Bool b -> []
  | If(e, e1, e2) ->
    union (freeVars e, union (freeVars e1, freeVars e2))
  | Primop (po, args) ->
    List.fold_right (fun e1 fv -> union (freeVars e1, fv)) args []
  | App (e1, e2) -> union (freeVars e1, freeVars e2)
  | Fn (x, e) -> delete ([x], freeVars e)
  | Let (Val (e1, x), e2) ->
      union (freeVars e1, delete ([x], freeVars e2))
  | Pair (e1, e2) -> union (freeVars e1, freeVars e2)
  (*| Let (Match (e1, x, y), e2) -> union (freeVars e1, delete ([y], delete ([x], freeVars e2)))*)
  | Let (Match (e1, x, y), e2) -> union (freeVars e1, delete ([x;y], freeVars e2))

  (* ---------------------------------------------------------------- *)
  (* Substitution
   subst : (exp * name) -> exp -> exp

   subst (e',x) e = [e'/x]e

   subst replaces every occurrence of the variable x
   in the expression e with e'.
  *)

  (* Q1.4: extend subst for Pair(_, _) and both Let (Match(_,_,_), _) *)
  let rec subst (e',x as s) e =
    match e with
    | Var y ->
       if x = y then e'
       else Var y
    | Int n  -> Int n
    | Bool b -> Bool b
    | Primop(po, args) ->
       Primop(po, List.map (subst s) args)
    | If(e, e1, e2) ->
       If(subst s e, subst s e1, subst s e2)
    | App (e1, e2) -> 
       App (subst s e1, subst s e2)
    | Fn (y, e) -> 
       if x = y then Fn (y, e)
       else 
         if member y (freeVars e') 
         then
           let y' = freshVar y in 
           let e' = rename (y', y) e in 
             Fn (y', subst s e')
         else 
           Fn (y, subst s e)
    | Let (Val(e1,y), e2) -> 
       let e1' = subst s e1 in 
       if x = y then 
         (* optimization: don't traverse e2 as there is not free occurrence of x in e2 *)
         Let (Val (e1', y), e2) 
       else 
         if member y (freeVars e') then 
           let y'  = freshVar y in 
           let e2' = rename (y', y) e2 in 
             Let(Val(e1', y'), subst s e2')
         else 
           Let(Val(e1', y), subst s e2)
     | Let (Match (e1, a, b), e2) -> let e1' = subst s e1 in
                                    if (a=x || b=x) then
                                      Let (Match (e1',a,b),e2)
                                    else
                                      if (member a (freeVars e') && member b (freeVars e')) then
                                        let a' = freshVar a in
                                        let b' = freshVar b in
                                        let e2'' = rename (a',a) e2 in
                                        let e2' = rename (b',b) e2'' in
                                          Let (Match (e1',a',b'), subst s e2')
                                      else
                                        Let (Match (e1',a,b),subst s e2)
    | Pair (e1, e2) -> Pair(subst s e1, subst s e2)
   

  and rename (x', x) e = subst (Var x', x) e
end

module Types =
  struct
    module E = Exp

    type tp = Int | Bool | Arr of tp * tp | TVar of (tp option) ref | Prod of tp * tp         

    let rec member l r = match l with
      | [] -> None
      | (a, r') :: l' -> if r = r' then Some a else member l' r

    let typ_to_string t = 
      let rec to_str t l = match t with
        | Int -> ("Int", l)
        | Bool -> ("Bool", l)
        | Arr (t1, t2) ->  
           let (s1, l1) = to_str t1 l in
           let (s2, l2) = to_str t2 l1 in 
           (s1 ^ " -> " ^ s2  , l2)
        | Prod (t1, t2) -> 
           let (s1, l1) = to_str t1 l in
           let (s2, l2) = to_str t2 l1 in 
           (s1 ^ " * " ^ s2  , l2)
        | TVar ({contents = None} as r) -> 
           begin try 
             (List.assoc r l , l)
           with Not_found -> 
             let a = Exp.freshVar "a" in (a, (r,a)::l)
           end 
        | TVar ({contents = Some t}) -> 
           to_str t l
      in 
       let (s, _ ) = to_str t [] in s

    exception TypeError of string
    exception Error of string

    let fail message = raise (TypeError message)

    let freshVar () = TVar (ref None)
    
    let rec occurs s t = match s, t with
      | _, Int -> false
      | _, Bool -> false
      | _, Arr (t1, t2) ->
         (occurs s t1) || (occurs s t2)
      | _, Prod (t1, t2)  ->
         (occurs s t1) || (occurs s t2)
      | _, TVar r ->
         match !r with
         | None -> (s == r)
         | Some t' -> (s == r) || (occurs s t')

   (* OPTIONAL: Extra Credit  *)
 let rec unify s t = 
    match s,t with
    |Int,Int -> ()
    |Bool,Bool -> ()
    |TVar(e),TVar(e1) -> (match !e,!e1 with 
                          | None,None -> let g = freshVar () in
                                          e1:= Some g;
                                          e:= Some g
                          | Some a,None -> if (occurs e t) then
                                            fail ("not unifiable")
                                           else e1 := Some a
                          | None,Some a ->  if (occurs e1 s) then
                                             fail ("not unifiable")
                                            else e := Some a
                          | Some a,Some b -> unify a b
                          )
    |TVar(e),a |a,TVar(e) ->( match !e with
                              None -> if (occurs e a) then
                                         fail ("not unifiable")
                                      else e := Some a
                              |Some b -> unify b a
                            )
    |Arr(a,b), Arr(c,d) |Prod(a,b), Prod(c,d) -> unify a c;unify b d
    | _ -> fail("not unifiable")

  let unifiable (t, t') = 
    try
      let _ = unify t t' in true
    with Error _s -> false


  type ctx = (E.name * tp ) list

  let lookup n g =
    try
      List.assoc n g
    with
      _ -> fail ("Could not find variable in the context")
        
    (* primopType p = (argTypes, returnType) *)
  let primopType p = match p with
    | E.Equals   -> ([Int; Int], Bool)
    | E.LessThan -> ([Int; Int], Bool)
    | E.Plus     -> ([Int; Int], Int)
    | E.Minus    -> ([Int; Int], Int)
    | E.Times    -> ([Int; Int], Int)
    | E.Negate   -> ([Int], Int)
       

    (* Extra Credit – modify your inference algorithm from Q1.6 
       to handle functions and function application; for simplicity, 
       you do not have to and support let-polymorphism. 

       Instead, support polymorphism in let-expressions by using the following rule: 

        Gamma |- e1 : T1
        Gamma |- [e1/x]e2 : T
        ————————————————————————      
        Gamma |- let x = e1 in e end : T

    *)
       
  let rec infer g e = match e with
   | E.Int _ -> Int
    | E.Bool _ -> Bool
    | E.If (e, e1, e2) -> unify (infer g e) Bool;
                          let r = infer g e1 in
                          let r2 = infer g e2 in
                          unify r r2; r
    | E.Primop (po, args) -> 
       let (expected_arg_types, resultType) = primopType po in
       let inferred_arg_types = List.map (infer g) args in

       let rec compare tlist1 tlist2 = match tlist1, tlist2 with
         | [] , [] -> resultType
         | t::tlist , s::slist -> (match s with
                                  |TVar x -> unify t s;compare tlist slist
                                  | _ -> if t = s then compare tlist slist
            else fail ("Expected " ^ typ_to_string t ^
                       " - Inferred " ^ typ_to_string s))
         | _ , _ -> fail ("Error: Primitve operator used with incorrect number of arguments")
       in
         compare expected_arg_types inferred_arg_types
    | E.Var x -> lookup x g
    | E.Let (E.Val (e1, x), e2) -> infer g (E.subst (e1,x) e2)
    | E.Pair (e1, e2) -> Prod(infer g e1, infer g e2)
    | E.Let (E.Match (e1, x, y), e2) -> let g1 = E.subst (e1,x) e2 in
                                        let g2 = E.subst (e1,y) g1 in 
                                        infer g g2     
      
    | E.Fn(a,b) -> let alpha = freshVar () in
                        Arr(alpha,infer ((a,alpha)::g) b)
    | E.App(a,b) -> match a with
                    E.Fn(c,d) -> infer ((c,(infer g b))::g) a
                    | _ -> fail ("wrong types")
end




(* TESTCASES FOR UNIFICATION *)

(* Equality testing on types                                         *)
(* equal(t,s) = bool
   
   checks whether type t and s are equal 
  
   equal:tp * tp -> true 
*)


module T = Types

let rec equal s t = match s, t with
  | T.Int, T.Int -> true
  | T.Bool, T.Bool -> true
  | T.Arr (t1, t2), T.Arr(s1, s2) ->
     (equal t1 s1) && (equal t2 s2)
  | T.Prod (t1, t2), T.Prod(s1, s2) ->
     (equal t1 s1) && (equal t2 s2)
  | T.TVar {contents = Some s'}, _ ->
     equal s' t
  |  _, T.TVar {contents = Some t'} ->
     equal s t'
  | T.TVar x, T.TVar y ->
     x == y
  | _,_ -> false


(* -------------------------------------------------------------------------*)
(* Some test cases *)
(* -------------------------------------------------------------------------*)
(* Define some type variables *)
let a1 : (T.tp option) ref = ref(None);;
let a2 : (T.tp option) ref = ref(None);;

let a3 : (T.tp option) ref = ref(None);;
let a4 : (T.tp option) ref = ref(None);;

let a5 : (T.tp option) ref = ref(None);;
let a6 : (T.tp option) ref = ref(None);;

let a7 : (T.tp option) ref = ref(None);;
let a8 : (T.tp option) ref = ref(None);;

(* Define some types *)
let t1 = T.Arr(T.Prod (T.TVar a1 , T.TVar a1), T.TVar a2);;
let t2 = T.Arr(T.Prod (T.Int , T.Int), T.TVar a1);;

let t3 = T.Arr(T.Prod (T.TVar a3 , T.TVar a4), T.TVar a4);;
let t4 = T.Arr(T.Prod (T.TVar a4 , T.TVar a3), T.TVar a3);;

let t5 = T.Arr(T.Prod (T.TVar a5 , T.TVar a6), T.TVar a6);;
let t6 = T.Arr(T.TVar a6, T.TVar a5);;


(* Tests *)

T.unify t1 t2;;
(* val it = () : unit *)
T.toString t1;;
(* val it = "((int ) * (int )) -> (int )" : string *)
T.toString t2;;
(* val it = "((int ) * (int )) -> (int )" : string *)
equal t1 t2;;
(* val it = true : bool *)
T.unify t3 t4;;
(* val it = () : unit *)
T.toString t3;;
(* val it = "((a1) * (a1)) -> (a1)" : string *)
T.toString t4;;
(* val it = "((a1) * (a1)) -> (a1)" : string *)
equal t3 t4;;
(* val it = true : bool *)
equal t5 t6;;
(* val it = false : bool *)
T.unify t5 t6;;
(* uncaught exception Error
  raised at: ...
 *)

