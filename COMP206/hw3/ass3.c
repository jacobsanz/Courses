// Jacob Sanz-Robinson (260706158)

#include <stdio.h>
#include <stdlib.h>

void encrypt(char sentence[], int key);
void decrypt(char sentence[], int key);

int main()
{
    int key; //initialize vars
    char sentence[256]; //Waaaaay above the average sentence length in the english language (75-100 chars).

    printf("Sentence: "); //prompts to get sentence and key.
    fgets(sentence, sizeof(sentence), stdin); //fgets is far less annoying than scanf for strings.
    printf("Key: ");
    scanf("%d", &key);

    if (key < 0 || key > 25) { //get a key within the right range
        printf(" 1<key<25.\n");
        return 3; //error used for debugging.
    }
    printf("Original Message: "); //prints and calls.
    printf("%s", sentence);
    printf("Encrypted Message: ");
    encrypt(sentence, key);
    printf("%s", sentence);
    printf("Decrypted Message: ");
    decrypt(sentence, key);
    printf("%s", sentence);

    return 0;
}

void encrypt(char sentence[], int key)
{
    int i = 1;
    while (sentence[i] != '\0'){ //loop through the sentence as long as we don't encounter null.
        int current = sentence[i-1]; //load current car
        int new_current;
        if (current == 32){ //if space
            new_current = 32;
        }
        if (current > 96 && current < 123){ //if lowercase
            current -= 'a'; //follow formula given in pdf to get value from 0-25
            new_current = (current - key + (26*key)) % 26; //shift left without ever going negative
            new_current += 'a'; //back to ascii value
        }
        if (current > 64 && current < 91) { //else is uppercase
            current -= 'A'; //same as above, but for uppercase
            new_current = (current - key + (26*key)) % 26;
            new_current += 'A';
        }
        sentence[i-1] = new_current; //place it back in the array
        i++; //move index to next char.
    }

}

void decrypt(char sentence[], int key) //Same as encrypt function, with one change on the modulus operation.
{ 
    int i = 1;
    while (sentence[i] != '\0'){ 
        int current = sentence[i-1]; 
        int new_current;
        if (current == 32){ 
            new_current = 32;
        }
        if (current > 96 && current < 123){ 
            current -= 'a'; 
            new_current = (current + key) % 26; //follow operation given in pdf for modulus change
            new_current += 'a'; 
        }
        if (current > 64 && current < 91) { 
            current -= 'A'; 
            new_current = (current + key) % 26;
            new_current += 'A';
        }
        sentence[i-1] = new_current; 
        i++;
    }
}



