#!/bin/bash

#MakeProject.sh - COMP206 assignment2

#Store name. Change path to home using abs path. Error if no name.
name=$1
cd ~
if [ "$name" = "" ]
then
	echo 'Gimme gimme gimme a name after midnight'
	exit
fi

#verifies if project subdir exists do nothing, else create it. Moves IN AND AROUND project using relative path.
if [ -d "project" ]
then	:
	else
		mkdir project
	fi
cd ./project


#verifies if cs206 sub exists, else create. move into it.
if [ -d "cs206" ]
then    :
        else
                mkdir cs206
        fi
cd ./cs206

#verifies if name sub exists, error. Else creates name sub  with all its subs. Moves to source.
if [ -d "$name"  ]
then
	echo 'This project name has already been used.'
	else
		mkdir $name
		cd ./$name
		mkdir archive
		mkdir backup
		mkdir docs
		mkdir assets
		mkdir database
		mkdir source
	fi

cd ./source

#creates backup.sh file. Appends needed text into it. Gives permissions to use.
echo '#!/bin/bash'  >> backup.sh
echo 'cp *.c ../backup; cp *.j ../backup' >> backup.sh
chmod a+rwx backup.sh

#last bit
echo 'You project directories have been created.'
