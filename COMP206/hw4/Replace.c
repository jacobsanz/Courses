// Jacob Sanz-Robinson (260706158)
#include <stdio.h>
#include <stdlib.h>

//Helper functions, using only pointers.
int Strs(char *substring, char *bigstring);
int Length(char *p);
void Copyto(char *original, char *copy);

//Assigned functions.
void FindRecord(char *filename, char *name, char record[]);
void Replace(char *name, char *newname, char record[]);
void SaveRecord(char *filename, char *name, char record[]);

int main(){
    char curr[20]; //current name initiliazed
    char *curr_pointer; //pointer (will point to curr)
    curr_pointer = curr; //curr_pointer points to first char of curr

    char new[20]; //as above, but for new.
    char *new_pointer;
    new_pointer = new;

    char *file = "phonebook.csv"; //point to name of the csv file we are editing.

    char row[1000]; //the row we want

    printf("Gimme gimme gimme a name after midnight: "); //prompts to get old name (in the style of ABBA).
    scanf("%s", curr);
    printf("Plz gimme a name to replace the one you just specified: "); //prompts to get new name
    scanf("%s", new);

    FindRecord(file, curr_pointer, row); //after this, row now stores the line we want to update
    Replace(curr_pointer, new_pointer, row); //after this, row contains updated line.
    SaveRecord(file, curr_pointer, row); //creates a temp with the updated line, then makes the temp the real thing.
}

int Strs(char *substring, char *bigstring) { //find a substring in a bigstring. 1 if found, 0 otherwise. Cheap implementation of StrStr or StrComp
    while (*substring != '\0') { //loop while the susbtring hasnt ended
      char *sub_head = substring; //points to the beginning of the substring
      while ((*substring != '\0') && (*bigstring != '\0') && (*substring == *bigstring) ){ //while neither string has ended, and they are equal.
        substring++; //advance both indices
        bigstring++;
      }
      if (*bigstring == '\0'){ //reached end of bigstring and found our result
        return 1;
      }
      substring = sub_head + 1; //now we perform the whole loop for the next char
    }
    return 0; //reached end of bigstring and didn't find result.
  }

  int Length(char *p){ //finds length of string pointed to.
      int count = 0;
      while( *p != '\0'){ //until the end of the string.
          count++; //inc count
          *p++; //inc pointer
      }
      return count;
  }

  void Copyto(char *original, char *copy){  //copies the original string to a different intiialized string  
    while (*original != '\0'){ 
        *copy = *original; //copy char by char until the end
        *copy++;
        *original++;
    } 
    *copy = '\0'; //add a null. Why not.
  } 

void FindRecord(char *filename, char *name, char record[]){ //store the correct row in record[]
    FILE *file_name = fopen(filename, "rt"); //opens the file
    char curr_line[1000]; //temporary array to store lines as we go through them
    if (file_name == NULL) { //check we actually have a filename.
        printf("File not found. \n");
    }
    while (fgets(curr_line, sizeof(curr_line), file_name)) { //loops through every line in the csv file
        if (Strs(curr_line, name) != 0) { //if we find a match, using my own version of the strstr method.
            for (int i = 0; i < 999; i++) { //change our record[] parameter from main to be this record
                record[i] = curr_line[i]; //loop through it, copying all 1000 characters.
            }
        }
    }
    fclose(file_name);
}

void Replace(char *name, char *newname, char record[]){ //NO ARRAYS HERE, AS SPECIFIED.
    char *prec = record; //points to beginning of record[]
    char temparr[1000]; //worst case scenario, it's 1000 long.
    char *temp = temparr; //points to temporary array

    int name_length = Length(name); //Calls a function (uses pointers only) to find the length of the original name.
    int new_length = Length(newname); //as above, for the new name.

    Copyto(newname, temp); //copy the newname to the beginning of temp
    prec = prec + name_length; //update pointer to skip the name field.
    Copyto(prec, temp+new_length); //copy the stuff after the name to temp
    prec = record; //point to beginning of record again.
    temp = temparr; //point to the beginning of temporary array again.
    Copyto(temp, prec);  //copy temp to record.
}

void SaveRecord(char *filename, char *name, char record[]){
    FILE *file1 = fopen(filename, "r+"); 
    FILE *file2 = fopen("temp.txt", "a"); //a to append stuff to the temp file.
    char *rec = record; //points to the beginning of the updated row we want to insert
    char curr_line[1000]; //will hold the current row 

    while (fgets(curr_line, sizeof(curr_line), file1)) { //while there are still lines in the original csv file
        if (Strs(curr_line, name) != 1) { //if we do NOT find the line we want 
            fprintf(file2 , "%s", curr_line); //place in temp file
        }
        if (Strs(curr_line, name) == 1) { //we found the line we want to update
            fprintf(file2, "%s", record); //we place the updated record in file, as opposed to placing the current line
        }
    }

    remove(filename); //remove the outdated one
    rename("temp.txt", filename) ; //rename the temp file (NOBODY SHALL EVER KNOW THE DIFFERENCE MUAHAHAHAHA)
    fclose(file1);
    fclose(file2); 
}