public class YearlyBudget {
  
    public static void main(String[] args) {
      double yearlyIncome = Double.parseDouble(args[0]);
      double bracket1Dollars = Double.parseDouble(args[1]);
      double bracket1Rate = Double.parseDouble(args[2]);
      double bracket2Dollars = Double.parseDouble(args[3]);
      double bracket2Rate = Double.parseDouble(args[4]);
      double bracket3Dollars = Double.parseDouble(args[5]);
      double bracket3Rate = Double.parseDouble(args[6]);
      double tax_deducted = calculateTax(yearlyIncome, bracket1Dollars, bracket1Rate, bracket2Dollars, bracket2Rate, bracket3Dollars, bracket3Rate);
      System.out.println("Question 1:");
      System.out.println(tax_deducted);
      //
      double monthly_expenses = 169.0;
      double monthly_income_after_tax = 969.0; //Dummy values for question 2.
      double month_savings = monthlySavings(monthly_expenses, monthly_income_after_tax);
      System.out.println("Question 2:");
      System.out.println(month_savings);
      //
      long ccnum = 1234567812345678L; //positive 16 digit integer
      boolean cccheck = validateCreditCard(ccnum);
      System.out.println("Question 3:");
      System.out.println(cccheck);
      //
      double monthly_rent = 600.0;
      double[] monthly_waste = buildExpense(monthly_rent);
      System.out.println("Question 4:");
      System.out.println("Array so prints wierd " + monthly_waste);
      //
      double yrincome_post_tax = 1200.0;
      double[] monthly_cc_pay = buildPayments(yrincome_post_tax);
      System.out.println("Question 5:");
      System.out.println("Array so prints wierd " + monthly_cc_pay);
      //
      double cc_balance = 1000.0;
      double yearly_interest_rate = 10.0;
  }
        
  //Question 1
  public static double calculateTax(double yearlyIncome, double bracket1Dollars, double bracket1Rate, double bracket2Dollars, double bracket2Rate, double bracket3Dollars, double bracket3Rate) {
    double paid = ((bracket2Dollars - bracket1Dollars) * (bracket1Rate/100)) + ((bracket3Dollars - bracket2Dollars) * (bracket2Rate/100)) + ((yearlyIncome - bracket3Dollars) * (bracket3Rate/100));
    return paid;
  }
  
  //Question 2
  public static double monthlySavings(double monthly_expenses, double monthly_income_after_tax) {
    return (monthly_income_after_tax - monthly_expenses); //Subtract expenses from tax-deducted income to obtain leftover savings.
  }
    
  //Question 3
  public static boolean validateCreditCard(long ccnum) {
    double even_storage = 0; //initializing variables for storing sums.
    double odd_storage = 0;
    for(int i=1 ; i <= 16; i++){ //loop from 1 to 16 digits of CC.
      double digit = ((ccnum % Math.pow(10, i)) - (ccnum % Math.pow(10, i-1)))/(Math.pow(10,i-1)); //the individual digit of each number in the CC. From right to left.
      if (i % 2 != 0){ //Checks if even. Seems reversed as counter goes from right to left.
        even_storage += digit;
      } else {
        double even_calc = (digit * 2) % 9;
        odd_storage += even_calc;
      }
    }
    double sum_even_odd = odd_storage + even_storage; //adds together sums of even and odd positions.
    if (sum_even_odd % 10 == 0){
      return true;
    } else {
      return false;}
  }
  
  //Question 4
  public static double[] buildExpense(double monthly_rent){
    double[] months = new double[12];
    for (int i = 0; i < months.length; i++){
      months[i] += monthly_rent; //pay rent
      months[i] += 600.0; //pay miscellaneous costs.
      if ((i == 0) || (i == 5)){
        months[i] += 200.0; //pay dentist
      }
      if (i == 8){
        months[i] += 300.0; //textbooks
      }
      if ((i == 3) || (i == 6) || (i == 8)){
        months[i] += 100.0; //birthday presents
      }
      if (i == 11){
        months[i] += 200.0; //holidays.
      }
    }
    return months;
  }
  
  //Question 5
  public static double[] buildPayments(double yrincome_post_tax){ 
    double[] ccpays_monthly = new double[12];
    for (int i = 0; i < ccpays_monthly.length; i++){
      ccpays_monthly[i] += (0.1 *(yrincome_post_tax/12.0)); //10% of monthly income
      if (i == 11){
        ccpays_monthly[i] += 150.0; //December gift.
      }
      if (i == 8){
        ccpays_monthly[i] += 200.0; //September gift.
      }
    }
    return ccpays_monthly;
  }

  //Question 6
  public static void printBalance(double cc_balance, double yearly_interest_rate, double[] monthly_waste, double[] monthly_cc_pay ){
    for (int i = 0; i < 12; i++){
      double month_var = 0;
      month_var += (cc_balance + monthly_waste[i] - monthly_cc_pay[i]);
      month_var += (yearly_interest_rate/1200 * month_var); //monthly interest rate added on.
      System.out.println("Month " + (i+1) + " balance " + month_var); //prints, doesn't return
    }
  }


  
  
  
  



}
