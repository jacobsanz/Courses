public class Q6 {
   
  public static void main(String[] args) { 
    double cc_balance = 1000.0;
    double yearly_interest_rate = 10.0;
    double[] monthly_waste = {200.0,100.0,300.0,100.0,100.0,100.0,100.0,100.0,100.0,100.0,100.0,500.0};
    double[] monthly_cc_pay = {20.0,10.0,30.0,10.0,10.0,10.0,10.0,10.0,10.0,10.0,10.0,50.0};
  
    for (int i = 0; i < 12; i++){
      double month_var = 0;
      month_var += (cc_balance + monthly_waste[i] - monthly_cc_pay[i]);
      month_var += (yearly_interest_rate/1200 * month_var); //monthly interest rate added on.
      System.out.println("Month " + (i+1) + " balance " + month_var);
    }
    
  }
  
  
}
