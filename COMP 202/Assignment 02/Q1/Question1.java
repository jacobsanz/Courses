public class Question1 {
  
  public static void main(String[] args) { 
    //Taking user inputs in order specified.
    double yearlyIncome = Double.parseDouble(args[0]);
    double bracket1Dollars = Double.parseDouble(args[1]);
    double bracket1Rate = Double.parseDouble(args[2]);
    double bracket2Dollars = Double.parseDouble(args[3]);
    double bracket2Rate = Double.parseDouble(args[4]);
    double bracket3Dollars = Double.parseDouble(args[5]);
    double bracket3Rate = Double.parseDouble(args[6]);
    //Calculations
    double paid = ((bracket2Dollars - bracket1Dollars) * (bracket1Rate/100)) + ((bracket3Dollars - bracket2Dollars) * (bracket2Rate/100)) + ((yearlyIncome - bracket3Dollars) * (bracket3Rate/100));
    System.out.println(paid);
  }
  

}
