public class Question4 {
    
  public static void main(String[] args) { 
    
    double monthly_rent = 600.0;
    
    double[] months = new double[12];
    for (int i = 0; i < months.length; i++){
      months[i] += monthly_rent; //pay rent
      months[i] += 600.0; //pay miscellaneous costs.
      if ((i == 0) || (i == 5)){
        months[i] += 200.0; //pay dentist
      }
      if (i == 8){
        months[i] += 300.0; //textbooks
      }
      if ((i == 3) || (i == 6) || (i == 8)){
        months[i] += 100.0; //birthday presents
      }
      if (i == 11){
        months[i] += 200.0; //holidays.
      }
    }
    
    
    ///////////////////////
    
    for (int i = 0; i < months.length; i++){
      System.out.println("month " + i);
      System.out.println(months[i]);
    }
    
    
  }
}
