public class YearlyBudget {
//This program takes the information specified through command line arguments.
//If a valid credit card number passes the check, it calculates and outputs monthly balances and savings.
//it accounts for taxes and interest rates.
  
    public static void main(String[] args) {
      //Bracket info for Question 1 to work
      double bracket1Dollars = 10000;
      double bracket1Rate = 20;
      double bracket2Dollars = 20000;
      double bracket2Rate = 30;
      double bracket3Dollars = 45000;
      double bracket3Rate = 50;
      
      //Question 7
      //Outputs may seem strange to a broke student because only 10% of income is going towards paying card.
      //user inputted data
      double pretax_income = Double.parseDouble(args[0]);
      double present_cc_balance = Double.parseDouble(args[1]);
      double annual_ir = Double.parseDouble(args[2]);
      long ccnum = Long.parseLong(args[3]);
      double monthly_rent = Double.parseDouble(args[4]);
      
      //Assigning method calls to variables.
      double[] monthly_waste = buildExpense(monthly_rent);
      double yr_income_post_tax = (pretax_income) - (calculateTax(pretax_income, bracket1Dollars, bracket1Rate, bracket2Dollars, bracket2Rate, bracket3Dollars, bracket3Rate));
      double[] monthly_cc_pay = buildPayments(yr_income_post_tax);
      double save_sum = 0; //will be used to determine sum of savings.
      
      if (validateCreditCard(ccnum) == true){ //calls method which checks card validity.
        printBalance(present_cc_balance, annual_ir, monthly_waste, monthly_cc_pay); //prints monthly balance
        for (int i = 0; i < 12; i++){ //prints monthly savings
          save_sum += monthlySavings(monthly_waste[i], (yr_income_post_tax/12.0));
          System.out.println("Month " + (i+1) + " savings " + monthlySavings(monthly_waste[i], (yr_income_post_tax/12.0))); //prints monthly savings.
        }
        System.out.println("total cumulative savings for the year: " + save_sum); //print sum of savings.
      } else{
      System.out.println("Invalid card"); //In case of invalid card number.
      }
        
  }
        
  //Question 1
  //A formula - uses the specified tax brackets and their respective rate to return taxes paid on yearly income.
  public static double calculateTax(double yearlyIncome, double bracket1Dollars, double bracket1Rate, double bracket2Dollars, double bracket2Rate, double bracket3Dollars, double bracket3Rate) {
    double paid = ((bracket2Dollars - bracket1Dollars) * (bracket1Rate/100)) + ((bracket3Dollars - bracket2Dollars) * (bracket2Rate/100)) + ((yearlyIncome - bracket3Dollars) * (bracket3Rate/100));
    return paid;
  }
  
  //Question 2
  //Formula - returns a double for the amount of money saved in a single month.
  public static double monthlySavings(double monthly_expenses, double monthly_income_after_tax) {
    return (monthly_income_after_tax - monthly_expenses); //Subtract expenses from tax-deducted income to obtain leftover savings.
  }
    
  //Question 3
  //Method which checks if the card number is valid, so as to proceed with calculations in the main.
  public static boolean validateCreditCard(long ccnum) {
    double even_storage = 0; //initializing variables for storing sums.
    double odd_storage = 0;
    for(int i=1 ; i <= 16; i++){ //loop from 1 to 16 digits of CC.
      double digit = ((ccnum % Math.pow(10, i)) - (ccnum % Math.pow(10, i-1)))/(Math.pow(10,i-1)); //the individual digit of each number in the CC. From right to left.
      if (i % 2 != 0){ //Checks if even. Seems reversed as counter goes from right to left.
        even_storage += digit;
      } else {
        double even_calc = (digit * 2) % 9;
        odd_storage += even_calc;
      }
    }
    double sum_even_odd = odd_storage + even_storage; //adds together sums of even and odd positions.
    if (sum_even_odd % 10 == 0){ //checks for mod10 divisibility of sum, final stage of check.
      return true;
    } else {
      return false;}
  }
  
  //Question 4
  //Outflows of money are added to an array of doubles.
  public static double[] buildExpense(double monthly_rent){
    double[] months = new double[12]; //one array element for every month.
    for (int i = 0; i < months.length; i++){
      months[i] += monthly_rent; //pay rent
      months[i] += 600.0; //pay miscellaneous costs.
      if ((i == 0) || (i == 5)){
        months[i] += 200.0; //pay dentist
      }
      if (i == 8){
        months[i] += 300.0; //textbooks
      }
      if ((i == 3) || (i == 6) || (i == 8)){
        months[i] += 100.0; //birthday presents
      }
      if (i == 11){
        months[i] += 200.0; //holidays.
      }
    }
    return months;
  }
  
  //Question 5
  //Money that goes into payng the credit card each month, returns an array of doubles.
  public static double[] buildPayments(double yrincome_post_tax){ 
    double[] ccpays_monthly = new double[12];
    for (int i = 0; i < ccpays_monthly.length; i++){
      ccpays_monthly[i] += (0.1 *(yrincome_post_tax/12.0)); //10% of monthly income. Try changing it to a higher % for it to make any sense with student income.
      if (i == 11){
        ccpays_monthly[i] += 150.0; //December gift.
      }
      if (i == 8){
        ccpays_monthly[i] += 200.0; //September gift.
      }
    }
    return ccpays_monthly;
  }

  //Question 6
  //Uses returns of other methods to print out card balance for each month.
  public static void printBalance(double cc_balance, double yearly_interest_rate, double[] monthly_waste, double[] monthly_cc_pay ){
    for (int i = 0; i < 12; i++){
      double month_var = 0;
      month_var += (cc_balance + monthly_waste[i] - monthly_cc_pay[i]); //adds spending, subtracts paying from current balance
      month_var += (yearly_interest_rate/1200 * month_var); //monthly interest rate added on.
      cc_balance = month_var; //refreshes cc balance
      System.out.println("Month " + (i+1) + " balance " + month_var); //prints, doesn't return
    }
  }
  
}