public class RaceTrack {
  public static void main(String[] args) {
    //Inputs in variables.
    Double large_radius = Double.parseDouble(args[0]);             //Converts args[0] user inputted radius from string to double, then assigns it to a variable.
    Integer num_lanes = Integer.parseInt(args[1]);                 //Converts args[1] to integer, assigns it to  variable for inputted number of lanes.
      
    //Calculations
    Double outer_area =  large_radius * large_radius * (Math.PI);   //Uses large radius and Pi to calculate area of outer circle. Formula(A = Pi*r*r).
    Double lane_width = num_lanes * 2.8;                            //Creates 'double' which uses integer number of lanes to calculate total lane width.
    Double inner_radius = large_radius - lane_width;                //Creates variable to find inner radius (empty space in center of track).
    Double inner_area = inner_radius * inner_radius * (Math.PI);    //Calculates inner space in center of track.
    Double run_area = outer_area - inner_area;                      //Calculates total area for asphalt.
    Double cost = run_area * 7.49;                                  //Calculates the price for said area.
    Double tax = (0.15 * cost);                                     //Calculates 15% of the cost.
    Integer total = (int)(Math.ceil(tax + cost));                   //Calculates cost with tax, rounds up to integer.
    
    //Outputs
    System.out.println("Total area: " + run_area);                  //Prints concatenation of strings and variables as outputs.
    System.out.println("Subtotal: " + cost + "$");
    System.out.println("Sales Tax (15%): " + tax + "$");
    System.out.println("Total: " + total + "$");
  } 
}
