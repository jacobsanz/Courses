import java.awt.Color;
import becker.robots.*;
public class RobotsMoveLightsSmall {
  
  public static void main(String[] args) {
   Robot robot = makeSmallCity();
   //This code gets the robot out of the maze
   robot.turnLeft();    //Robot turns to face downwards.
   robot.turnLeft();
   robot.turnLeft();
   robot.move();        //It moves in this direction (downwards).
   robot.move();
   robot.move();
   robot.turnLeft();    //Turns to face the light.
   robot.move();        //Moves towards the light
   robot.pickThing();   //Picks up the light
   robot.turnLeft();    //Initiates celebration dance/twirl. Sweet moves.
   robot.turnLeft();
   robot.turnLeft();
   robot.turnLeft();
   robot.move();        //Starts heading to the exit.
   robot.move();
   robot.move();
   robot.turnLeft();    //Turns towards the exit.
   robot.turnLeft();
   robot.turnLeft();
   robot.move();        //Moves closer to the exit. Almost there.
   robot.turnLeft();    //Turns to face the exit
   robot.move();        //Moves through the exit.

  }
  
//Ignore everything in this method and below.
public static Robot makeSmallCity(){
  final int SIZE = 4;
  final int LIGHT_STREET = 1;
  final int LIGHT_AVENUE = 1;
  
  City ottawa = new City(12,12);
  
  //box
  for (int i = 0; i < SIZE-1; i++) {
   new Wall(ottawa, LIGHT_STREET, LIGHT_AVENUE + i,
    Direction.NORTH);
  } 
  
  for (int i = SIZE-1; i < SIZE+1; i++) {
   new Wall(ottawa, LIGHT_STREET+2, LIGHT_AVENUE + i,
    Direction.NORTH);
  }
  
  for (int i = 0; i < SIZE+1; i++) {
   new Wall(ottawa, LIGHT_STREET+SIZE, LIGHT_AVENUE + i,
    Direction.SOUTH);
  }  
  for (int i = -1; i < SIZE; i++) {
   new Wall(ottawa, LIGHT_STREET+i+1, LIGHT_AVENUE,
    Direction.WEST);
  }
  for (int i = -1; i < SIZE; i++) {
    if(LIGHT_STREET+i == 0 || LIGHT_STREET+i ==1)
      new Wall(ottawa, LIGHT_STREET+i+1, LIGHT_AVENUE+2, Direction.EAST);
    else if(LIGHT_STREET+i != 4)
      new Wall(ottawa, LIGHT_STREET+i+1, LIGHT_AVENUE+4, Direction.EAST);
  } 
  Robot asimo = new Robot(ottawa, LIGHT_STREET,
   LIGHT_AVENUE, Direction.EAST);
  new Flasher(ottawa, 4, 2, true);
  return asimo;
}

}