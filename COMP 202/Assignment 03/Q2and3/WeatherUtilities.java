mport java.util.Scanner;
import java.util.Collections;

public class WeatherUtilities {
  
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
    int n = Integer.parseInt(args[0]);
    double[] temps = new double[n];
    boolean[] sunny = new boolean[n];
    WeatherEntry[] weatherlist = new WeatherEntry[n]; //creates a null array.
    
    for (int i = 0; i < n; i++){ //single iteration takes temp and sunny value for single day.
      System.out.println("Day " + (i+1) + " temperature:");
      temps[i] = scan.nextDouble();
      System.out.println("Was day " + (i+1) + " sunny?:");
      sunny[i] = scan.nextBoolean();
    }
    
    for (int j = 0; j < n; j++){ //fills null array with real weatherentries. Fulfills requirements of unnecessary? extra loop.
      WeatherEntry temp_object = new WeatherEntry(temps[j], sunny[j]);
      weatherlist[j] = temp_object;
    }
    
    int nice_days = countGoodDays(weatherlist); //method overloading.
    double max = maxi(temps);
    double min = mini(temps);
    System.out.println("There were " + nice_days +  " nice days.");
    System.out.println("The highest temperature was " + max + " degrees Celsius and the lowest was " + min + ".");
  }
  
  public static int countGoodDays(double[] temps, boolean[] sunny){
    int counter = 0;
    if (temps.length != sunny.length){
      throw new IllegalArgumentException("Arrays are not of same length");
    }
    for (int i = 0; i < temps.length; i++){
      if ((temps[i] > -30.0) && (sunny[i] == true)){
        counter += 1;
      }
    }
    return counter;
  }
  
  public static int countGoodDays(WeatherEntry[] wtf){
    int counter = 0;
    for (int i = 0; i < wtf.length; i++){
      if (wtf[i].isGoodWeather() == true){
       counter++; 
      }
    }
    return counter;
  }
  
  private static double maxi(double[] temperaturelist){
    double current_max = temperaturelist[0];
    for (int i = 0; i < temperaturelist.length; i++){
      if (temperaturelist[i] > current_max){
        current_max = temperaturelist[i];
      }
    }
    return current_max;
  }
  
  private static double mini(double[] temperaturelist){
    double current_min = temperaturelist[0];
    for (int i = 0; i < temperaturelist.length; i++){
      if (temperaturelist[i] < current_min){
        current_min = temperaturelist[i];
      }
    }
    return current_min;
  }
  
}
