public class WeatherEntry {
  private double temperatureInCelsius;
  private boolean isSunny;
    
  public WeatherEntry(double temperatureInCelsius, boolean isSunny) { 
    this.temperatureInCelsius = temperatureInCelsius;
    this.isSunny = isSunny;
  }
  
  public double getTemperatureCelsius(){
    return this.temperatureInCelsius;
  }
  
  public boolean isGoodWeather(){
    if ((this.temperatureInCelsius > -30.0) && (this.isSunny == true)){
      return true;
    } else{
      return false;
    }
  }
  
  public void display(boolean isCelsius){
    double fahrenheit_conv = (getTemperatureCelsius() * 1.8) + 32.0; //from formula
    if (isCelsius && isSunny && isGoodWeather()){
      System.out.println("It is " + getTemperatureCelsius() + "degrees Celsius and is sunny. It is a good day");
    }
    if (isCelsius && !isSunny){
      System.out.println("It is " + getTemperatureCelsius() + "degrees Celsius and is not sunny. It is not a good day");
    }
    if (isCelsius && isSunny && !isGoodWeather()){
      System.out.println("It is " + getTemperatureCelsius() + "degrees Celsius and is sunny. It is not a good day");
    }
    if (!isCelsius && isSunny && isGoodWeather()){
      System.out.println("It is " + fahrenheit_conv + "degrees Fahrenheit and is sunny. It is a good day");
    }
      if (!isCelsius && !isSunny){
      System.out.println("It is " + fahrenheit_conv + "degrees Fahrenheit and is not sunny. It is not a good day");
    }
    if (!isCelsius && isSunny && !isGoodWeather()){
      System.out.println("It is " + fahrenheit_conv + "degrees Fahrenheit and is sunny. It is not a good day");
    }
  }

  
}
