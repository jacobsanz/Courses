public class BirthdayParadox {
  
  public static void main(String[] args) { 
    //Question 7
    for (int i = 1; i <= 100; i++){
      System.out.println(i + " " + runExperiment(i));
    }
  }
  
  //Question 1
  public static int[] generateArray(int size, int range) {
    int[] array = new int[size];
    for (int i = 0; i < size; i++){
      array[i] = (int)(Math.random() * range); //Math.random works until range-1, so don't need to manually put the -1.
    }
    return array;
  }
  
  //Question 2
  public static int[][] generateAllData(int iterations, int size, int range){
    int[][] array2D = new int[iterations][size];
    for (int i = 0; i < iterations; i++){
      array2D[i] = generateArray(size, range);
    }
    return array2D;
  }
    
  //Question 3
  public static int countElement(int[][] list, int element){
    int counter = 0;
    for (int i = 0; i < list.length; i++){
      for (int j = 0; j < list[i].length; j++){
        if (element == list[i][j]){
          counter += 1;
        }
      }
    }
    return counter;
  }
    
  //Question 4
  public static int maxDay(int[][] listy){
    int current_maxday = -10; //dummy starting value that won't interfere.
    int current_count = 0;
    for (int i = 0; i < listy.length; i++){
      for (int j = 0; j < listy[i].length; j++){
        if (countElement(listy, listy[i][j]) > current_count){
          current_maxday = listy[i][j]; //This way, if there is a tie, the method automatically chooses the "first" of the two modes.
          current_count = countElement(listy, listy[i][j]);
        }
      }
    }
    return current_maxday;
  }
  
  //Question 5
  public static boolean hasDuplicates(int[] listz){
    int counter = 0;
    int[][] nest_list = new int[1][1];
    nest_list[0] = listz; //convert int[] into int[][]
    int mode = maxDay(nest_list);
    if (countElement(nest_list, mode) > 1){ //if the mode occurs more than once, there are repeats.
      return true;
    } else{
      return false;
    }
  }
  
  //Question 6
  public static double runExperiment(int size){
    if (size < 1){
      throw new IllegalArgumentException("Size must be no smaller than 1");
    }
    int[][] datas = generateAllData(200, size, 365);
    double dupli_counter = 0.0;
    for (int i = 0; i < datas.length; i++) {
      if (hasDuplicates(datas[i]) == true){
        dupli_counter += 1.0;
      }
    }
    return (dupli_counter / datas.length);
  }  
}
