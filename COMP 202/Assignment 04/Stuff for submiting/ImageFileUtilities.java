import java.io.*;
import java.util.*;

public class ImageFileUtilities {
  
  public static Image read(String filename) throws IOException { 
    Scanner scan = new Scanner(new File(filename)); //for loop 1
    Scanner scan2 = new Scanner(new File(filename)); //for loop 2
    int i = 0; //bunch of int variables to divide up the lines.
    int j = 0; 
    int k = 0;
    String format = ""; //create variable to hold P2 or P3
    String comments = ""; //var to hold metadata
    int width = 0; //the stuff we want to isolate to create the Image.
    int height = 0;
    int max = 0;
    
    while (scan.hasNextLine()){ //this loops handles the format and the comments.
      String current_line = scan.nextLine();
      if (i == 0){
        format = current_line;
        i++;
      }
      if (current_line.charAt(0) == '#'){
        comments +=  current_line + "\n"; //+ " "; //store metadata.
        i++; //gives us the position of the line of the last comment.
      }
      j++;
    }
    
    while (k < i){ //skips lines without integers.
     scan2.nextLine();
     k++;
    }
    while (k < j){ //gets width, height, and max.
     String current = scan2.nextLine();
     if (k == i){
      String[] result = current.split(" ", 2); //use split method to divide line into 2 strings.
      width = Integer.parseInt(result[0]); //parse strings to int for width and height.
      height = Integer.parseInt(result[1]);
     }
     if (k == i+1){
       max = Integer.parseInt(current);
     }
     k++;
    }

    Pixel[][] collection = new Pixel[height][width];
    Scanner scan3 = new Scanner(new File(filename));
    int l = 0;
    //Case 1: The p3 Pixel[][]
    if (format.equals("P3")){
      while(l < i+2){ //skips to the line where the pixel values start
        scan3.nextLine();
        l++;
      }
      while(scan3.hasNextInt()){
        for (int z = 0; z < collection.length; z++){
          for (int w = 0; w < collection[z].length; w++){
            int red = scan3.nextInt();
            int green = scan3.nextInt();
            int blue = scan3.nextInt();
            Pixel rgb = new Pixel(red, green, blue);
            collection[z][w] = rgb;
          }
        }
      }
    }
    //Case 2: The p2 Pixel[][]
    if (format.equals("P2")){
      while(l < i+2){ //same as above for p3
        scan3.nextLine();
        l++;
      }
      while(scan3.hasNextInt()){
        for (int z = 0; z < collection.length; z++){
          for (int w = 0; w < collection[z].length; w++){
            int only = scan3.nextInt();
            Pixel rgb = new Pixel(only);
            collection[z][w] = rgb;
          }
        }
      }
    } 
  Image x = new Image(comments, max, collection);
  return x;
  }
  
  public static void writePnm(Image img, String filename)throws IOException{ //NOT TESTED YET
    BufferedWriter bw = null;
    File file = new File(filename);
    FileWriter fw = new FileWriter(file);
    bw = new BufferedWriter(fw);
    String filecontent = "P3 \n";
    filecontent += img.getMetadata() + "\n";
    filecontent += img.getWidth() + " " + img.getHeight() + "\n";
    filecontent += img.getMaxRange() + "\n";
    for (int i = 0; i < img.getHeight(); i++){
      for (int j = 0; j < img.getWidth(); j++){
        filecontent += (img.getPixel(i, j)).getRed() + "\n";
        filecontent += (img.getPixel(i, j)).getGreen() + "\n";
        filecontent += (img.getPixel(i, j)).getBlue() + "\n";
      }
    }
    bw.write(filecontent);
    bw.close();
  }
  
    public static void writePgm(Image img, String filename)throws IOException{ 
    BufferedWriter bw = null;
    File file = new File(filename);
    FileWriter fw = new FileWriter(file);
    bw = new BufferedWriter(fw);
    String filecontent = "P2 \n";
    filecontent += img.getMetadata() + "\n";
    filecontent += img.getWidth() + " " + img.getHeight() + "\n";
    filecontent += img.getMaxRange() + "\n";
    for (int i = 0; i < img.getHeight(); i++){
      for (int j = 0; j < img.getWidth(); j++){
        filecontent += (img.getPixel(i, j)).grey() + "\n"; //same as above, but grey.
      }
    }
    bw.write(filecontent);
    bw.close();
  }
  
}
