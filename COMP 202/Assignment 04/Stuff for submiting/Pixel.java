public class Pixel {
  
  private int red;
  private int green;
  private int blue;
  
  public Pixel(int red_in, int green_in, int blue_in) { 
    if ((red_in < 0) || (green_in < 0) || (blue_in < 0) || (red_in > 255) || (green_in > 255) || (blue_in > 255)){
      throw new IllegalArgumentException("intensity must be in range [0-255]");
    }
    this.red = red_in;
    this.green = green_in;
    this.blue = blue_in;
  }
  
  public Pixel(int intensity) {
    if ((intensity < 0) || (intensity > 255)){
      throw new IllegalArgumentException("intensity must be in range [0-255]");
    }
    this.red = intensity;
    this.green = intensity;
    this.blue = intensity;
  }
  
  public int getRed(){
   return this.red; 
  }
  
  public int getGreen(){
   return this.green; 
  }
  
  public int getBlue(){
   return this.blue; 
  }
  
  public int grey(){
   int average = (this.red + this.green + this.blue)/3; //int division means last decimal point is truncated.
   return average;
  }
  
  //public static void main(){
    //System.out.println(getBlue()); 
  //}
  
}
