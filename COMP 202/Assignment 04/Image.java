public class Image {
  
  private String metadata;
  private int maxRange;
  private Pixel[][] data; // Pixel[height][width]
  
  public Image(String metadata, int maxRange, Pixel[][] data) { //constructor
    this.metadata = metadata; //necessary?
    this.maxRange = maxRange;
    this.data = data;
    Pixel[][] data_copy = new Pixel[data.length][data[0].length];
    for (int i = 0; i < data.length; i++){
      for (int j = 0; j < data[i].length; j++){
        data_copy[i][j] = data[i][j];
      }
    }
    if (maxRange < 0){
      throw new IllegalArgumentException("maxRange can't be negative");
    }
  }
  
  public String getMetadata(){ //ARE ALL OF THESE GETS MEANT TO BE PRIVATE? AMBIGUOUS INSTRUCTIONS.
    return this.metadata;
  }
  
  public int getMaxRange(){
    return this.maxRange;
  }
  
  public int getWidth(){ //Are height and width right way around?
    int wide = this.data[0].length; //Assuming all entries are of the same length as the first one.
    return wide;
  }
  
  public int getHeight(){
    int high = this.data.length;
    return high;
  }
  
  public Pixel getPixel(int i, int j){
    Pixel gimme = this.data[i][j];
    return gimme;
  }
  
  public void flip(boolean horizontal){
    Pixel[][] flipped = new Pixel[this.data.length][this.data[0].length];
    //vertical flip
    if (horizontal == false){
      for (int i = 0; i < flipped.length; i++){
        for (int j = 0; j < flipped[i].length; j++){
          flipped[i][j] = this.data[getHeight()-1-i][j]; //-1 to get the index.
        }
      }
    }
    //horizontal flip
    if (horizontal == true){
      for (int i = 0; i < flipped.length; i++){
        for (int j = 0; j < flipped[i].length; j++){
          flipped[i][j] = this.data[i][getWidth()-1-j];
        }
      }
    }
    this.data = flipped; //equivalent to return?
  }
  
  public void toGrey(){ 
    for (int i = 0; i < this.data.length; i++){
        for (int j = 0; j < this.data[i].length; j++){
          int current_pixel = this.data[i][j].grey();
          this.data[i][j] = new Pixel(current_pixel); //overloaded constructor in pixel will allow single int input.
        }
      }
  }
    
  public void crop(int startX, int startY, int endX, int endY){ 
    if ((startX < 0) || (startY < 0) || (endX < 0) || (endY < 0) || (endY-startY < 0) || (endX-startX < 0)){
      throw new IllegalArgumentException("Invalid input.");
    }
    Pixel[][] cropped = new Pixel[endX-startX][endY-startY]; 
    for (int i = startX; i < endX; i++){
      for (int j = startY; j < endY; j++){
        cropped[i-startX][j-startY] = this.data[i][j];
      }
    }
    this.data = cropped;
  }
  
  
}
